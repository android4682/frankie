/***
 * Format for console.log message:
 * [MODULE][MSG_TYPE] MESSAGE
 * ex.:
 * [MySQL][Error] Couldn't connect to host.
 * 
 * Modules can be functions, bot commands or literal modules from nodejs/npm.
 * 
 * MSG_TYPEs: (with the exact case letters)
 * Info
 * Warning
 * Critical
 * Error
 * DEBUG
 * KillerDEBUG
 * 
 * Error codes:
 * 0 = Succesfull exit
 * 1 = Failed to connect to bot database
 * 2 = Failed to connect to web database
 * 3 = Failed to load 'quotes' from database
 * 4 = Failed to load 'quotes' to memory
 * 5 = Failed to load 'ranks' from database
 * 6 = Failed to load 'ranks' to memory
 * 7 = Failed to load 'warnings' from database
 * 8 = Failed to load 'warnings' to memory
 * 9 = Unexpected error while running init_db1()
 * 10 = Unexpected error while running init_db2()
 * 11 = Unexpected error while running init_db3()
 * 12 = Failed to load 'dashboards' from database
 * 13 = Failed to load 'dashboards' to memory
 * 14 = Failed to load 'guild_info' from database
 * 15 = Failed to load 'guild_info' to memory
 * 16 = Failed to load 'levels' from database
 * 17 = Failed to load 'levels' to memory
 * 18 = Failed to load 'modcom' from database
 * 19 = Failed to load 'modcom' to memory
 * 20 = Failed to load 'quotes' from database
 * 21 = Failed to load 'quotes' to memory
 * 22 = Failed to load 'reaction' from database
 * 23 = Failed to load 'reaction' to memory
 * 22 = Failed to load 'rip' from database
 * 23 = Failed to load 'rip' to memory
 * 26 = Failed to load 'voicecom' from database
 * 27 = Failed to load 'voicecom' to memory
 * 28 = Failed to load 'warnings' from database
 * 29 = Failed to load 'warnings' to memory
 * 30 = Unexpected error while running init_db1()
 * 31 = Unexpected error while running init_db2()
 * 32 = Unexpected error while running init_db3()
 * 33 = Unexpected error while running init_db4()
 * 34 = Unexpected error while running init_db5()
 * 35 = Unexpected error while running init_db6()
 * 36 = Unexpected error while running init_db7()
 * 37 = Unexpected error while running init_db8()
 * 38 = Unexpected error while running init_db9()
 * 39 = Unexpected error while running init_db10()
 * 40 = Unexpected error while running init_bot()
 * 41 = Unexpected error while running init_webData()
 * 42 = Failed to load 'roles' (user_updates) from database
 * 43 = Failed to load 'roles' (user_updates) to memory
 * 44 = Unexpected error while running init_db11()
 * 45 = Failed to get the latest git commit info
 * 46 = Couldn't find the latest {branch} update info on remote
 * 47 = Failed to get the local branch
 * 48 = Failed to create the ipLists.js file!
 * 49 = Failed to load ipLists.js contents from file!
 * 50 = ipList.js contents are not a JSON! Failed to initialize ip lists
 * 51 = Failed to connect to the Discord API Servers
 * 52 = Failed to load API Semi Keys
 ***/

// Has to set early in this script
var moduleName = "Discord-Bot"

// Setting a color scheme for console.log
const colorScheme = require("./utils/colorScheme.js")
console.log('\x1b[1m') // Setting color pre-settings

/***
 * debugMsg - Send message to console (only to be used for debug messages!)
 * @param {String} dMsg - Message to be logged to console
 * @param {Array,Object} dirVar - array or object that needs to be fully printed
 ***/
function debugMsg(dMsg, dirVar) {
	if (debug) {
		console.log(colorScheme.once.debug + "[Unique]" + dMsg + colorScheme.once.reset)
		if (dirVar != null) {
			console.dir(dirVar)
		}
	} else if (killerDebug) {
		console.log(colorScheme.once.killerDebug + dMsg + colorScheme.once.reset)
		if (dirVar != null) {
			console.dir(dirVar)
		}
	} else if (devDebug) {
		console.log(colorScheme.once.devDebug + dMsg + colorScheme.once.reset)
		if (dirVar != null) {
			console.dir(dirVar)
		}
	}
}

function kDebugMsg(dMsg, dirVar) {
	if (killerDebug) {
		console.log(colorScheme.once.killerDebug + "[Unique]" + dMsg + colorScheme.once.reset)
		if (dirVar != null) {
			console.dir(dirVar)
		}
	} else if (devDebug) {
		console.log(colorScheme.once.devDebug + dMsg + colorScheme.once.reset)
		if (dirVar != null) {
			console.dir(dirVar)
		}
	}
}

function devMsg(dMsg, dirVar) {
	if (devDebug) {
		console.log(colorScheme.once.devDebug + "[Unique]" + dMsg + colorScheme.once.reset)
		if (dirVar != null) {
			console.dir(dirVar)
		}
	}
}

// Defining debug just in case
var debug = false
var killerDebug = false
var devDebug = false
var debugMode = "NO-DEBUG"

// Check if script is run with the debug switch, if so enable debug mode.
if (process.argv.indexOf("debug") != -1) {
	debug = true
	debugMode = "DEBUG"
	debugMsg(`[${moduleName}][${debugMode}] Debug enabled!`)
} else if (process.argv.indexOf("killme") != -1) {
	killerDebug = true
	debugMode = "KillerDEBUG"
	const killerColor = {
		chat: "\x1b[41m\x1b[1m\x1b[37m",
		reset: "\x1b[0m\x1b[1m"
	}
	const killerMessages = {
		welcome: [
			`${killerColor.chat}I'm your worst nightmare.${killerColor.reset}`,
			`${killerColor.chat}I'm your worst debugger.${killerColor.reset}`,
			`${killerColor.chat}I hope I'll kill you with my verbose!${killerColor.reset}`
		]
	}
	console.log(colorScheme.debug, `[${moduleName}][${debugMode}] Killer debug enabled!`)
	kDebugMsg(`[${moduleName}][${debugMode}-Chat] ${killerMessages.welcome[randomInt(0, killerMessages.welcome.length-1)]}`)
} else if (process.argv.indexOf("devops") != -1) {
	devDebug = true
	debugMode = "Dev-DEBUG"
	const devColor = {
		chat: "\x1b[0m\x1b[45m\x1b[1m\x1b[37m",
		reset: "\x1b[0m\x1b[1m"
	}
	const devMessages = {
		welcome: [
			`${devColor.chat}Every detail is visable!${devColor.reset}`,
			`${devColor.chat}This isn't gonna be fun..${devColor.reset}`,
			`${devColor.chat}Oh boy. Here we go again!${devColor.reset}`,
			`${devColor.chat}If I'm here that means...${devColor.reset}`,
			`${devColor.chat}OH SHIET!${devColor.reset}`
		]
	}
	console.log(colorScheme.debug, `[${moduleName}][${debugMode}] Dev debug enabled!`)
	devMsg(`[${moduleName}][${debugMode}-Chat] ${devMessages.welcome[randomInt(0, devMessages.welcome.length-1)]}`)
}

// Loading MySQL credentials
const mysqlData = require("./credentials/mysql.js")

// Loading bot credentials
const botCred = require("./credentials/botCred.js")

// Loading Modules
const Eris = require("eris") // Eris lib, for the Discord functionality, duh.
debugMsg(`[${moduleName}][`+debugMode+`] Module 'eris' loaded as 'Eris'.`)
const mysql = require("mysql") // mysql lib, for mysql connection(s) (pool(s))
debugMsg(`[`+moduleName+`][`+debugMode+`] Module 'mysql' loaded as 'mysql'.`)
const async = require("async") // async lib, for async stuff like looping and shit
debugMsg(`[`+moduleName+`][`+debugMode+`] Module 'async' loaded as 'async'.`)
const dateformat = require("dateformat") // dateformat lib, for formatting dates cuz we ain't a browser
debugMsg(`[`+moduleName+`][`+debugMode+`] Module 'dateformat' loaded as 'dateformat'.`)
const math = require("mathjs") // mathjs lib, to do quick mafs
debugMsg(`[`+moduleName+`][`+debugMode+`] Module 'mathjs' loaded as 'math'.`)
const git = require("simple-git") // simple-git lib, now we are able todo git commands from this script!
debugMsg(`[`+moduleName+`][`+debugMode+`] Module 'simple-git' loaded as 'git'.`)
const md5 = require("md5") // md5 lib, for encrypting strings to md5 (mostly used apiAuth)
debugMsg(`[`+moduleName+`][`+debugMode+`] Module 'md5' loaded as 'md5'.`)
const fs = require("fs") // fs lib, to create, read, edit, appand and delete files on the disk
debugMsg(`[`+moduleName+`][`+debugMode+`] Module 'fs' loaded as 'fs'.`)

// Setting prefix of bot
const botPrefix = "fr!"

// Version of the bot
var versionInfo = {
	versionNumber: "v1.1.4",
	gitBranch: "beta"
}

versionInfo.version = `${versionInfo.versionNumber} (${versionInfo.gitBranch})`

// (Overwrite) Name of bot
const botName = "Frankie"

// Default values
/***
 * Template/Structure:
 * settings = {
 *	bot: {
 *		"quotes": {
 *			<guild_id>: {
 *				<user_id>: [
 *					{
 *						"id": <database_id>
 *						"guild_id": <guild_id>
 *						"quote": <quote>
 *						"user_id": <user_id>
 *						"timestamp": <timestamp in UNIX code>
 *						"year_k": <year_k>
 *					}, // Next Quote
 *				], // Next User
 *			}, // Next Guild
 *		},
 *		"ranks": {
 *			<guild_id>: {
 *				<user_id>: {
 *					"id": <database_id>
 *					"guild_id": <guild_id>
 *					"user_id": <user_id>
 *					"exp": <exp>
 *					"total_exp": <total_exp>
 *					"level": <level>
 *					"op_pause": <op_pause>
 *				}, // Next User
 *			}, // Next Guild
 *		},
 *		"warnings": {
 *			<guild_id>: {
 *				<user_id>: [
 *	 				{
 *						"id": <database_id>
 *						"guild_id": <guild_id>
 *						"user_id": <user_id>
 *						"reason":  <reason of warning>
 *						"timestamp": <timestamp in UNIX>
 *						"given_by_id": <user id that gave the warning>
 *					}, // Next Warning of User
 *				], // Next User
 *			}, // Next Guild
 *		}
 *	},
 * 	web: {
 * 		"dashboards": {
 * 			<guild_id>: {
 * 				"guild_id": <guid>
 * 				"user_ids": <user_ids that have access to dashboard> // Unnecessary
 * 				"permissions": <bots_premission> // Unnecessary
 * 				"date_code": <date_code(not in unix)> // Unnecessary
 * 				"levels_ena": <levels_ena>
 * 				"modcom_ena": <modcom_ena>
 * 				"voicecom_ena": <voicecom_ena>
 * 				"reaction_ena": <reaction_ena>
 * 				"roles_ena": <roles_ena>
 * 				"quotes_ena": <quotes_ena>
 * 				"warnigns_ena": <warnigns_ena>
 * 				"welcome_ena": <welcome_ena>
 * 				"rip_ena": <rip_ena>
 * 			}, // Next Guild
 * 		},
 * 		"guild_info": {
 * 			<guild_id>: {
 * 				"guild_id": <guid>
 * 				"ann_ch": <ann_ch>
 * 				"ann_hdj_ch": <ann_hdj_ch>
 * 				"gen_ch": <gen_ch>
 * 			}, // Next Guild
 * 		},
 * 		"levels": {
 * 			<guild_id>: {
 * 				"guild_id": <guid>
 * 				"lvlup_msg": <lvlup_msg>
 * 				"lvlup_msg_ena": <lvlup_msg_ena>
 * 				"lvlup_msg_dm": <lvlup_msg_dm>
 * 				"banned_roles": <banned_roles>
 * 				"reward_roles": <reward_roles>
 * 				"reward_lvls": <reward_lvls>
 * 			}, // Next Guild
 * 		},
 * 		"modcom": {
 * 			<guild_id>: {
 * 				"guild_id": <guid>
 * 				"kick_ena": <kick_ena>
 * 				"kick_msg": <kick_msg>
 * 				"ban_ena": <ban_ena>
 * 				"ban_msg": <ban_msg>
 * 				"mute_ena": <mute_ena>
 * 				"mute_msg": <mute_msg>
 * 				"unmute_msg": <unmute_msg>
 * 				"banned_words": <banned_words>
 * 			}, // Next Guild
 * 		},
 * 		"quotes": {
 * 			<guild_id>: {
 * 				"guild_id": <guid>
 * 				"role_access": <role_access>
 * 			}, // Next Guild
 * 		},
 * 		"reaction": {
 * 			<guild_id>: {
 * 				"guild_id": <guid>
 * 				"hoi_ena": <hoi_ena>
 * 				"doei_ena": <doei_ena>
 * 				"honger_ena": <honger_ena>
 * 				"ty_ena": <ty_ena>
 * 				"pesten_ena": <pesten_ena>
 * 				"cooldog_ena": <cooldog_ena>
 * 			}, // Next Guild
 * 		},
 * 		"rip": {
 * 			<guild_id>: {
 * 				"guild_id": <guid>
 * 				"rip_ena": <rip_ena>
 * 				"rip_cv_ena": <rip_cv_ena>
 * 				"role_access": <role_access>
 * 			}, // Next Guild
 * 		},
 * 		"voicecom": {
 * 			<guild_id>: {
 * 				"guild_id": <guid>
 * 				"er_ena": <er_ena>
				"isnv_ena": <isnv_ena>
				"neuken_ena": <neuken_ena>
				"khnma_ena": <khnma_ena>
				"aandacht_ena": <aandacht_ena>
				"haha_ena": <haha_ena>
				"hjn_ena": <hjn_ena>
				"goeiemorgen_ena": <goeiemorgen_ena>
				"gadver_ena": <gadver_ena>
				"wakker_ena": <wakker_ena>
				"darkness_ena": <darkness_ena>
				"nigger_ena": <nigger_ena>
				"gay_ena": <gay_ena>
				"sb_ena": <sb_ena>
				"noot_ena": <noot_ena>
				"youdoyou_ena": <youdoyou_ena>
				"laf_ena": <laf_ena>
				"onion_ena": <onion_ena>
				"pls_ena": <pls_ena>
				"role_access": <role_access>
 			}, // Next Guild
 * 		},
 * 		"warnings": {
 * 			<guild_id>: {
 * 				"guild_id": <guid>
				"max_warn": <max_warn>
				"max_warn_ena": <max_warn_ena>
				"warn_ann_msg": <warn_ann_msg>
				"warn_ann_msg_ena": <warn_ann_msg_ena>
				"access_roles": <access_roles>
 * 			}, // Next Guild
 * 		},
 * 		"welcome": {
 * 			<guild_id>: {
 * 				"guild_id": <guid>
				"welcome_txt_ena": <welcome_txt_ena>
				"welcome_txt": <welcome_txt>
				"leave_txt_ena": <leave_txt_ena>
				"leave_txt": <leave_txt>
 * 			}, // Next Guild
 * 		},
 * 		"user_updates": {
 * 			<guild_id>: {
	 			"guild_id": <guid>
				"role_update_ena": <role_update_ena>
				"role_update_msg": <role_update_msg>
				"nickname_update_ena": <nickname_update_ena>
				"nickname_update_msg": <nickname_update_msg>
 * 			}, // Next Guild
 * 		}
 * 	}
 * }
 ***/
var settings = {
	bot: {
		"quotes": {},
		"ranks": {},
		"warnings": {}
	},
	web: {
		"dashboards": {},
		"guild_info": {},
		"levels": {},
		"modcom": {},
		"quotes": {},
		"reaction": {},
		"rip": {},
		"voicecom": {},
		"warnings": {},
		"welcome": {},
		"user_updates": {}
	}
}

// API Auth key storage
/***
 * Structure:
 * apiAuthKeys = {
 * 		<api_key>: {
 * 			userID: <user_id>,
 * 			key: <auth_key>,
 * 			expires: <date>
 * 		}
 * }
 ***/
var apiAuthKeys = {}
const apiAKfilename = "apiAK"
const adminReqFileName = "adminRequests"

/***
 * Structure:
 * nonLeave: {
 * 		<guild_id>: {
 * 			<user_id>: "kick" OR "ban" OR null/undefined
 * 		}
 * }
 ***/
var nonLeave = {}

var ipLists = {
	whitelist: [],
	blacklist: []
}

const alertTimeout = 15 // Timeout in minutes
const alertMsgs = {
	noSettings: "Hmm ik ken deze server nog niet volgens mijn papieren. Zorg ervoor dat je via de website (http://www.discord-frankie.nl/) een controle paneel hebt zodat je mijn diensten kan gebruiken.\nIk zal dit bericht elke 15 minuten (als er gepraat word tenminste) herhalen zodat je dit niet vergeet in te stellen\n\nDenk je dat dit een bug is? Ga dan even naar het Tevredenheids Bureau."
}
/**
 * Structure:
 * 	alertGuilds = {
 * 		<guild_id>: <expire timestamp in UNIX>, // Next guild
 * 	}
 */
var alertGuilds = {}

// Defining bot aka Eris Command Client
const bot = new Eris.CommandClient(botCred.token, {
	maxShards: 'auto'
}, {
	defaultHelpCommand: true,
	description: "Jouw vertrouwde vriend als het gaat om (hotel) management.",
	ignoreBots: true,
	ignoreSelf: true,
	name: botName,
	owner: "Androiddd#4682",
	prefix: botPrefix
})

// Default Functions
/***
 * randomInt - Generate a random number between the 2 givin numbers
 * @param {Number} low - Lowest number it can return
 * @param {Number} high - Highest number it can return
 * @return {Number} - Returns a random number between the 2 givin numbers
 ***/
function randomInt(low, high) {
	return Math.floor(Math.random() * (high - low + 1) + low)
}

/***
 * isEmpty - Checks if sting is empty (mostly used out of lazyness)
 * @param {String} str - String to be checked
 * @return {Boolean} - Returns true on empty, false on not empty
 ***/
function isEmpty(str) {
	return (!str || 0 === str.length);
}

/***
 * logger - Logs messages in a log file
 * @param {String} logType - Type of log. This also determains the filename for the log file
 * @param {String} logMsg - Messages that has to be logged
 * @return {Promise<Number>} - Returns a promise with a status code on resolve and a error code on reject
 ***/
function logger(logType, logMsg) {
	return new Promise(function(resolve, reject) {
		let dateNow = new Date()
		fs.writeFile(`./logs/${logType}_${dateformat(dateNow, "yyyy_m_d")}.log`, `[${dateformat(dateNow, "HH:MM:ss")}]${logMsg}\n`, {"flag": 'a'}, function(err) {
			if (err) {
				console.log(colorScheme.critical, `[Redact-LOG][Critical] Failed to save '${logType}.log'. Error:`, err)
				resolve(1)
			} else {
				resolve(0)
			}
		})
	})
}

// Custom Functions
function isGuild(guid_obj) {
    if (typeof guid_obj == 'undefined') {
    	return false
    } else {
    	return true
    }
}

/**
 * switchCheck() - Checks if module is enabled for a specific server
 * @param {String} guildID - ID of guild to find the settings of
 * @param {String} dashVar - Dashboard variable to check
 * @param {String} cat - Category to check in
 * @param {String} subCat - Subcategory to check
 * @returns {Promise<errCode>} - Returns error code on reject(), returns nothing on resolve()
 */
function switchCheck(guildID, dashVar, cat, subCat = null, ret = "promise") {
	if (ret == "promise") {
		return new Promise(function(resolve, reject) {
			if (settings.web.dashboards[guildID][dashVar] === true && settings.web[cat][guildID]) {
				if (subCat === null || settings.web[cat][guildID][subCat] === true) {
					resolve()
				} else {
					reject(2)
				}
			} else if (settings.web.dashboards[guildID][dashVar] === false) {
				reject(1)
			} else if (settings.web[cat][guildID] && settings.web[cat][guildID][subCat] === false) {
				reject(2)
			} else {
				reject(3)
			}
		})
	} else if (ret == "default") {
		if (settings.web.dashboards[guildID][dashVar] === true) {
			if (subCat === null || settings.web[cat][guildID][subCat] === true) {
				return 0
			} else {
				return 2
			}
		} else if (settings.web.dashboards[guildID][dashVar] === false) {
			return 1
		} else if (settings.web[cat][guildID][subCat] === false) {
			return 2
		} else {
			return 3
		}
	} else {
		console.log(colorScheme.critical, `[${moduleName}][Critical][${guildID}] An unexpected error occured with switchCheck(), ret is not 'default' OR 'promise'. See debug for other vars.`)
		debugMsg(`[`+moduleName+`][`+debugMode+`] dashVar: ${dashVar}`)
		debugMsg(`[`+moduleName+`][`+debugMode+`] cat: ${cat}`)
		debugMsg(`[`+moduleName+`][`+debugMode+`] subCat: ${subCat}`)
		debugMsg(`[`+moduleName+`][`+debugMode+`] ret: ${ret}`)
		return false
	}
}

function nonleaver(guildID, userID, action = null, del = false) {
	return new Promise(function(resolve, reject) {
		if (del === false) {
			if (!nonLeave[guildID]) {
				nonLeave[guildID] = {}
			}
			nonLeave[guildID][userID] = action
			resolve()
		} else if (del === true) {
			if (nonLeave[guildID] && nonLeave[guildID][userID]) {
				delete nonLeave[guildID][userID]
			}
			resolve()
		} else {
			reject("Del isn't a valid boolean.")
		}
	})
}

function nonleaverFind(guildID, userID) {
	return new Promise(function(resolve, reject) {
		if (nonLeave[guildID] && nonLeave[guildID][userID]) {
			if (nonLeave[guildID][userID] == "kick" || nonLeave[guildID][userID] == "ban") {
				resolve()
			} else {
				reject()
			}
		} else {
			reject()
		}
	})
}

function roleAccess(dbArr, memObj) {
	return new Promise(function(resolve, reject) {
		async.forEachOf(dbArr, (roleID, key, callback) => {
			if (memObj.roles.indexOf(roleID) != -1) {
				callback(true)
			} else {
				callback()
			}
		}, function(succ) {
			if (succ === true) {
				resolve()
			} else {
				reject()
			}
		})
	})
}

function randomObj(obj, ret = "obj") {
	let keys = Object.keys(obj)
	if (ret === "obj") {
		return obj[keys[ keys.length * Math.random() << 0]]
	} else if (ret === "key") {
		return keys[ keys.length * Math.random() << 0]
	}
}

function roleHighCheck(authorRoleArr, targetRoleArr, guildRoleColl) {
	return new Promise(function(resolve, reject) {
		let authorRoleHigh = 0
		let mentionedUserRoleHigh = 0
		async.forEachOf(authorRoleArr, (roleID, key, callback) => {
			if (guildRoleColl.get(roleID).position > authorRoleHigh) {
				authorRoleHigh = guildRoleColl.get(roleID).position
			}
			callback()
		}, function(err) {
			if (err) {
				reject(err)
			} else {
				async.forEachOf(targetRoleArr, (roleID, key, callback) => {
					if (guildRoleColl.get(roleID).position > mentionedUserRoleHigh) {
						mentionedUserRoleHigh = guildRoleColl.get(roleID).position
					}
					callback()
				}, function(err) {
					if (err) {
						reject(err)
					} else {
						if (authorRoleHigh > mentionedUserRoleHigh) {
							resolve()
						} else {
							reject()
						}
					}
				})
			}
		})
	})
}

function roleHighest(roleArr, guildID) {
	let highRole = null
	let roleColl = bot.guilds.get(guildID).roles
	let error = false
	let everyoneRole = null
	devMsg(`[roleHighest()][`+debugMode+`][${guildID}] Role collection: `, roleColl)
	async.forEachOf(roleArr, (roleID, _key, callback) => {
		let role = roleColl.get(roleID)
		devMsg(`[roleHighest()][`+debugMode+`][${guildID}] Got role: `, role)
		devMsg(`[roleHighest()][`+debugMode+`][${guildID}] Highest role is: `, highRole)
		if (role.name == "@everyone") {
			everyoneRole = role
		}
		if (highRole == null) {
			highRole = role
			callback()
		} else {
			if (highRole.position < role.position) {
				highRole = role
			}
			callback()
		}
	}, function(err) {
		if (err) {
			error = true
			console.log(colorScheme.critical, `[${moduleName}][Critical][${guildID}] An unexpected error has occured while running 'roleHighest()'. See debug for error.`)
			debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
			return
		}
	})
	if (error === true) {
		return null
	} else {
		if (highRole != null && highRole.color) {
			devMsg(`[roleHighest()][`+debugMode+`][${guildID}] Returning ${highRole.id}, full obj: `, highRole.color)
			return highRole.id
		} else {
			devMsg(`[roleHighest()][`+debugMode+`][${guildID}] Retuning 'everyoneRole' (which is default)`, everyoneRole)
			return null
		}
	}
}

fAlert = {
	noSettings: function(guildID) {
		devMsg(`[`+moduleName+`][`+debugMode+`] fAlert.noSettings ran for (${guildID}). alertGuilds: `, alertGuilds)
		let dateNow = new Date()
		if (alertGuilds[guildID]) {
			devMsg(`[`+moduleName+`][`+debugMode+`] Date already exist.`, alertGuilds[guildID])
			if (dateNow >= new Date(alertGuilds[guildID])) {
				devMsg(`[`+moduleName+`][`+debugMode+`] Date expired making new date and return true`)
				dateNow.setMinutes(dateNow.getMinutes() + alertTimeout)
				alertGuilds[guildID] = dateNow.valueOf()
				devMsg(`[`+moduleName+`][`+debugMode+`] New date code it:`, alertGuilds[guildID])
				return true
			} else {
				devMsg(`[`+moduleName+`][`+debugMode+`] Date not yet expired returning false`)
				return false
			}
		} else {
			dateNow.setMinutes(dateNow.getMinutes() + alertTimeout)
			alertGuilds[guildID] = dateNow.valueOf()
			devMsg(`[`+moduleName+`][`+debugMode+`] No date exist yet, creating new date and returning true. Timestamp: `, alertGuilds[guildID])
			return true
		}
	}
}

// Level and EXP system vars
/***
 * Structure of timeoutObj
 * 
 * 	timeoutObj: {
 * 		<guild_id>: {
 * 			<user_id>: <dateCode in UNIX>
 * 		}
 * 	}
 ***/
var timeoutObj = {}
const def_exp = 100
const minExp = 15
const maxExp = 25
const cooldown_spam_min = 1
const incres_exp = 60
// const decrease_rate = 2 // Is never being used

function expCalc(guildID, userID) {
	kDebugMsg(`[`+moduleName+`][`+debugMode+`] expCalc ran`)
	let currDate = new Date()
	devMsg(`[expCalc()][`+debugMode+`][${guildID}] Checking if timeoutObj[${guildID}] AND timeoutObj[${guildID}][${userID}] exists and this seems to be ${(timeoutObj[guildID]) ? true : false} AND ${(timeoutObj[guildID]) ? (timeoutObj[guildID][userID]) ? true : false : undefined}`)
	if (timeoutObj[guildID] && timeoutObj[guildID][userID]) {
		devMsg(`[expCalc()][`+debugMode+`] Checking if ${dateformat(new Date(timeoutObj[guildID][userID]), "yyyy-mm-dd HH:MM:ss")} > ${dateformat(currDate, "yyyy-mm-dd HH:MM:ss")} and this seems to be ${(new Date(timeoutObj[guildID][userID]) > currDate)}`)
		if (new Date(timeoutObj[guildID][userID]) > currDate) {
			devMsg(`[expCalc()][`+debugMode+`][${guildID}] Cooldown for user ${userID} isn't over yet. Returning...`)
			return // Cooldown isn't over yet
		}
	}
	if (!settings.bot.ranks[guildID]) {
		settings.bot.ranks[guildID] = {}
	}
	let insert = false
	if (!settings.bot.ranks[guildID][userID]) {
		settings.bot.ranks[guildID][userID] = {
			id: null,
			guild_id: guildID,
			user_id: userID,
			exp: 0,
			total_exp: 0,
			level: 0,
			op_pause: 0
		}
		insert = true
	}
	let userRank = settings.bot.ranks[guildID][userID]
	if (userRank.op_pause === 1) {
		devMsg(`[expCalc()][`+debugMode+`][${guildID}] User ${userID} is stopped by mod+ from receive EXP. Returning...`)
		return // Is stopped by mod+ from receive EXP
	}
	let level = userRank.level
	let levelUp = false
	let earnedEXP = randomInt(minExp, maxExp)
	let earnedEXP4Calc = userRank.exp + earnedEXP
	let newLevelGoal = math.eval(`${def_exp} + (${incres_exp} * ${userRank.level})`)
	// Level up if goal has been reached
	if (earnedEXP4Calc >= newLevelGoal) {
		devMsg(`[expCalc()][`+debugMode+`][${guildID}] New level reached for user ${userID}`)
		earnedEXP4Calc -= newLevelGoal
		level += 1
		levelUp = true
	}
	// Update database
	let SQL = `UPDATE \`ranks\` SET exp=${earnedEXP4Calc}, total_exp=total_exp+${earnedEXP}, level=${level} WHERE guild_id=${guildID} AND user_id=${userID}`
	if (insert === true) {
		devMsg(`[expCalc()][`+debugMode+`][${guildID}] Using INSERT SQL instead of UPDATE of user ${userID}`)
		SQL = `INSERT INTO \`ranks\` (guild_id, user_id, exp, total_exp, level) VALUES ('${guildID}', '${userID}', '${earnedEXP4Calc}', '${earnedEXP}', '${level}')`
	}
	mysql_con_bot.query(SQL, function(err, result) {
		if (err) {
			console.log(colorScheme.critical, `[MySQL][Critical][${guildID}] SQL ${(insert === true ? "INSERT" : "UPDATE")} failed. See debug for error.`)
			debugMsg(`[`+moduleName+`][`+debugMode+`] MySQL error: `, err)
			return
		} else {
			// Update local ranking settings
			if (insert === true) {
				devMsg(`[expCalc()][`+debugMode+`][${guildID}] Giving user ${userID} correct db id (${result.insertId}) for rank`)
				settings.bot.ranks[guildID][userID].id = result.insertId
			}
			settings.bot.ranks[guildID][userID].exp = earnedEXP4Calc
			settings.bot.ranks[guildID][userID].total_exp += earnedEXP
			settings.bot.ranks[guildID][userID].level = level
			// Reward Role (if set)
			if (settings.web.levels[guildID].reward_roles[0] && settings.web.levels[guildID].reward_lvls[0]) {
				let rewardRoles = settings.web.levels[guildID].reward_roles
				let rewardLvls = settings.web.levels[guildID].reward_lvls
				devMsg(`[expCalc()][`+debugMode+`][${guildID}] Reward Roles: `, rewardRoles)
				devMsg(`[expCalc()][`+debugMode+`][${guildID}] Reward Levels: `, rewardLvls)
				for (let i = 0; i < rewardRoles.length; i++) {
					devMsg(`[expCalc()][`+debugMode+`][${guildID}] Checking if ${level} == ${rewardLvls[i]} and it seems to be ${(level == rewardLvls[i])}`)
					if (level == rewardLvls[i] && rewardLvls[i] != 0) {
						bot.addGuildMemberRole(guildID, userID, rewardRoles[i], `Heeft level ${level} bereikt!`).catch((error) => {
							if (settings.web.guild_info[guildID]) {
								bot.createMessage(settings.web.guild_info[guildID].ann_hdj_ch, `Sorry voor het storen maar het is mij niet gelukt om <@${userID}> zijn nieuwe role \`${bot.guilds.get(guildID).roles.get(rewardRoles[i]).name}\` te geven.\nGelieve dit handmatig doen en controlleren of ik de juiste rechten hebt om dit te kunnen doen.`).catch((error) => {
									console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
								})
							}
							console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Role couldn't be given. Error:`, error)
						})
					}
				}
			}
			// Congratz the user (when needed)
			devMsg(`[expCalc()][`+debugMode+`][${guildID}] Checking if switchCheck(${guildID}, "levels_ena", "levels", "lvlup_msg_ena", "default") is true and this seems to be ${switchCheck(guildID, "levels_ena", "levels", "lvlup_msg_ena", "default")} also checking if levelUp is true and this seems to be ${levelUp}.`)
			if (switchCheck(guildID, "levels_ena", "levels", "lvlup_msg_ena", "default") == 0 && levelUp === true) {
				devMsg(`[expCalc()][`+debugMode+`][${guildID}] Congratzing user ${userID}!`)
				if (settings.web.guild_info[guildID]) {
					bot.createMessage(settings.web.guild_info[guildID].gen_ch, settings.web.levels[guildID].lvlup_msg.replace(player_web_reg, `<@${userID}>`).replace(level_web_reg, level)).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
				}
			}
			devMsg(`[expCalc()][`+debugMode+`][${guildID}] Checking if switchCheck(${guildID}, "levels_ena", "levels", "lvlup_msg_dm", "default") is true and this seems to be ${switchCheck(guildID, "levels_ena", "levels", "lvlup_msg_dm", "default")} also checking if levelUp is true and this seems to be ${levelUp}.`)
			if (switchCheck(guildID, "levels_ena", "levels", "lvlup_msg_dm", "default") == 0 && levelUp === true) {
				devMsg(`[expCalc()][`+debugMode+`][${guildID}] Congratzing user ${userID} in DM!`)
				bot.getDMChannel(userID).then((pChan) => {
					pChan.createMessage(`${settings.web.levels[guildID].lvlup_msg.replace(player_web_reg, `<@${userID}>`).replace(level_web_reg, level)}\n\n*Dit heb je behaald op de ${bot.guilds.get(guildID).name} server*`).catch((error) => {
						console.log(colorScheme.Info, `[DiscordShard][Info][${msg.channel.guild.id}] Private Message couldn't be sent (ranks congratz dm). Error:`, error)
					})
				})
			}
			// Give timeout
			if (!timeoutObj[guildID]) {
				devMsg(`[expCalc()][`+debugMode+`][${guildID}] Timeout object for guild ${guildID} doesn't exist yet. Creating...`)
				timeoutObj[guildID] = {}
			}
			devMsg(`[expCalc()][`+debugMode+`][${guildID}] Gave user ${userID} ${cooldown_spam_min} minute cooldown from receiving EXP`)
			timeoutObj[guildID][userID] = currDate.setMinutes(currDate.getMinutes() + cooldown_spam_min)
		}
	})
}

function apiSemiKeysIO(task = "write", echoing = true) {
	return new Promise(function(resolve, reject) {
		if (task == "write") {
			let semiKeysObj = {}
			async.forEachOf(apiAuthKeys, (keyObj, key, callback) => {
				if (keyObj.expires == "semi-permanent") {
					semiKeysObj[key] = keyObj
				}
				callback()
			}, function() {
				fs.writeFile(`./credentials/apiSemiKeys.json`, JSON.stringify(semiKeysObj), function(err) {
					if (err) {
						if (echoing === true) {
							console.log(colorScheme.critical, `[${moduleName}][Critical] Failed to save API Semi Keys to file! Please fix this in case of a crash!`)
							console.log(colorScheme.critical, `[${moduleName}][Critical-Message] ${err}`)
						}
						reject(1)
					} else {
						if (echoing === true) {
							debugMsg(`[${moduleName}][`+debugMode+`] API Semi Keys saved to file!`)
						}
						resolve(0)
					}
				})
			})
		} else if (task == "load") {
			fs.readFile(`./credentials/apiSemiKeys.json`, (err, data) => {
				if (err) {
					if (err.message.indexOf("no such file or directory") != -1) {
						console.log(colorScheme.warning, `[${moduleName}][Warning] File 'apiSemiKeys.json' doesn't seem to exists. Creating a new file with default values!`)
						fs.writeFile(`./credentials/apiSemiKeys.json`, JSON.stringify({}), function(err) {
							if (err) {
								console.log(colorScheme.error, `[${moduleName}][Error] Failed to create the file! Please fix this in order for this script to run.`)
								console.log(colorScheme.error, `[${moduleName}][Error-Message] ${err}`)
								reject(5)
							} else {
								console.log(colorScheme.warning, `[${moduleName}][Warning] New 'apiSemiKeys.json' file has been created successfully!`)
								resolve(2)
							}
						})
					} else {
						console.log(colorScheme.error, `[${moduleName}][Error] Failed to load API Semi Keys from file! Please fix this to load the API Semi Keys!`)
						console.log(colorScheme.error, `[${moduleName}][Error-Message] ${err}`)
						reject(1)
					}
				} else {
					try {
						apiAuthKeys = JSON.parse(data)
						resolve(0)
					} catch (error) {
						console.log(colorScheme.critical, `[${moduleName}][Critical] 'apiSemiKeys.json' content is not a JSON! Please correct this issue if you want to load the API Semi Keys!`)
						debugMsg(`[`+moduleName+`][`+debugMode+`] Error messages: ${error}`)
						reject(3)
					}
				}
			})
		} else {
			reject(6)
		}
	})
}

function apiKeyFinder(userID, find = "access") {
	return new Promise(function(resolve, reject) {
		async.forEachOf(apiAuthKeys, (keyObj, authKey, callback) => {
			if (keyObj.userID == userID && keyObj.expires != "semi-permanent") {
				if (find == "access") {
					callback(true)
				} else if (find == "key") {
					callback(authKey)
				} else {
					callback(true)
				}
			} else if (keyObj.userID == userID && keyObj.expires == "semi-permanent") {
				if (find == "access") {
					callback(true)
				} else if (find == "semiKey") {
					callback(authKey)
				} else {
					callback(true)
				}
			} else {
				callback()
			}
		}, function(res) {
			if (res) {
				if (find == "access") {
					resolve(res)
				} else if (find == "key" || find == "semiKey") {
					resolve(res)
				} else {
					resolve()
				}
			} else {
				if (find == "access") {
					reject(false)
				} else if (find == "key" || find == "semiKey") {
					reject()
				} else {
					reject()
				}
			}
		})
	})
}

// Vars, arrays and objects 
// Requires (ext vars)
const developers = require("./vars/developers.js")
const credits = require("./vars/credits.js")
const apiAdmins = require("./vars/apiAdmins.js")

// Internal Vars
var botID = botCred.botID
const thisisfrankie_url = "https://habbofever.be/wp-content/uploads/2014/06/Frank_mit_Kabeln.png"
const soundDefLoc = "./sounds/"
const soundsObj = [
	{
		name: "placeholder",
		alias: [],
		succReac: "",
		failReac: "",
		soundFile: "JM_JER.opus"
	},
	{
		name: "even rustig!",
		alias: ["er"],
		switch: "er_ena",
		succReac: "Vind ik ook {player}.",
		failReac: "Dat moet jij zeker doen {player}.",
		soundFile: "JM_JER.opus"
	},
	{
		name: "snap het niet",
		alias: ["ik snap het niet", "isnv"],
		switch: "isnv_ena",
		succReac: "Ik snap het ook niet hoor, {player}.",
		failReac: "Ik snap **jouw** ook niet :stuck_out_tongue: {player}.",
		soundFile: "JM_ISNV.opus"
	},
	{
		name: "neuken",
		alias: [],
		switch: "neuken_ena",
		succReac: "NEUKEN NEUKEN!",
		failReac: "Doe is even normaal, {player}!",
		soundFile: "JM_NN.opus"
	},
	{
		name: "ik kan het niet meer aan",
		alias: ["khnma"],
		switch: "khnma_ena",
		succReac: "Ik snap je helemaal {player}.",
		failReac: "Dan ga je naar een therapeut {player}.",
		soundFile: "JM_KHNMA.opus"
	},
	{
		name: "mag ik even de aandacht?",
		alias: ["mag ik even de aandacht", "aandacht"],
		switch: "aandacht_ena",
		succReac: "Tuurlijk mag jij dat {player}.",
		failReac: "Nee, zorg daar zelf maar voor {player}.",
		soundFile: "JM_EA.opus"
	},
	{
		name: "haha",
		alias: [],
		switch: "haha_ena",
		succReac: "HAHAHAHAHAHA.\nDat is grappig :smile:",
		failReac: "Ik vind jouw niet grappig {player}..",
		soundFile: "JM_HA.opus"
	},
	{
		name: "ik hoor het niet",
		alias: ["hjn"],
		switch: "hjn_ena",
		succReac: "Ik hoor ze ook niet hoor.\nHet ligt niet alleen aan jouw.",
		failReac: "Wat zei je?\nIk hoor je niet, ik hoor je niet, ik hoor, ik hoor, ik hoor je niet,\nik hoor je niet, ik hoor je niet, ik kan je niet verstaan.",
		soundFile: "JM_HJN.opus"
	},
	{
		name: "goeiemorgen",
		alias: [],
		switch: "goeiemorgen_ena",
		succReac: "GOEDEMORGEN POSTCODE KANJER!",
		failReac: "Ook goeiemorgen {player}.",
		soundFile: "JM_GM.opus"
	},
	{
		name: "gadverdamme",
		alias: ["gadver"],
		switch: "gadver_ena",
		succReac: "Dat is zeker smerig {player}.",
		failReac: "GADVERDAMME {player}, GADVERDAMME!",
		soundFile: "JM_GOMA.opus"
	},
	{
		name: "wakker worden",
		alias: ["wakker"],
		switch: "wakker_ena",
		succReac: "Inderdaad het is tijd om wakker te worden {player}!!!",
		failReac: "Nee! Ik slaap nog ja!",
		soundFile: "JM_WAKKER.opus"
	},
	{
		name: "nigger",
		alias: ["neger"],
		switch: "nigger_ena",
		succReac: "Vind jij DAT nou normaal {player}?",
		failReac: "Oi! Normaal doen hoor {player}!",
		soundFile: "NIGGER.opus"
	},
	{
		name: "i'm gay",
		alias: ["ik ben homo", "gay", "homo"],
		switch: "gay_ena",
		succReac: "Ja maar tymon...",
		failReac: "Ik ben blij dat je uit de kast gekomen bent {player}.",
		soundFile: "IMGAY.opus"
	},
	{
		name: "spongebob",
		alias: ["sb"],
		switch: "sb_ena",
		succReac: "OHHHHHHH!!!!!",
		failReac: "JAAAAA tymon!! Spongebob!",
		soundFile: "sp_er.opus"
	},
	{
		name: "noot noot",
		alias: ["noot"],
		switch: "noot_ena",
		succReac: "NOOT NOOT MOTHERF-----",
		failReac: "Send noots plz {player}\nhttp://images-cdn.9gag.com/photo/aOdPRZD_700b.jpg",
		soundFile: "pingu_noot.opus"
	},
	{
		name: "you do you",
		alias: ["youdoyou"],
		switch: "youdoyou_ena",
		succReac: "Jij doet jij, ik doe mij en we doen niet elkaar.\nOke {player}?",
		failReac: "Dat zal ik zeker doen {player}.",
		soundFile: "Markiplier_FNAF_DO.opus"
	},
	{
		name: "ladies and fricks",
		alias: ["laf"],
		switch: "laf_ena",
		succReac: "..ja allemaal even luisteren aub!",
		failReac: "Hé! Even normaal doen jij!",
		soundFile: "chadtronic_laf.opus"
	},
	{
		name: "onion",
		alias: [],
		switch: "onion_ena",
		succReac: "Gefeliciteerd?",
		failReac: "SHREKT YA!",
		soundFile: "MARK_Union.opus"
	},
	{
		name: "please",
		alias: ["pls", "plz"],
		switch: "pls_ena",
		succReac: "Wie?",
		failReac: "Misschien {player}...\nMisschien",
		soundFile: "PEWDIEPIE_MenatiaPlease.opus"
	},
	{
		name: "honger",
		alias: [],
		switch: "eastereggs_ena",
		succReac: "Ga naar de keuken en kijk of er eten ligt\n\n*Easter Egg #1*",
		failReac: "",
		soundFile: "EtenHalen.opus"
	},
	{
		name: "what?!",
		alias: ["wtf?!", "wwtf"],
		switch: "eastereggs_ena",
		succReac: "Wat?! Wat de ----?!?!\n\n*Easter Egg #2 gevonden.*",
		failReac: "",
		soundFile: "JT_WWTF.opus"
	},
	{
		name: "wtf richard",
		alias: ["wtfr"],
		switch: "eastereggs_ena",
		succReac: "Richard... Waarom..?\n\n*Easter Egg #3 gevonden.*",
		failReac: "",
		soundFile: "WTFR.opus"
	},
	{
		name: "triggered",
		alias: [],
		switch: "eastereggs_ena",
		succReac: "Oh ja zo noemen jullie millennials dat..\n\n*Easter Egg #4 gevonden.*",
		failReac: "",
		soundFile: "Triggered.opus"
	},
]

const hello_text = [
	"hey <@{botid}>",
	"hoi <@{botid}>",
	"hallo <@{botid}>",
	"oi <@{botid}>",
	"yo <@{botid}>",
]

const hello_resp = [
	"Hey",
	"Hoi",
	"Hallo"
]

const bye_text = [
	"doei <@{botid}>",
	"tot zo <@{botid}>",
	"dag <@{botid}>",
	"bye <@{botid}>"
]

const bye_resp = [
	"Doei",
	"Tot zo",
	"Dag",
	"Bye"
]

const ty_text = [
	"dankje <@{botid}>",
	"bedankt <@{botid}>",
	"ty <@{botid}>",
	"thx <@{botid}>",
	"thanks <@{botid}>",
]

const ty_resp = [
	"Geen probleem",
	"Fijn dat ik kon helpen",
	"Alsjeblieft",
	"Dat is mijn werk",
	"No problem",
	"No problemó",
]

const quote_desc = [
	`Hier heb ik een quote op pagina {randomPage}.`,
	`Deze is goed op pagina {randomPage}.`,
	`Op pagina {randomPage} staat een leuke.`,
	`Me boek ligt nog open op pagina {randomPage}.`,
]

const ripText = {
	"rip": [
		"Rest in peace",
		"Rest in pieces",
		"Rest in pepperonis",
		"Rest in BIEM",
		"Rest in kebab"
	],
	"death": [
		"dood gegaan",
		"dead not big suprise",
		"overleden",
		"heeft deze wereld verlaten",
		"de pijp uitgegaan"
	],
	"story": [
		"Men zegt dat na regen de zon schijnt dat zelfs het verdriet verdwijnt\nToch kunnen zelfs de grootste krachten de pijn niet zomaar verzachten\nWant enkel wij kunnen verstaan hoe het voelt om zonder jou verder te gaan.",
		"Als jij er niet meer bent, zal ik je heel hard missen..\nJij die alles kent, en ik die die me overal in kan vergissen..\nik denk dat het nooit went, dat jij er gewoon niet meer bent ...",
		"Het mocht niet langer duren\nniet voor jou en niet voor ons\nhet geeft een leeg en onbegrijpelijk gevoel\nhet waarom zal nooit beantwoord worden\nmen zegt alles heeft een doel\nhet was een mooi leven zo vol van levenslust\nmaar toch niet afgemaakt en zomaar weggerukt\nwe zullen het moeten accepteren of we nu willen of niet\nhet is niet eerlijk\nhet doet zo'n pijn\nje was zo lief altijd\nwe hebben van je genoten\nje levenslust, je liefde\nje spontaniteit\nje was een doorzetter altijd\nen genoot van ieder moment\nwe maakte de grootste lol\nwe noemen je ons zonnetje, altijd opgewekt en blij.",
		"De leegte die je achterliet is\nnog steeds ondragelijk groot\nenkel gevuld met goede herinneringen\nom je te koesteren en foto's\nom met ontroering naar te kijken\nen terug te denken aan.\ndat fijne mens die jij was!",
		"Elk zuchtje wind\ndat mijn gezicht streelt\nis een knuffel van mijn engelenkind\ndat nu tussen de wolken speelt"
	]
}

const ranking_desc = [
	"Nou {player} de score staat op dit moment als volgt.",
	"...ja inderdaad {player} dat was een geweldige uitvoering en de score staat als volgt.",
	"Wat was dat geweldig. Hier is de score.",
	"De stand staat als volgt."
]

const rank_desc = [
	"Dit is hoe jij er nu voor staat.",
	"Blijven je best doen.",
	"Als je de top berijkt krijg je een :cookie:."
]

const warning_give = [
	":rotating_light: We zijn in een achtervolging met {player}! We hebben alvast een waarschuwing klaar staan.",
	":soccer: Das een gele kaart {player}!",
	":flag_au: Oi mate! Terug komen jij, je hebt een waarschuwing te pakken.",
	":oncoming_police_car: Wil jij eens even uit leggen waarom jij te hard reed?"
]

const warning_limit = [
	"Ik heb goed nieuws en slecht nieuws.",
	"JA JA DAMES EN HEREN DE POSTCODE KNALLER IS GEVALLEN OP...",
	":rotating_light: ALARM! Het waarschuwings limiet is berijkt! :rotating_light:",
	"Het is weer sjekkie time! Ja ja dames en heren!",
	"In mijn systeem komt voor dat het waarschuwings limiet is berijkt is."
]

const warning_list = [
	"Heb ik me bonus al verdiend?",
	"Die laatste was echt een gedoe.",
	"Ja die nummer #1 moet echt oppassen!",
	"Het lijkt wel een race joh.",
	"Ik ben me ban hammer al aan het poetsen.",
	"Ben me kick schoenen al aan het pakken."
]

const version_desc = [
	"Beep boop! Dit is mijn versie info. :robot:",
	"Zo dus jij wilt mijn versie info weten? MAG NIE!",
	"Met *bzzz* mij gaat alles *bzzz* goed.\n*bzzz* Hoezo? Denk *bzzz* je van *bzzz* niet dan? *bzzz*",
	"Ja maar Tymon, dat wil ik niet."
]

// RegExes
const honger_reg = new RegExp("honger", 'i')
const honger2_reg = new RegExp("wil eten", 'i')
const rip_reg = new RegExp("rip", 'i')
const winx_reg = new RegExp("winx", 'i')
const w1nx_reg = new RegExp("w!nx", 'i')
const winks_reg = new RegExp("winks", 'i')
const w1nks_reg = new RegExp("w1nks", 'i')
const kut_reg = new RegExp("kut", 'i')
const frank_reg = new RegExp("frank", 'i')
const frankie_reg = new RegExp("frankie", 'i')
const kanker_reg = new RegExp("kanker", 'i')
const hjb_reg = new RegExp("hjb", 'i')
const hjb_f_reg = new RegExp("hou je", 'i')
const hjb_f2_reg = new RegExp("houd je", 'i')
const pop_reg = new RegExp("pop", 'i')
const pixie_reg = new RegExp("pixie", 'i')
const doekni_reg = new RegExp("doekni", 'i')
const beyonce_reg = new RegExp("beyonce", 'i')
const gepest_reg = new RegExp("gepest", 'i')
const kapot_reg = new RegExp("kapot", 'i')
const oprotten_reg = new RegExp("oprotten", 'i')

const player_web_reg = new RegExp("{player}", 'gm')
const oldnick_web_reg = new RegExp("{oudenaam}", 'gm')
const newnick_web_reg = new RegExp("{nieuwenaam}", 'gm')
const ranks_web_reg = new RegExp("{rank}", 'gm')
const warnings_web_reg = new RegExp("{warnings}", 'gm')
const maxWarnings_web_reg = new RegExp("{maxWarnings}", 'gm')
const level_web_reg = new RegExp("{level}", 'gm')
const servernaam_web_reg = new RegExp("{servernaam}", 'gm')

const randomPage_reg = new RegExp("{randomPage}", 'gm')
const botid_reg = new RegExp("{botid}", 'gm')

bot.on("ready", () => { // When the bot is ready
	console.log(colorScheme.connectionIS, `[${moduleName}][Info] Token log in as '${bot.user.username}#${bot.user.discriminator}'.`)

	let gitInfInit = function() {
		return new Promise(function(resolve, reject) {
			let cbg = function() {
				// Get current branch
				return new Promise(function(resolve, reject) {
					git().branchLocal((err, branchSumm) => {
						if (err) {
							reject(err)
						} else {
							devMsg(`[`+moduleName+`][`+debugMode+`] Found git's local branch info: `, branchSumm)
							debugMsg(`[Git][`+debugMode+`] Working branch '${branchSumm.current}'`)
							resolve(branchSumm.current)
						}
					})
				})
			}
			let lgc = function() {
				return new Promise(function(resolve, reject) {
					// Latest git commit
					git().log((err, log) => {
						if (err) {
							reject(err)
						} else {
							devMsg(`[bot.on("ready")/lgc()][`+debugMode+`] Found git log: `, log)
							let logKey = null
							for (let i = 0; i < log.all.length; i++) {
								devMsg(`[bot.on("ready")/lgc()][`+debugMode+`] Checking if (log.all[${i}].message.indexOf(\`gitlab/${versionInfo.gitBranch}\`) != -1) and this seems to be ${(log.all[i].message.indexOf(`gitlab/${versionInfo.gitBranch}`) != -1) ? true : false}, Current message: `, log.all[i].message)
								if (log.all[i].message.indexOf(`gitlab/${versionInfo.gitBranch}`) != -1) {
									logKey = i
									devMsg(`[bot.on("ready")/lgc()][`+debugMode+`] Found the logKey!`, logKey)
								}
							}
							if (logKey != null) {
								let message = log.all[logKey].message
								let HEADindexOf = log.all[logKey].message.indexOf(`(HEAD -> ${versionInfo.gitBranch}`)
								if (HEADindexOf != -1) {
									message = log.all[logKey].message.slice(0, HEADindexOf-1)
								}
								versionInfo.latestUpdate = {
									name: message,
									date: new Date(log.all[logKey].date),
									hash: log.all[logKey].hash.slice(0, 8),
									fullHash: log.all[logKey].hash,
									commitUrl: `https://gitlab.com/android4682/frankie/commit/${log.all[logKey].hash}`
								}
								debugMsg(`[Git][`+debugMode+`] Latest update '${versionInfo.latestUpdate.hash}'`)
								resolve()
							} else {
								reject()
							}
						}
					})
				})
			}
			cbg().then((cB) => {
				versionInfo.gitBranch = cB
				lgc().then(() => {
					versionInfo.version = `${versionInfo.versionNumber} (${versionInfo.gitBranch})`
					resolve()
				}).catch((err) => {
					if (err) {
						console.log(colorScheme.error, `[Git][Error] Failed to get the latest git commit info. Error: ${err}`)
						process.exit(45)
						reject()
					} else {
						console.log(colorScheme.error, `[Git][Error] Couldn't find the latest ${versionInfo.gitBranch} update info on remote.`)
						process.exit(46)
						reject()
					}
				})
			}).catch((err) => {
				console.log(colorScheme.error, `[Git][Error] Failed to get the local branch. Error: ${err}`)
				process.exit(47)
				reject()
			})
		})
	}

	gitInfInit().then(() => {
		console.log(colorScheme.purple, `[${moduleName}][Info] Running '${botName}' on version '${versionInfo.version}'.`)
		botID = bot.user.id

		for (let i = 0; i < hello_text.length; i++) {
			hello_text[i] = hello_text[i].replace(botid_reg, botID)
		}
		debugMsg(`[`+moduleName+`][`+debugMode+`] Sorted out 'hello_text' variable`)
		for (let i = 0; i < bye_text.length; i++) {
			bye_text[i] = bye_text[i].replace(botid_reg, botID)
		}
		debugMsg(`[`+moduleName+`][`+debugMode+`] Sorted out 'bye_text' variable`)
		for (let i = 0; i < ty_text.length; i++) {
			ty_text[i] = ty_text[i].replace(botid_reg, botID)
		}
		debugMsg(`[`+moduleName+`][`+debugMode+`] Sorted out 'ty_text' variable`)
		console.log(colorScheme.execSuccess, `[${moduleName}][Info] Variables sorted.`)

		mysql_con_bot = mysql.createPool({
			connectionLimit : mysqlData.connectionLimit,
			host            : mysqlData.host,
			user            : mysqlData.username,
			password        : mysqlData.password,
			database        : mysqlData.database
		})
		mysql_con_web = mysql.createPool({
			connectionLimit : mysqlData.connectionLimit,
			host            : mysqlData.host,
			user            : mysqlData.username,
			password        : mysqlData.password,
			database        : mysqlData.web_database
		})
		mysql_con_bot.getConnection(function(err, connection) {
			if (err) {
				console.error(colorScheme.error, `[MySQL][Error] Couldn't connect to the MySQL server DB: ${mysqlData.database}!`)
				debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
				process.exit(1)
			} else {
				console.log(colorScheme.connectionIS, `[MySQL][Info] Connected to MySQL server DB: ${mysqlData.database}.`)
			}
		})
		mysql_con_web.getConnection(function(err, connection) {
			if (err) {
				console.error(colorScheme.error, `[MySQL][Error] Couldn't connect to the MySQL server DB: ${mysqlData.web_database}!`)
				debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
				process.exit(2)
			} else {
				console.log(colorScheme.connectionIS, `[MySQL][Info] Connected to MySQL server DB: ${mysqlData.web_database}.`)
			}
		})
		let init_bot = function() {
			return new Promise(function(resolve, reject) {
				let init_db1 = function() {
					return new Promise(function(resolve, reject) {
						mysql_con_bot.query("SELECT * FROM `quotes` ORDER BY `guild_id` ASC", function(err, rows, fields) {
							if (err) {
								console.log(colorScheme.error, `[${moduleName}][Error] Error while trying to load 'quotes' from database.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
								process.exit(3)
							} else {
								async.forEachOf(rows, (row, key, callback) => {
									if (!settings.bot.quotes[row.guild_id]) {
										settings.bot.quotes[row.guild_id] = {}
									}
									if (!settings.bot.quotes[row.guild_id][row.user_id]) {
										settings.bot.quotes[row.guild_id][row.user_id] = []
									}
									settings.bot.quotes[row.guild_id][row.user_id].push({
										"id": row.id,
										"guild_id": row.guild_id,
										"quote": row.quote,
										"user_id":  row.user_id,
										"timestamp": row.timestamp,
										"year_k": row.year_k
									})
									callback()
								}, function(err) {
									if (err) {
										console.log(colorScheme.error, `[${moduleName}][Error] An error has occured while saving 'quotes' to memory.`)
										debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
										process.exit(4)
									} else {
										resolve()
									}
								})
							}
						})
					})
				}
				let init_db2 = function() {
					return new Promise(function(resolve, reject) {
						mysql_con_bot.query("SELECT * FROM `ranks` ORDER BY `guild_id` ASC", function(err, rows, fields) {
							if (err) {
								console.log(colorScheme.error, `[${moduleName}][Error] Error while trying to load 'ranks' from database.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
								process.exit(5)
							} else {
								async.forEachOf(rows, (row, key, callback) => {
									if (!settings.bot.ranks[row.guild_id]) {
										settings.bot.ranks[row.guild_id] = {}
									}
									settings.bot.ranks[row.guild_id][row.user_id] = {
										"id": row.id,
										"guild_id": row.guild_id,
										"user_id": row.user_id,
										"exp":  row.exp,
										"total_exp": row.total_exp,
										"level": row.level,
										"op_pause": row.op_pause
									}
									callback()
								}, function(err) {
									if (err) {
										console.log(colorScheme.error, `[${moduleName}][Error] An error has occured while saving 'ranks' to memory.`)
										debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
										process.exit(6)
									} else {
										resolve()
									}
								})
							}
						})
					})
				}
				let init_db3 = function() {
					return new Promise(function(resolve, reject) {
						mysql_con_bot.query("SELECT * FROM `warnings` ORDER BY `guild_id` ASC", function(err, rows, fields) {
							if (err) {
								console.log(colorScheme.error, `[${moduleName}][Error] Error while trying to load 'warnings' from database.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
								process.exit(7)
							} else {
								async.forEachOf(rows, (row, key, callback) => {
									if (!settings.bot.warnings[row.guild_id]) {
										settings.bot.warnings[row.guild_id] = {}
									}
									if (!settings.bot.warnings[row.guild_id][row.user_id]) {
										settings.bot.warnings[row.guild_id][row.user_id] = []
									}
									settings.bot.warnings[row.guild_id][row.user_id].push({
										"id": row.id,
										"guild_id": row.guild_id,
										"user_id": row.user_id,
										"reason":  row.reason,
										"timestamp": row.timestamp,
										"given_by_id": row.given_by_id
									})
									callback()
								}, function(err) {
									if (err) {
										console.log(colorScheme.error, `[${moduleName}][Error] An error has occured while saving 'warnings' to memory.`)
										debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
										process.exit(8)
									} else {
										resolve()
									}
								})
							}
						})
					})
				}
				init_db1().then(() => {
					debugMsg(`[`+moduleName+`][`+debugMode+`] 'quotes' loaded from MySQL database to memory`)
					init_db2().then(() => {
						debugMsg(`[`+moduleName+`][`+debugMode+`] 'ranks' loaded from MySQL database to memory`)
						init_db3().then(() => {
							debugMsg(`[`+moduleName+`][`+debugMode+`] 'warnings' loaded from MySQL database to memory`)
							resolve()
						}).catch((err) => {
							console.log(colorScheme.error, `[${moduleName}][Error] An unexpected error occurred while running init_db3().`)
							debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
							process.exit(11)
						})
					}).catch((err) => {
						console.log(colorScheme.error, `[${moduleName}][Error] An unexpected error occurred while running init_db2().`)
						debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
						process.exit(10)
					})
				}).catch((err) => {
					console.log(colorScheme.error, `[${moduleName}][Error] An unexpected error occurred while running init_db1().`)
					debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
					process.exit(9)
				})
			})
		}
		let init_webData = function() {
			return new Promise(function(resolve, reject) {
				let init_db1 = function() {
					return new Promise(function(resolve, reject) {
						mysql_con_web.query("SELECT * FROM `dashboards` ORDER BY `guid` ASC", function(err, rows, fields) {
							if (err) {
								console.log(colorScheme.error, `[${moduleName}][Error] Error while trying to load 'dashboards' from database.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
								process.exit(12)
							} else {
								async.forEachOf(rows, (row, key, callback) => {
									// Lines that are commented out are unnecessary
									settings.web.dashboards[row.guid] = {
										"guild_id": row.guid,
										// "user_ids": row.user_ids,
										// "permissions":  row.permissions,
										// "date_code": row.date_code,
										"levels_ena": (row.levels_ena == "on") ? true : false,
										"modcom_ena": (row.modcom_ena == "on") ? true : false,
										"voicecom_ena": (row.voicecom_ena == "on") ? true : false,
										"reaction_ena": (row.reaction_ena == "on") ? true : false,
										"roles_ena": (row.roles_ena == "on") ? true : false, // This is now used for user_updates
										"quotes_ena": (row.quotes_ena == "on") ? true : false,
										"warnigns_ena": (row.warnigns_ena == "on") ? true : false,
										"welcome_ena": (row.welcome_ena == "on") ? true : false,
										"rip_ena": (row.rip_ena == "on") ? true : false
									}
									callback()
								}, function(err) {
									if (err) {
										console.log(colorScheme.error, `[${moduleName}][Error] An error has occured while saving 'dashboards' to memory.`)
										debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
										process.exit(13)
									} else {
										resolve()
									}
								})
							}
						})
					})
				}
				let init_db2 = function() {
					return new Promise(function(resolve, reject) {
						mysql_con_web.query("SELECT * FROM `guild_info` ORDER BY `guid` ASC", function(err, rows, fields) {
							if (err) {
								console.log(colorScheme.error, `[${moduleName}][Error] Error while trying to load 'guild_info' from database.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
								process.exit(14)
							} else {
								async.forEachOf(rows, (row, key, callback) => {
									settings.web.guild_info[row.guid] = {
										"guild_id": row.guid,
										"ann_ch": row.ann_ch,
										"ann_hdj_ch": row.ann_hdj_ch,
										"gen_ch": row.gen_ch
									}
									callback()
								}, function(err) {
									if (err) {
										console.log(colorScheme.error, `[${moduleName}][Error] An error has occured while saving 'guild_info' to memory.`)
										debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
										process.exit(15)
									} else {
										resolve()
									}
								})
							}
						})
					})
				}
				let init_db3 = function() {
					return new Promise(function(resolve, reject) {
						mysql_con_web.query("SELECT * FROM `levels` ORDER BY `guid` ASC", function(err, rows, fields) {
							if (err) {
								console.log(colorScheme.error, `[${moduleName}][Error] Error while trying to load 'levels' from database.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
								process.exit(16)
							} else {
								async.forEachOf(rows, (row, key, callback) => {
									settings.web.levels[row.guid] = {
										"guild_id": row.guid,
										"lvlup_msg": row.lvlup_msg,
										"lvlup_msg_ena": (row.lvlup_msg_ena == "yes") ? true : false,
										"lvlup_msg_dm": (row.lvlup_msg_dm == "yes") ? true : false,
										"banned_roles": row.banned_roles.split(","),
										"reward_roles": row.reward_roles.split(","),
										"reward_lvls": row.reward_lvls.split(",")
									}
									callback()
								}, function(err) {
									if (err) {
										console.log(colorScheme.error, `[${moduleName}][Error] An error has occured while saving 'levels' to memory.`)
										debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
										process.exit(17)
									} else {
										resolve()
									}
								})
							}
						})
					})
				}
				let init_db4 = function() {
					return new Promise(function(resolve, reject) {
						mysql_con_web.query("SELECT * FROM `modcom` ORDER BY `guid` ASC", function(err, rows, fields) {
							if (err) {
								console.log(colorScheme.error, `[${moduleName}][Error] Error while trying to load 'modcom' from database.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
								process.exit(18)
							} else {
								async.forEachOf(rows, (row, key, callback) => {
									settings.web.modcom[row.guid] = {
										"guild_id": row.guid,
										"kick_ena": (row.kick_ena == "yes") ? true : false,
										"kick_msg": row.kick_msg,
										"ban_ena": (row.ban_ena == "yes") ? true : false,
										"ban_msg": row.ban_msg,
										"mute_ena": (row.mute_ena == "yes") ? true : false,
										"mute_msg": row.mute_msg,
										"unmute_msg": row.unmute_msg,
										"banned_words": row.banned_words.split(",")
									}
									callback()
								}, function(err) {
									if (err) {
										console.log(colorScheme.error, `[${moduleName}][Error] An error has occured while saving 'modcom' to memory.`)
										debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
										process.exit(19)
									} else {
										resolve()
									}
								})
							}
						})
					})
				}
				let init_db5 = function() {
					return new Promise(function(resolve, reject) {
						mysql_con_web.query("SELECT * FROM `quotes` ORDER BY `guid` ASC", function(err, rows, fields) {
							if (err) {
								console.log(colorScheme.error, `[${moduleName}][Error] Error while trying to load 'quotes' from database.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
								process.exit(20)
							} else {
								async.forEachOf(rows, (row, key, callback) => {
									settings.web.quotes[row.guid] = {
										"guild_id": row.guid,
										"role_access": row.role_access.split(",")
									}
									callback()
								}, function(err) {
									if (err) {
										console.log(colorScheme.error, `[${moduleName}][Error] An error has occured while saving 'quotes' to memory.`)
										debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
										process.exit(21)
									} else {
										resolve()
									}
								})
							}
						})
					})
				}
				let init_db6 = function() {
					return new Promise(function(resolve, reject) {
						mysql_con_web.query("SELECT * FROM `reaction` ORDER BY `guid` ASC", function(err, rows, fields) {
							if (err) {
								console.log(colorScheme.error, `[${moduleName}][Error] Error while trying to load 'reaction' from database.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
								process.exit(22)
							} else {
								async.forEachOf(rows, (row, key, callback) => {
									settings.web.reaction[row.guid] = {
										"guild_id": row.guid,
										"hoi_ena": (row.hoi_ena == "yes") ? true : false,
										"doei_ena": (row.doei_ena == "yes") ? true : false,
										"honger_ena": (row.honger_ena == "yes") ? true : false,
										"ty_ena": (row.ty_ena == "yes") ? true : false,
										"pesten_ena": (row.pesten_ena == "yes") ? true : false,
										"cooldog_ena": (row.cooldog_ena == "yes") ? true : false,
									}
									callback()
								}, function(err) {
									if (err) {
										console.log(colorScheme.error, `[${moduleName}][Error] An error has occured while saving 'reaction' to memory.`)
										debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
										process.exit(23)
									} else {
										resolve()
									}
								})
							}
						})
					})
				}
				let init_db7 = function() {
					return new Promise(function(resolve, reject) {
						mysql_con_web.query("SELECT * FROM `rip` ORDER BY `guid` ASC", function(err, rows, fields) {
							if (err) {
								console.log(colorScheme.error, `[${moduleName}][Error] Error while trying to load 'rip' from database.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
								process.exit(24)
							} else {
								async.forEachOf(rows, (row, key, callback) => {
									settings.web.rip[row.guid] = {
										"guild_id": row.guid,
										"rip_ena": (row.rip_ena == "yes") ? true : false,
										"rip_cv_ena": (row.rip_cv_ena == "yes") ? true : false,
										"role_access": row.role_access.split(",")
									}
									callback()
								}, function(err) {
									if (err) {
										console.log(colorScheme.error, `[${moduleName}][Error] An error has occured while saving 'rip' to memory.`)
										debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
										process.exit(25)
									} else {
										resolve()
									}
								})
							}
						})
					})
				}
				let init_db8 = function() {
					return new Promise(function(resolve, reject) {
						mysql_con_web.query("SELECT * FROM `voicecom` ORDER BY `guid` ASC", function(err, rows, fields) {
							if (err) {
								console.log(colorScheme.error, `[${moduleName}][Error] Error while trying to load 'voicecom' from database.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
								process.exit(26)
							} else {
								async.forEachOf(rows, (row, key, callback) => {
									settings.web.voicecom[row.guid] = {
										"guild_id": row.guid,
										"er_ena": (row.er_ena == "yes") ? true : false,
										"isnv_ena": (row.isnv_ena == "yes") ? true : false,
										"neuken_ena": (row.neuken_ena == "yes") ? true : false,
										"khnma_ena": (row.khnma_ena == "yes") ? true : false,
										"aandacht_ena": (row.aandacht_ena == "yes") ? true : false,
										"haha_ena": (row.haha_ena == "yes") ? true : false,
										"hjn_ena": (row.hjn_ena == "yes") ? true : false,
										"goeiemorgen_ena": (row.goeiemorgen_ena == "yes") ? true : false,
										"gadver_ena": (row.gadver_ena == "yes") ? true : false,
										"wakker_ena": (row.wakker_ena == "yes") ? true : false,
										"darkness_ena": (row.darkness_ena == "yes") ? true : false,
										"nigger_ena": (row.nigger_ena == "yes") ? true : false,
										"gay_ena": (row.gay_ena == "yes") ? true : false,
										"sb_ena": (row.sb_ena == "yes") ? true : false,
										"noot_ena": (row.noot_ena == "yes") ? true : false,
										"youdoyou_ena": (row.youdoyou_ena == "yes") ? true : false,
										"laf_ena": (row.laf_ena == "yes") ? true : false,
										"onion_ena": (row.onion_ena == "yes") ? true : false,
										"pls_ena": (row.pls_ena == "yes") ? true : false,
										"eastereggs_ena": (row.eastereggs_ena == "yes") ? true : false,
										"role_access": row.role_access.split(",")
									}
									callback()
								}, function(err) {
									if (err) {
										console.log(colorScheme.error, `[${moduleName}][Error] An error has occured while saving 'voicecom' to memory.`)
										debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
										process.exit(27)
									} else {
										resolve()
									}
								})
							}
						})
					})
				}
				let init_db9 = function() {
					return new Promise(function(resolve, reject) {
						mysql_con_web.query("SELECT * FROM `warnings` ORDER BY `guid` ASC", function(err, rows, fields) {
							if (err) {
								console.log(colorScheme.error, `[${moduleName}][Error] Error while trying to load 'warnings' from database.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
								process.exit(28)
							} else {
								async.forEachOf(rows, (row, key, callback) => {
									settings.web.warnings[row.guid] = {
										"guild_id": row.guid,
										"max_warn": row.max_warn,
										"max_warn_ena": (row.max_warn_ena == "yes") ? true : false,
										"warn_ann_msg": row.warn_ann_msg,
										"warn_ann_msg_ena": (row.warn_ann_msg_ena == "yes") ? true : false,
										"access_roles": row.access_roles.split(",")
									}
									callback()
								}, function(err) {
									if (err) {
										console.log(colorScheme.error, `[${moduleName}][Error] An error has occured while saving 'warnings' to memory.`)
										debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
										process.exit(29)
									} else {
										resolve()
									}
								})
							}
						})
					})
				}
				let init_db10 = function() {
					return new Promise(function(resolve, reject) {
						mysql_con_web.query("SELECT * FROM `welcome` ORDER BY `guid` ASC", function(err, rows, fields) {
							if (err) {
								console.log(colorScheme.error, `[${moduleName}][Error] Error while trying to load 'welcome' from database.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
								process.exit(28)
							} else {
								async.forEachOf(rows, (row, key, callback) => {
									settings.web.welcome[row.guid] = {
										"guild_id": row.guid,
										"welcome_txt_ena": (row.welcome_txt_ena == "yes") ? true : false,
										"welcome_txt": row.welcome_txt,
										"leave_txt_ena": (row.leave_txt_ena == "yes") ? true : false,
										"leave_txt": row.leave_txt
									}
									callback()
								}, function(err) {
									if (err) {
										console.log(colorScheme.error, `[${moduleName}][Error] An error has occured while saving 'welcome' to memory.`)
										debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
										process.exit(29)
									} else {
										resolve()
									}
								})
							}
						})
					})
				}
				let init_db11 = function() {
					return new Promise(function(resolve, reject) {
						mysql_con_web.query("SELECT * FROM `roles` ORDER BY `guid` ASC", function(err, rows, fields) {
							if (err) {
								console.log(colorScheme.error, `[${moduleName}][Error] Error while trying to load 'user_updates' from database.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
								process.exit(42)
							} else {
								async.forEachOf(rows, (row, key, callback) => {
									settings.web.user_updates[row.guid] = {
										"guild_id": row.guid,
										"role_update_ena": (row.role_update_ena == "yes") ? true : false,
										"role_update_msg": row.role_update_msg,
										"nickname_update_ena": (row.nickname_update_ena == "yes") ? true : false,
										"nickname_update_msg": row.nickname_update_msg
									}
									callback()
								}, function(err) {
									if (err) {
										console.log(colorScheme.error, `[${moduleName}][Error] An error has occured while saving 'user_updates' to memory.`)
										debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
										process.exit(43)
									} else {
										resolve()
									}
								})
							}
						})
					})
				}
				init_db1().then(() => {
					debugMsg(`[`+moduleName+`][`+debugMode+`] 'dashboards' loaded from MySQL database to memory`)
					init_db2().then(() => {
						debugMsg(`[`+moduleName+`][`+debugMode+`] 'guild_info' loaded from MySQL database to memory`)
						init_db3().then(() => {
							debugMsg(`[`+moduleName+`][`+debugMode+`] 'levels' loaded from MySQL database to memory`)
							init_db4().then(() => {
								debugMsg(`[`+moduleName+`][`+debugMode+`] 'modcom' loaded from MySQL database to memory`)
								init_db5().then(() => {
									debugMsg(`[`+moduleName+`][`+debugMode+`] 'quotes' loaded from MySQL database to memory`)
									init_db6().then(() => {
										debugMsg(`[`+moduleName+`][`+debugMode+`] 'reaction' loaded from MySQL database to memory`)
										init_db7().then(() => {
											debugMsg(`[`+moduleName+`][`+debugMode+`] 'rip' loaded from MySQL database to memory`)
											init_db8().then(() => {
												debugMsg(`[`+moduleName+`][`+debugMode+`] 'voicecom' loaded from MySQL database to memory`)
												init_db9().then(() => {
													debugMsg(`[`+moduleName+`][`+debugMode+`] 'warnings' loaded from MySQL database to memory`)
													init_db10().then(() => {
														debugMsg(`[`+moduleName+`][`+debugMode+`] 'welcome' loaded from MySQL database to memory`)
														init_db11().then(() => {
															debugMsg(`[`+moduleName+`][`+debugMode+`] 'roles' loaded from MySQL database to memory`)
															resolve()
														}).catch((err) => {
															console.log(colorScheme.error, `[${moduleName}][Error] An unexpected error occurred while running (web) init_db11().`)
															debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
															process.exit(44)
														})
													}).catch((err) => {
														console.log(colorScheme.error, `[${moduleName}][Error] An unexpected error occurred while running (web) init_db10().`)
														debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
														process.exit(39)
													})
												}).catch((err) => {
													console.log(colorScheme.error, `[${moduleName}][Error] An unexpected error occurred while running (web) init_db9().`)
													debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
													process.exit(38)
												})
											}).catch((err) => {
												console.log(colorScheme.error, `[${moduleName}][Error] An unexpected error occurred while running (web) init_db8().`)
												debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
												process.exit(37)
											})
										}).catch((err) => {
											console.log(colorScheme.error, `[${moduleName}][Error] An unexpected error occurred while running (web) init_db7().`)
											debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
											process.exit(36)
										})
									}).catch((err) => {
										console.log(colorScheme.error, `[${moduleName}][Error] An unexpected error occurred while running (web) init_db6().`)
										debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
										process.exit(35)
									})
								}).catch((err) => {
									console.log(colorScheme.error, `[${moduleName}][Error] An unexpected error occurred while running (web) init_db5().`)
									debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
									process.exit(34)
								})
							}).catch((err) => {
								console.log(colorScheme.error, `[${moduleName}][Error] An unexpected error occurred while running (web) init_db4().`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
								process.exit(33)
							})
						}).catch((err) => {
							console.log(colorScheme.error, `[${moduleName}][Error] An unexpected error occurred while running (web) init_db3().`)
							debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
							process.exit(32)
						})
					}).catch((err) => {
						console.log(colorScheme.error, `[${moduleName}][Error] An unexpected error occurred while running (web) init_db2().`)
						debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
						process.exit(31)
					})
				}).catch((err) => {
					console.log(colorScheme.error, `[${moduleName}][Error] An unexpected error occurred while running (web) init_db1().`)
					debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
					process.exit(30)
				})
			})
		}
		init_bot().then(() => {
			console.log(colorScheme.execSuccess, `[${moduleName}][Info] Bot data initialized.`)
			init_webData().then(() => {
				console.log(colorScheme.execSuccess, `[${moduleName}][Info] Web data initialized.`)
				bot.editStatus("online", {
					name: `Type ${botPrefix}help | discord-frankie.nl`
				})
				console.log(colorScheme.execSuccess, `[${moduleName}][Info] Bot loaded and running on ${bot.guilds.size} guilds for ${bot.users.size-1} users.`)
			}).catch((err) => {
				console.log(colorScheme.error, `[${moduleName}][Error] An unexpected error occurred while running init_webData().`)
				debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
				process.exit(41)
			})
		}).catch((err) => {
			console.log(colorScheme.error, `[${moduleName}][Error] An unexpected error occurred while running init_bot().`)
			debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
			process.exit(40)
		})
	}).catch(() => {
		process.exit()
	})
})

bot.on("guildMemberAdd", (guild, member) => {
	nonleaver(guild.id, member.id, null, true).catch((err) => {
		console.log(colorScheme.warning, `[${moduleName}][Warning][${guild.id}] An unexpected error occured with 'nonleaver()'. See debug for error message.`)
		debugMsg(`[`+moduleName+`][`+debugMode+`] Error: `, err)
	})
	switchCheck(guild.id, "welcome_ena", "welcome", "welcome_txt_ena").then(() => {
		var response = settings.web.welcome[guild.id].welcome_txt.replace(player_web_reg, member.mention)
		response = response.replace(servernaam_web_reg, guild.name)
		if (settings.web.guild_info[guild.id]) {
			bot.createMessage(settings.web.guild_info[guild.id].ann_ch, response).catch((error) => {
				console.log(colorScheme.warning, `[DiscordShard][Warning][${guild.id}] Message couldn't be sent. Error:`, error)
			})
		}
	}).catch((errCode) => {	
		debugMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] switchCheck failed: `, errCode)
		kDebugMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Settings: `, settings.web.welcome[guild.id])
		kDebugMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Dashboard settings: `, settings.web.dashboards[guild.id])
	})
})

bot.on("guildMemberUpdate", (guild, member, oldMember) => {
	if (member.id != bot.user.id && oldMember) {
		if (member.roles.toString() != oldMember.roles.toString()) {
			switchCheck(guild.id, "roles_ena", "user_updates", "role_update_ena").then(() => {
				let ggrn = function(mr) {
					i = 0
					var ra = []
					while (typeof mr[i] !== 'undefined') {
						ra.push("`" + guild.roles.get(mr[i]).name + "`")
						i++
					}
					return ra.join(", ")
				}
				var response = settings.web.user_updates[guild.id].role_update_msg.replace(player_web_reg, member.mention)
				response = response.replace(ranks_web_reg, ggrn(member.roles))
				if (settings.web.guild_info[guild.id]) {
					bot.createMessage(settings.web.guild_info[guild.id].ann_ch, response).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${guild.id}] Message couldn't be sent. Error:`, error)
					})
				}
			}).catch((errCode) => {
				debugMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] switchCheck failed: `, errCode)
				kDebugMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Settings: `, settings.web.user_updates[guild.id])
				kDebugMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Dashboard settings: `, settings.web.dashboards[guild.id])
			})
		} else if (member.nick != oldMember.nick) {
			switchCheck(guild.id, "roles_ena", "user_updates", "nickname_update_ena").then(() => {
				if (typeof oldMember.nick == 'object') {
					var oldName = member.username
				} else {
					var oldName = oldMember.nick
				}
				if (typeof member.nick == 'object') {
					var newName = member.username
				} else {
					var newName = member.nick
				}
				var response = settings.web.user_updates[guild.id].nickname_update_msg.replace(oldnick_web_reg, `\`${oldName}\``)
				response = response.replace(newnick_web_reg, `\`${newName}\``)
				if (settings.web.guild_info[guild.id]) {
					bot.createMessage(settings.web.guild_info[guild.id].ann_ch, response).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${guild.id}] Message couldn't be sent. Error:`, error)
					})
				}
			}).catch((errCode) => {
				debugMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] switchCheck failed: `, errCode)
				kDebugMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Settings: `, settings.web.user_updates[guild.id])
				kDebugMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Dashboard settings: `, settings.web.dashboards[guild.id])
			})
		}
	}
})

bot.on("guildMemberRemove", (guild, member) => {
	if (member.id != bot.user.id) {
		switchCheck(guild.id, "welcome_ena", "welcome", "leave_txt_ena").then(() => {
			nonleaverFind(guild.id, member.id).catch(() => {
				nonleaver(guild.id, member.id, null, true).then(() => {
					let response = settings.web.welcome[guild.id].leave_txt.replace(player_web_reg, member.mention)
					response = response.replace(servernaam_web_reg, guild.name)
					if (!settings.web.guild_info[guild.id] || !settings.web.guild_info[guild.id].ann_ch) {
						return
					}
					bot.createMessage(settings.web.guild_info[guild.id].ann_ch, response).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${guild.id}] Message couldn't be sent. Error:`, error)
					})
				}).catch((err) => {
					console.log(colorScheme.warning, `[NonLeaver][Warning][${guild.id}] Failed to delete (${member.id}) from guild (${guild.id}) from 'nonleaver' variable. Error:`, err)
				})
			})
		}).catch((errCode) => {
			if (errCode != 1 && errCode != 2) {
				debugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] switchCheck failed: `, errCode)
				kDebugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] Settings: `, settings.web.welcome[guild.id])
				kDebugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] Dashboard settings: `, settings.web.dashboards[guild.id])
			}
		})
		switchCheck(guild.id, "levels_ena", "levels", null).then(() => {
			mysql_con_bot.query("DELETE FROM `ranks` WHERE guild_id='" + guild.id + "' AND user_id='" + member.id + "'", function(err) {
				if (err) {
					console.log(colorScheme.critical, `[MySQL][Critical][${guild.id}] MySQL query failed, see debug for error.`)
					debugMsg(`[/guildMemberRemove/MySQL][`+debugMode+`][${guild.id}] Error: `, err)
				}
			})
		}).catch((errCode) => {
			if (errCode != 1 && errCode != 2) {
				debugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] switchCheck failed: `, errCode)
				kDebugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] Settings: `, settings.web.levels[guild.id])
				kDebugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] Dashboard settings: `, settings.web.dashboards[guild.id])
			}
		})
		if (settings.bot.ranks[guild.id] && settings.bot.ranks[guild.id][member.id]) {
			delete settings.bot.ranks[guild.id][member.id]
		}
		switchCheck(guild.id, "warnigns_ena", "warnings", null).then(() => {
			mysql_con_bot.query("DELETE FROM `warnings` WHERE guild_id='" + guild.id + "' AND user_id='" + member.id + "'", function(err) {
				if (err) {
					console.log(colorScheme.critical, `[MySQL][Critical][${guild.id}] MySQL query failed, see debug for error.`)
					debugMsg(`[/guildMemberRemove/MySQL][`+debugMode+`][${guild.id}] Error: `, err)
				}
			})
		}).catch((errCode) => {
			if (errCode != 1 && errCode != 2) {
				debugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] switchCheck failed: `, errCode)
				kDebugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] Settings: `, settings.web.warnings[guild.id])
				kDebugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] Dashboard settings: `, settings.web.dashboards[guild.id])
			}
		})
		if (settings.bot.warnings[guild.id] && settings.bot.warnings[guild.id][member.id]) {
			delete settings.bot.warnings[guild.id][member.id]
		}
		switchCheck(guild.id, "quotes_ena", "quotes", null).then(() => {
			mysql_con_bot.query("DELETE FROM `quotes` WHERE guild_id='" + guild.id + "' AND user_id='" + member.id + "'", function(err) {
				if (err) {
					console.log(colorScheme.critical, `[MySQL][Critical][${guild.id}] MySQL query failed, see debug for error.`)
					debugMsg(`[/guildMemberRemove/MySQL][`+debugMode+`][${guild.id}] Error: `, err)
				}
			})
		}).catch((errCode) => {
			if (errCode != 1 && errCode != 2) {
				debugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] switchCheck failed: `, errCode)
				kDebugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] Settings: `, settings.web.quotes[guild.id])
				kDebugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] Dashboard settings: `, settings.web.dashboards[guild.id])
			}
		})
		if (settings.bot.quotes[guild.id] && settings.bot.quotes[guild.id][member.id]) {
			delete settings.bot.quotes[guild.id][member.id]
		}
	}
})

bot.on("guildDelete", (guild) => {
	switchCheck(guild.id, "levels_ena", "levels", null).then(() => {
		mysql_con_bot.query("DELETE FROM `ranks` WHERE guild_id='" + guild.id + "'", function(err) {
			if (err) {
				console.log(colorScheme.critical, `[MySQL][Critical][${guild.id}] MySQL query failed, see debug for error.`)
				debugMsg(`[/guildDelete/MySQL][`+debugMode+`][${guild.id}] Error: `, err)
			}
		})
	}).catch((errCode) => {
		if (errCode != 1 && errCode != 2) {
			debugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] switchCheck failed: `, errCode)
			kDebugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] Settings: `, settings.web.levels[guild.id])
			kDebugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] Dashboard settings: `, settings.web.dashboards[guild.id])
		}
	})
	if (settings.bot.ranks[guild.id]) {
		delete settings.bot.ranks[guild.id]
	}
	switchCheck(guild.id, "warnigns_ena", "warnings", null).then(() => {
		mysql_con_bot.query("DELETE FROM `warnings` WHERE guild_id='" + guild.id + "'", function(err) {
			if (err) {
				console.log(colorScheme.critical, `[MySQL][Critical][${guild.id}] MySQL query failed, see debug for error.`)
				debugMsg(`[/guildDelete/MySQL][`+debugMode+`][${guild.id}] Error: `, err)
			}
		})
	}).catch((errCode) => {
		if (errCode != 1 && errCode != 2) {
			debugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] switchCheck failed: `, errCode)
			kDebugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] Settings: `, settings.web.warnings[guild.id])
			kDebugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] Dashboard settings: `, settings.web.dashboards[guild.id])
		}
	})
	if (settings.bot.warnings[guild.id]) {
		delete settings.bot.warnings[guild.id]
	}
	switchCheck(guild.id, "quotes_ena", "quotes", null).then(() => {
		mysql_con_bot.query("DELETE FROM `quotes` WHERE guild_id='" + guild.id + "'", function(err) {
			if (err) {
				console.log(colorScheme.critical, `[MySQL][Critical][${guild.id}] MySQL query failed, see debug for error.`)
				debugMsg(`[/guildDelete/MySQL][`+debugMode+`][${guild.id}] Error: `, err)
			}
		})
	}).catch((errCode) => {
		if (errCode != 1 && errCode != 2) {
			debugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] switchCheck failed: `, errCode)
			kDebugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] Settings: `, settings.web.quotes[guild.id])
			kDebugMsg(`[`+moduleName+`/guildMemberRemove][`+debugMode+`][${guild.id}] Dashboard settings: `, settings.web.dashboards[guild.id])
		}
	})
	if (settings.bot.quotes[guild.id]) {
		delete settings.bot.quotes[guild.id]
	}
})

bot.on("messageCreate", (msg) => {
	if (msg.author.bot === true) {
		return
	}
	if (isGuild(msg.channel.guild) && !settings.web.dashboards[msg.channel.guild.id]) {
		console.log(colorScheme.red, `[`+moduleName+`][Alert][${msg.channel.guild.id}] Found a guild with no dashboard settings! Name: ${msg.channel.guild.name}`)
		if (fAlert.noSettings(msg.channel.guild.id) === true) {
			msg.channel.createMessage(alertMsgs.noSettings)
		}
		return
	}
	// Code section: Response to hello
	if (hello_text.indexOf(msg.content.toLowerCase()) != -1) {
		if (!isGuild(msg.channel.guild)) {
			if (msg.content.toLocaleLowerCase() == "oi <@" + botID + ">") {
				msg.channel.createMessage(`Oi my mate ${msg.author.mention}.`).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
				})
			} else if (msg.content.toLocaleLowerCase() == "yo <@" + botID + ">") {
				msg.channel.createMessage(`Yo mijn swegga broeder ${msg.author.mention}.`).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
				})
			} else {
				msg.channel.createMessage(`${hello_resp[randomInt(0, hello_resp.length-1)]} ${msg.author.mention}.`).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
				})
			}
		} else {
			switchCheck(msg.channel.guild.id, "reaction_ena", "reaction", "hoi_ena").then(() => {
				if (msg.content.toLocaleLowerCase() == "oi <@" + botID + ">") {
					msg.channel.createMessage(`Oi my mate ${msg.author.mention}.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
					})
				} else if (msg.content.toLocaleLowerCase() == "yo <@" + botID + ">") {
					msg.channel.createMessage(`Yo mijn swegga broeder ${msg.author.mention}.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
					})
				} else {
					msg.channel.createMessage(`${hello_resp[randomInt(0, hello_resp.length-1)]} ${msg.author.mention}.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
					})
				}
			}).catch((errCode) => {
				kDebugMsg(`[`+moduleName+`][`+debugMode+`][${msg.channel.guild.id}] Hoi function denied: `, errCode)
			})
		}
	// Code section: Response to bye text
	} else if (bye_text.indexOf(msg.content.toLowerCase()) != -1) {
		if (!isGuild(msg.channel.guild)) {
			msg.channel.createMessage(`${bye_resp[randomInt(0, bye_resp.length-1)]} ${msg.author.mention}.`).catch((error) => {
				console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
			})
		} else {
			switchCheck(msg.channel.guild.id, "reaction_ena", "reaction", "doei_ena").then(() => {
				msg.channel.createMessage(`${bye_resp[randomInt(0, bye_resp.length-1)]} ${msg.author.mention}.`).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
				})
			}).catch((errCode) => {
				kDebugMsg(`[`+moduleName+`][`+debugMode+`][${msg.channel.guild.id}] Doei function denied: `, errCode)
			})
		}
	// Code section: Response on thank you
	} else if (ty_text.indexOf(msg.content.toLowerCase()) != -1) {
		if (!isGuild(msg.channel.guild)) {
			msg.channel.createMessage(`${ty_resp[randomInt(0, ty_resp.length-1)]} ${msg.author.mention}.`).catch((error) => {
				console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
			})
		} else {
			switchCheck(msg.channel.guild.id, "reaction_ena", "reaction", "ty_ena").then(() => {
				msg.channel.createMessage(`${ty_resp[randomInt(0, ty_resp.length-1)]} ${msg.author.mention}.`).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
				})
			}).catch((errCode) => {
				kDebugMsg(`[`+moduleName+`][`+debugMode+`][${msg.channel.guild.id}] Dankje function denied: `, errCode)
			})
		}
	// Code section: Response on someone else being bullied
	} else if (gepest_reg.exec(msg.content) != null && (frank_reg.exec(msg.content) != null || frankie_reg.exec(msg.content) != null || (msg.mentions[0] && msg.mentions[0].id == botID))) {
		let target = msg.author
		if (typeof(msg.mentions[0]) == "object") {
			target = msg.mentions[0]
		}
		msg.channel.createMessage(`${target.mention} https://www.stoppestennu.nl/ alsjeblieft en success. :wink:`).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
		})
	// Code section: Response on someone threatening Frankie
	} else if (kapot_reg.exec(msg.content) != null && (frank_reg.exec(msg.content) != null || frankie_reg.exec(msg.content) != null || (msg.mentions[0] && msg.mentions[0].id == botID))) {
		msg.channel.createMessage("Zeg " + msg.author.mention + " doe is ff rustig jij!\n" + thisisfrankie_url).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
		})
	// Code section: Response on someone threatening Frankie
	} else if (oprotten_reg.exec(msg.content) != null && (frank_reg.exec(msg.content) != null || frankie_reg.exec(msg.content) != null || (msg.mentions[0] && msg.mentions[0].id == botID))) {
		msg.channel.createMessage("Zeg " + msg.author.mention + " doe is ff rustig jij!\n" + thisisfrankie_url).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
		})
	// Code section: Response to bullying againts Frankie
	} else if (kut_reg.exec(msg.content) != null && (frank_reg.exec(msg.content) != null || frankie_reg.exec(msg.content) != null || (msg.mentions[0] && msg.mentions[0].id == botID))) {
		msg.channel.createMessage("Jij bent kut, " + msg.author.mention + ".").catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
		})
	// Code section: Response to bullying againts Frankie
	} else if (kanker_reg.exec(msg.content) != null && (frank_reg.exec(msg.content) != null || frankie_reg.exec(msg.content) != null || (msg.mentions[0] && msg.mentions[0].id == botID))) {
		msg.channel.createMessage("Jij bent zelf kanker, " + msg.author.mention + ".").catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
		})
	// Code section: Response to bullying againts Frankie
	} else if (hjb_reg.exec(msg.content) != null && (frank_reg.exec(msg.content) != null || frankie_reg.exec(msg.content) != null || (msg.mentions[0] && msg.mentions[0].id == botID))) {
		msg.channel.createMessage("Nee vrijheid van meningsuiting, " + msg.author.mention + ".").catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
		})
	// Code section: Response to bullying againts Frankie
	} else if (hjb_f_reg.exec(msg.content) != null && (frank_reg.exec(msg.content) != null || frankie_reg.exec(msg.content) != null || (msg.mentions[0] && msg.mentions[0].id == botID))) {
		msg.channel.createMessage("Nee vrijheid van meningsuiting, " + msg.author.mention + ".").catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
		})
	// Code section: Response to bullying againts Frankie
	} else if (hjb_f2_reg.exec(msg.content) != null && (frank_reg.exec(msg.content) != null || frankie_reg.exec(msg.content) != null || (msg.mentions[0] && msg.mentions[0].id == botID))) {
		msg.channel.createMessage("Nee vrijheid van meningsuiting, " + msg.author.mention + ".").catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
		})
	} else if (isGuild(msg.channel.guild) && msg.content.indexOf(botPrefix) == -1 && msg.author.bot === false) {
		// Checking for bad words
		switchCheck(msg.channel.guild.id, "modcom_ena", "modcom", null).then(() => {
			if (typeof(settings.web.modcom[msg.channel.guild.id].banned_words[0]) == "string" && settings.web.modcom[msg.channel.guild.id].banned_words[0] != "") {
				devMsg(`[Bad Words][${debugMode}][${msg.channel.guild.id}] Bad words are set for this server! Bad words array: `, settings.web.modcom[msg.channel.guild.id].banned_words)
				async.forEachOf(settings.web.modcom[msg.channel.guild.id].banned_words, (bannedWord, key, callback) => {
					devMsg(`[Bad Words][${debugMode}][${msg.channel.guild.id}] Checking if '${bannedWord}' can be found in '${msg.content}'`)
					if (msg.content.match(new RegExp(bannedWord, "gmi")) != null) {
						callback(true)
					} else {
						callback()
					}
				}, function(found) {
					if (found === true) {
						devMsg(`[Bad Words][${debugMode}][${msg.channel.guild.id}] Found a bad word via the async from earlier, deleting original message...`)
						msg.delete().catch(() => {
							msg.channel.createMessage(`Ik heb een verbannen word gezien! maar het blijkt dat ik geen rechten hebt om berichten te verwijderen (beheren) in dit kanaal.`).catch((error) => {
								console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] message couldn't be sent. Error:`, error)
							})
							console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Couldn't delete message (bad word). Error:`, error)
						})
					} else {
						// Give EXP if switch is on
						switchCheck(msg.channel.guild.id, "levels_ena", "levels").then(() => {
							let bannedEXPRoles = settings.web.levels[msg.channel.guild.id].banned_roles
							let skipEXP = false
							for (let i = 0; i < bannedEXPRoles.length; i++) {
								if (msg.member.roles.indexOf(bannedEXPRoles[i]) != -1) {
									skipEXP = true
								}
							}
							if (skipEXP === false) {
								expCalc(msg.channel.guild.id, msg.author.id)
							}
						}).catch((err) => {
							if (typeof(err) != "number") {
								console.log(colorScheme.warning, `[${moduleName}][Warning][${msg.channel.guild}] switchCheck() failed for EXP giver. See debug for error.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error: `, err)
							}
						})
					}
				})
			} else {
				let bannedEXPRoles = settings.web.levels[msg.channel.guild.id].banned_roles
				let skipEXP = false
				for (let i = 0; i < bannedEXPRoles.length; i++) {
					if (msg.member.roles.indexOf(bannedEXPRoles[i]) != -1) {
						skipEXP = true
					}
				}
				if (skipEXP === false) {
					expCalc(msg.channel.guild.id, msg.author.id)
				}
			}
		}).catch(() => {
			// Give EXP if switch is on
			switchCheck(msg.channel.guild.id, "levels_ena", "levels").then(() => {
				expCalc(msg.channel.guild.id, msg.author.id)
			}).catch((err) => {
				if (typeof(err) != "number") {
					console.log(colorScheme.warning, `[${moduleName}][Warning][${msg.channel.guild}] switchCheck() failed for EXP giver. See debug for error.`)
					debugMsg(`[`+moduleName+`][`+debugMode+`] Error: `, err)
				}
			})
		})
	}
})

bot.registerCommand("kick", (msg, args) => {
	let kickUser = function(reason) {
		bot.kickGuildMember(msg.channel.guild.id, msg.mentions[0].id, reason).then(() => {
			if (settings.web.modcom[msg.channel.guild.id].kick_msg != "") {
				var kick_msg = settings.web.modcom[msg.channel.guild.id].kick_msg.replace(player_web_reg, msg.mentions[0].username)
				let channelID = (settings.web.guild_info[msg.channel.guild.id] && settings.web.guild_info[msg.channel.guild.id].ann_ch) ? settings.web.guild_info[msg.channel.guild.id].ann_ch : msg.channel.id
				bot.createMessage(channelID, kick_msg).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
				})
			}
		}).catch((error) => {
			if (args[1] && args[args.length-1] == "ja") {
				bot.getDMChannel(msg.mentions[0].id).then((privechan) => {
					privechan.getMessages(1).then((arrMsgs) => {
						arrMsgs[0].delete().catch((error) => {
							console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Failed to delete message from private channel. Error:`, error)
						})
					}).catch((error) => {
						if (typeof(bot.users.get(msg.mentions[0].id)) != "undefined") {
							console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Failed to get messages from private channel. Error:`, error)
						}
					})
				}).catch((error) => {
					// Oh. nothing...
				})
			}
			if (error.message.indexOf("403 FORBIDDEN on DELETE") != -1) {
				msg.channel.createMessage(`Mijn kick schoenen zijn niet sterk genoeg om deze persoon te kicken van de server. Geef mij sterkere kick schoenen en probeer het opnieuw.`).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
				})
			} else {
				msg.channel.createMessage(`Mijn kick schoenen zijn kapot! Wie heeft deze gesloopt?!\nMocht ik het over een paar minuten nog niet gerepareerd hebben wil je dan even naar het tevredensheid bureua lopen en vragen of hun het kunnen fixen?`).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
				})
				console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Couldn't kick user (${msg.mentions[0].id}). Error:`, error)
			}
		})
	}
	if (msg.mentions[0].id === botID) {
		msg.channel.createMessage(`Mijn werk kleding is veels te mooi om vies te maken met me eigen kick schoenen.`).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
	} else {
		nonleaver(msg.channel.guild.id, msg.mentions[0].id, "kick").then(() => {
			if (args[1]) {
				if (args[args.length-1] == "ja" || args[args.length-1] == "nee") {
					var tArr = args.slice(0, args.length-1)
				} else {
					var tArr = args
				}
				var reason = tArr.slice(1, tArr.length).join(" ")
			} else {
				var reason = null
			}
			if (args[args.length-1] == "ja" && reason != null) {
				kDebugMsg(`[`+moduleName+`][`+debugMode+`] Kick reason: `, reason)
				bot.getDMChannel(msg.mentions[0].id).then((privechan) => {
					privechan.createMessage("Hallo " + msg.mentions[0].username + ". Je bent gekicked van `" + msg.channel.guild.name + "` vanwege de volgende reden:\n```" + reason + "```").then(() => {
						kickUser(reason)
					}).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
				}).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Couldn't find DM channel. Error:`, error)
				})
			} else {
				kickUser(reason)
			}
		}).catch((err) => {
			console.log(colorScheme.critical, `[${moduleName}][Critical][${msg.channel.guild.id}] An unexpected error occured with 'nonleaver()'. See debug for error message.`)
			debugMsg(`[`+moduleName+`][`+debugMode+`] Error: `, err)
		})
	}
}, {
	aliases: ["ontsla"],
	argsRequired: true,
	cooldown: 10000,
	cooldownExclusions: {
		userIDs: developers.list
	},
	cooldownMessage: "Ik ben nog steeds bezig met de papieren van de vorige persoon die je hebt ontslagen.\nGeef me 10 seconden om dit af te maken.",
	cooldownReturns: 1,
	deleteCommand: true,
	description: "Kickt @iemand van de server",
	errorMessage: "Sorry ik kan kick schoenen niet vinden. Als ik hem over een paar minuten nog niet heb loop dan even naar het Tevredenheid Bureau.",
	fullDescription: "Kickt @iemand van de server\n[] <= Zijn optioneel",
	guildOnly: true,
	hidden: false,
	invalidUsageMessage: "Je hebt geen persoon opgegeven. Tenzij je met onzichtbare inkt hebt geschreven.",
	permissionMessage: function(msg) {
		if (isGuild(msg.channel.guild)) {
			return null
		} else {
			return "Sorry maar deze functie is niet beschikbaar in privé/group chats."
		}
	},
	requirements: {
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else if (typeof(msg.mentions[0]) != "object") {
				msg.channel.createMessage(msg.command.invalidUsageMessage).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
				})
				return false
			} else {
				let statCode = switchCheck(msg.channel.guild.id, "modcom_ena", "modcom", "kick_ena", "default")
				if (statCode === 0) {
					if (msg.member.permission.has("kickMembers")) {
						return true
					} else {
						msg.channel.createMessage(`NEE! Jij mag mijn kick schoenen niet gebruiken!`).catch((error) => {
							console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
						})
						return false
					}
				} else if (statCode === 1) {
					msg.channel.createMessage(`De Mod Commands module is niet geactiveerd.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else if (statCode === 2) {
					msg.channel.createMessage(`De kick/ontsla functie is niet aan gezet.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else {
					msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				}
			}
		}
	},
	restartCooldown: false,
	usage: "@gebruiker [reden (voor server logboek)] [moet de reden in privé gestuurd worden? ja/nee]"
})

bot.registerCommand("ban", (msg, args) => {
	let banUser = function(reason, rmDays) {
		bot.banGuildMember(msg.channel.guild.id, msg.mentions[0].id, rmDays, reason).then(() => {
			if (settings.web.modcom[msg.channel.guild.id].ban_msg != "") {
				var ban_msg = settings.web.modcom[msg.channel.guild.id].ban_msg.replace(player_web_reg, msg.mentions[0].username)
				let channelID = (settings.web.guild_info[msg.channel.guild.id] && settings.web.guild_info[msg.channel.guild.id].ann_ch) ? settings.web.guild_info[msg.channel.guild.id].ann_ch : msg.channel.id
				bot.createMessage(channelID, ban_msg).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
				})
			}
		}).catch((error) => {
			if (args[1] && args[args.length-1] == "ja") {
				bot.getDMChannel(msg.mentions[0].id).then((privechan) => {
					privechan.getMessages(1).then((arrMsgs) => {
						arrMsgs[0].delete().catch((error) => {
							console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Failed to delete message from private channel. Error:`, error)
						})
					}).catch((error) => {
						if (typeof(bot.users.get(msg.mentions[0].id)) != "undefined") {
							console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Failed to get messages from private channel. Error:`, error)
						}
					})
				}).catch((error) => {
					// Oh. nothing...
				})
			}
			if (error.message.indexOf("403 FORBIDDEN on DELETE") != -1) {
				msg.channel.createMessage(`Mijn ban hammer is niet sterk genoeg om deze persoon weg te slaan van deze server. Geef mij een sterkere ban hammer en probeer het opnieuw.`).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
				})
			} else {
				msg.channel.createMessage(`Mijn ban hammer is kapot! Wie heeft deze gesloopt?!\nMocht ik het over een paar minuten nog niet gerepareerd hebben wil je dan even naar het tevredensheid bureua lopen en vragen of hun het kunnen fixen?`).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
				})
				console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Couldn't ban user (${msg.mentions[0].id}). Error:`, error)
			}
		})
	}
	if (msg.mentions[0].id === botID) {
		msg.channel.createMessage("Waarom zou ik mezelf slaan met me eigen ban hammer?").catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
	} else {
		nonleaver(msg.channel.guild.id, msg.mentions[0].id, "ban").then(() => {
			if (args[1]) {
				if (args[args.length-1] == "ja" || args[args.length-1] == "nee") {
					if (!isNaN(parseInt(args[args.length-2]))) {
						var tArr = args.slice(0, args.length-2)
					} else {
						var tArr = args.slice(0, args.length-1)
					}
				} else {
					if (!isNaN(parseInt(args[args.length-1]))) {
						var tArr = args.slice(0, args.length-1)
					} else {
						var tArr = args
					}
				}
				var reason = tArr.slice(1, tArr.length).join(" ")
			} else {
				var reason = null
			}
			if (!isNaN(parseInt(args[args.length-2]))) {
				if (parseInt(args[args.length-2]) < 0) {
					var rmDays = 0
				} else if (parseInt(args[args.length-2]) > 7) {
					var rmDays = 7
				} else {
					var rmDays = parseInt(args[args.length-2])
				}
			} else if (!isNaN(parseInt(args[args.length-1]))) {
				if (parseInt(args[args.length-1]) < 0) {
					var rmDays = 0
				} else if (parseInt(args[args.length-1]) > 7) {
					var rmDays = 7
				} else {
					var rmDays = parseInt(args[args.length-1])
				}
			} else {
				var rmDays = 0
			}
			if (args[args.length-1] == "ja" && reason != null) {
				kDebugMsg(`[`+moduleName+`][`+debugMode+`] Ban reason: `, reason)
				bot.getDMChannel(msg.mentions[0].id).then((privechan) => {
					privechan.createMessage("Hallo " + msg.mentions[0].username + ". Je bent verbannen van `" + msg.channel.guild.name + "` vanwege de volgende reden:\n```" + reason + "```").then(() => {
						banUser(reason, rmDays)
					}).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
				}).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Couldn't find DM channel. Error:`, error)
				})
			} else {
				banUser(reason, rmDays)
			}
		}).catch((err) => {
			console.log(colorScheme.critical, `[${moduleName}][Critical][${msg.channel.guild.id}] An unexpected error occured with 'nonleaver()'. See debug for error message.`)
			debugMsg(`[`+moduleName+`][`+debugMode+`] Error: `, err)
		})
	}
}, {
	// Boolean (optional) - If arguments are required or not
	argsRequired: true,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 10000,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: developers.list
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "De openhaard zit nog vol met de koffers van de vorige persoon die verbannen is.\nGeef me 10 seconden om dit op te ruimen.",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 1,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: true,
	// String (optional) - A short description of the command to show in the default help command
	description: "Banned @iemand van de server",
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Mijn openhaard gaat niet aan.\nMocht dit na een paar minuten nog zo zijn loop dan even naar het Tevredenheids bureau.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Banned @iemand van de server\n[] <= Zijn optioneel",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: true,
	hidden: false,
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "Je hebt geen persoon opgegeven. Tenzij je met onzichtbare inkt hebt geschreven.",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: function(msg) {
		if (isGuild(msg.channel.guild)) {
			return null
		} else {
			return "Sorry maar deze functie is niet beschikbaar in privé/group chats."
		}
	},
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else if (typeof(msg.mentions[0]) != "object") {
				msg.channel.createMessage(msg.command.invalidUsageMessage)
				return false
			} else {
				let statCode = switchCheck(msg.channel.guild.id, "modcom_ena", "modcom", "ban_ena", "default")
				if (statCode === 0) {
					if (msg.member.permission.has("banMembers")) {
						return true
					} else {
						msg.channel.createMessage(`NEE! Jij mag mijn ban hammer niet gebruiken!`).catch((error) => {
							console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
						})
						return false
					}
				} else if (statCode === 1) {
					msg.channel.createMessage(`De Mod Commands module is niet geactiveerd.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else if (statCode === 2) {
					msg.channel.createMessage(`De ban functie is niet aan gezet.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else {
					msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "@gebruiker [reden (voor server logboek)] [berichtgeschiedenis verwijderen van de afgelopen (0-7) dagen] [moet de reden in privé gestuurd worden? ja/nee]"
})

bot.registerCommand("voicecom", (msg, args) => {
	let voiceComm = function(sSoundObj) {
		bot.joinVoiceChannel(msg.member.voiceState.channelID).then((connection) => {
			if (connection.playing) {
				msg.channel.createMessage("Ik ben nog even bezig in een andere kamer.\nProbeer het straks nog eens.").catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
				})
				return
			}
			connection.play(soundDefLoc + sSoundObj.soundFile, {
				format: "ogg",
				sampleRate: 44100
			})
			msg.channel.createMessage(sSoundObj.succReac.replace(player_web_reg, msg.author.mention)).catch((error) => {
				console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
			})
			connection.once("end", () => {
				bot.leaveVoiceChannel(msg.member.voiceState.channelID)
			})
		}).catch((err) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Couldn't join VoiceChannel when Voicecom was called. Error:`, err)
			msg.channel.createMessage("Sorry kon niet de juiste kamer vinden. Ik zal de volgende keer beter me best doen.").catch((error) => {
				console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
			})
		})
	}
	let cass = args.join(" ").toLocaleLowerCase()
	kDebugMsg(`[`+moduleName+`][`+debugMode+`] Args as string lower case: `, cass)
	if (!msg.member.voiceState.channelID) {
		msg.channel.createMessage(`Sorry maar je zit niet in een spraak kamer.\nDus ik kan niet langs komen.`).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
	} else {
		async.forEachOf(soundsObj, (sObj, key, callback) => {
			kDebugMsg(`[`+moduleName+`][`+debugMode+`] Index is: `, sObj.alias.indexOf(cass))
			if (sObj.name == cass || sObj.alias.indexOf(cass) != -1) {
				callback(key)
			} else {
				callback()
			}
		}, function(retKey) {
			if (typeof(retKey) == "number") {
				kDebugMsg(`[`+moduleName+`][`+debugMode+`] retKey (${typeof(retKey)}) = `, retKey)
				roleAccess(settings.web.voicecom[msg.channel.guild.id].role_access, msg.member).then(() => {
					switchCheck(msg.channel.guild.id, "voicecom_ena", "voicecom", soundsObj[retKey].switch).then(() => {
						voiceComm(soundsObj[retKey])
					}).catch((statCode) => {
						if (statCode === 2) {
							msg.channel.createMessage(`Je hebt de \`${soundsObj[retKey].name}\` voice command niet aan gezet.\nGa naar het paneel en zet het aan om dit geluidje te gebruiken.`).catch((error) => {
								console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
							})
						} else {
							console.log(colorScheme.critical, `[${moduleName}][Critical][${msg.channel.guild.id}] An unexpected error happend while 'switchCheck()'ing (2nd check) for voicecom. See debug for error.`)
							debugMsg(`[`+moduleName+`][`+debugMode+`] Error (code): `, statCode)
							msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
								console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
							})
						}
					})
				}).catch((err) => {
					kDebugMsg(`[`+moduleName+`][`+debugMode+`] roleAccess @ voicecom gave an error: `, err)
					msg.channel.createMessage(soundsObj[retKey].failReac.replace(player_web_reg, msg.author.mention)).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
				})
			} else {
				kDebugMsg(`[`+moduleName+`][`+debugMode+`] retKey (${typeof(retKey)}) = `, retKey)
				msg.channel.createMessage(msg.command.invalidUsageMessage).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
				})
			}
		})
	}
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["vc"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: true,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 5000,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: developers.list,
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "Heb even geduld ik ben nog bezig om de vorige cassette op te bergen.\nGeef me 5 seconden.",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 1,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Speelt een opgegeven geluidje af",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Me cassette bandjes liggen overal op de vloer. Ik zal ze allemaal oprapen en opnieuw sorteren.\nMocht ik over een paar minuten daar nog niet klaar mee zijn loop dan even naar het Tevredenheids Bureau en vraag om hulp.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Geef een geluidje op en dan word deze afgespeeld als je in een spraak kanaal zit.",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: true,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "Sorry maar deze cassette heb ik niet. Geef een geldig geluidje op.",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: function(msg) {
		if (isGuild(msg.channel.guild)) {
			return null
		} else {
			return "Sorry maar deze functie is niet beschikbaar in privé/group chats."
		}
	},
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else {
				let statCode = switchCheck(msg.channel.guild.id, "voicecom_ena", "voicecom", null, "default")
				if (statCode === 0) {
					return true
				} else if (statCode === 1) {
					msg.channel.createMessage(`De Voice Commando's module niet geactiveerd.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else {
					msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "<naam van het geluidje>"
})

bot.registerCommand("mute", (msg, args) => {
	if (msg.mentions[0].id === botID) {
		msg.channel.createMessage(`Ik weet niet wat voor ideeën jij hebt maar ik ga geen touw en tape op mezelf gebruiken.`).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
	} else {
		let reason = null
		if (args[1]) {
			reason = args.slice(1, args.length).join(" ")
		}
		bot.editChannelPermission(msg.channel.id, msg.mentions[0].id, null, 2048, "member", reason).then(() => {
			let response = settings.web.modcom[msg.channel.guild.id].mute_msg.replace(player_web_reg, msg.mentions[0].mention)
			msg.channel.createMessage(response).catch((error) => {
				console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
			})
		}).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] User couldn't be muted. Error:`, error)
			msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
				console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
			})
		})
	}
}, {
	// Boolean (optional) - If arguments are required or not
	argsRequired: true,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 15000,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: developers.list
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "Ik ben nog bezig om de vorige persoon (legaal) stil te krijgen.\nProbeer het over 15 seconden nog een keer.",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a msg.
	cooldownReturns: 1,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: true,
	// String (optional) - A short description of the command to show in the default help command
	description: "Zorgt dat @iemand weer kan praten in het huidige tekst kanaal",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Ik kan me touw en tape niet vinden.\nProbeer het later nog een keer of loop even naar het tevredenheids bureau en vraag om nieuwe touw en tape.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Zorgt dat @iemand weer kan praten in het huidige tekst kanaal.\n[] <= Zijn optioneel",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: true,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// Object (optional) - A set of functions to be executed at different times throughout the command's processing
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "Je hebt niet iemand opgegeven die ik in het hotel kan vinden.\nGeef iemand op die er ook echt is.",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: function(msg) {
		if (isGuild(msg.channel.guild)) {
			return null
		} else {
			return "Sorry maar deze functie is niet beschikbaar in privé/group chats."
		}
	},
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else if (typeof(msg.mentions[0]) != "object") {
				msg.channel.createMessage(msg.command.invalidUsageMessage)
				return false
			} else {
				let statCode = switchCheck(msg.channel.guild.id, "modcom_ena", "modcom", "mute_ena", "default")
				if (statCode === 0) {
					if (msg.member.permission.has("manageChannels")) {
						return true
					} else {
						msg.channel.createMessage(`Wie zoek je?\nDie heb ik niet gezien, sorry.`).catch((error) => {
							console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
						})
						return false
					}
				} else if (statCode === 1) {
					msg.channel.createMessage(`De Mod Commands module is niet geactiveerd.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else if (statCode === 2) {
					msg.channel.createMessage(`De (un)mute functie is niet aan gezet.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else {
					msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "@iemand [reden]"
})

bot.registerCommand("unmute", (msg, args) => {
	if (msg.mentions[0].id === botID) {
		msg.channel.createMessage(`Maar ik ben niet vast gebonden.\nDeze commando hoef je niet op mij te gebruiken. :joy:`).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
	} else {
		let reason = null
		if (args[1]) {
			reason = args.slice(1, args.length).join(" ")
		}
		bot.deleteChannelPermission(msg.channel.id, msg.mentions[0].id, reason).then(() => {
			let response = settings.web.modcom[msg.channel.guild.id].unmute_msg.replace(player_web_reg, msg.mentions[0].mention)
			msg.channel.createMessage(response).catch((error) => {
				console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
			})
		}).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] User couldn't be UNmuted. Error:`, error)
			msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
				console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
			})
		})
	}
}, {
	// Boolean (optional) - If arguments are required or not
	argsRequired: true,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 15000,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: developers.list
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "Ik ben nog bezig met de vorige persoon te bevrijden.\nProbeer het over 15 seconden nog een keer.",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a msg.
	cooldownReturns: 1,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: true,
	// String (optional) - A short description of the command to show in the default help command
	description: "Zorgt dat @iemand weer kan praten in het huidige tekst kanaal",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Ik denk dat het touw en tape iets te strak zitten.\nProbeer het over een paar minuten nog een keer of schakel de hulp in van het tevredensheid bureau.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Zorgt dat @iemand weer kan praten in het huidige tekst kanaal.\n[] <= Zijn optioneel",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: true,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "Je hebt niemand opgegeven die in me kleder is.\nGeef iemand op die ik \"gemute\" heb.",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: function(msg) {
		if (isGuild(msg.channel.guild)) {
			return null
		} else {
			return "Sorry maar deze functie is niet beschikbaar in privé/group chats."
		}
	},
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else if (typeof(msg.mentions[0]) != "object") {
				msg.channel.createMessage(msg.command.invalidUsageMessage)
				return false
			} else {
				let statCode = switchCheck(msg.channel.guild.id, "modcom_ena", "modcom", "mute_ena", "default")
				if (statCode === 0) {
					if (msg.member.permission.has("manageChannels")) {
						return true
					} else {
						msg.channel.createMessage(`Welk touw en tape?\nMisschien even langs de onderhoud kamer gaan. Denk dat daar nog wat ligt.`).catch((error) => {
							console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
						})
						return false
					}
				} else if (statCode === 1) {
					msg.channel.createMessage(`De Mod Commands module is niet geactiveerd.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else if (statCode === 2) {
					msg.channel.createMessage(`De (un)mute functie is niet aan gezet.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else {
					msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "@iemand [reden]"
})

bot.registerCommand("quote", (msg, args) => {
	msg.channel.createMessage(`Voor dit commando is een subcommando nodig.\nType \`${botPrefix}help quote\` voor meer information.`)
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: [],
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 10000,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: developers.list
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "Ik heb echt net met quotes boek weggelegd.\nGeef me even 10 seconden dan pak ik het weer.",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 1,
	// Object (optional) - Default subcommand options. This object takes the same options as a normal Command
	defaultSubcommandOptions: {
		cooldown: 10000,
		cooldownExclusions: {
			userIDs: developers.list
		},
		cooldownMessage: "Ik heb echt net met quotes boek weggelegd.\nGeef me even 10 seconden dan pak ik het weer.",
		deleteCommand: true,
		guildOnly: false,
		requirements: {
			custom: function(msg) {
				let subComms = []
				let subLabels = Object.keys(msg.command.subcommands)
				let subAliases = []
				
				for (let i = 0; i < subLabels.length; i++) {
					subAliases = subAliases.concat(msg.command.subcommands[subLabels[i]].aliases)
				}
				subComms = subLabels.concat(subAliases)
				if (msg.command.label == "help") {
					return true
				} else if (subComms.indexOf(msg.content.split(" ")[1]) != -1) {
					return true
				} else {
					let statCode = switchCheck(msg.channel.guild.id, "quotes_ena", "quotes", null, "default")
					if (statCode === 0) {
						return true
					} else if (statCode === 1) {
						msg.channel.createMessage(`De Quote's module is niet geactiveerd.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
							console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
						})
						return false
					} else {
						msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
							console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
						})
						return false
					}
				}
			}
		}
	},
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: true,
	// String (optional) - A short description of the command to show in the default help command
	description: "Quotes opslaan en weergeven",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Ben me quotes boekje kwijt.\nMocht ik hem in een paar minuten niet gevonden hebben ga dan even naar het tevredenheids bureau en kijk of hun hem hebben.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Er word vast wel eens genialen dingen gezegd in jouw server. Met deze functie kan je quotes toevoegen en weergeven.",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: true,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// Object (optional) - A set of functions to be executed at different times throughout the command's processing
	hooks: {
		// function (optional) - A function that is executed before any permission or cooldown checks is made. The function is passed the command message and arguments as parameters.
		preCommand: function(cMsg, args) {
			return true
		},
		// function (optional) - A function that is executed after all checks have cleared, but before the command is executed. The function is passed the command message, arguments, and if command checks were passed as parameters.
		postCheck: function(cMsg, args, checks) {
			return true
		},
		// function (optional) - A function that is executed after the command is executed, regardless of the final failed state of the command. The function is passed the command message, arguments, and if execution succeeded as parameters.
		postExecution: function(cMsg, args, success) {
			return true
		},
		// function (optional) - A function that is executed after a response has been posted, and the command has finished processing. The function is passed the command message, arguments, and the response message (if applicable) as parameters.
		postCommand: function(cMsg, args, rMsg) {
			return true
		}
	},
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "Je hebt niet aangegevan wat je wilt doen met het grote quotes boek. En ik ben nog niet helderziend.",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "",
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else {
				let statCode = switchCheck(msg.channel.guild.id, "quotes_ena", "quotes", null, "default")
				if (statCode === 0) {
					return true
				} else if (statCode === 1) {
					msg.channel.createMessage(`De Quote's module is niet geactiveerd.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else {
					msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "<sub command>"
}).registerSubcommand("toevoegen", (msg, args) => {
	roleAccess(settings.web.quotes[msg.channel.guild.id].role_access, msg.member).then(() => {
		if (typeof msg.mentions[0] == 'undefined') {
			msg.channel.createMessage("En wiens naam mag ik opschrijven?").catch((error) => {
				console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
			})
		} else {
			let quote = args.splice(1, args.length).join(" ")
			let dateNow = new Date()
			mysql_con_bot.query("INSERT INTO `quotes` (guild_id, quote, user_id, timestamp, year_k) VALUES ('" + msg.channel.guild.id + "', '" + quote + "', '" + msg.mentions[0].id + "', '" + dateNow.valueOf() + "', '2k" + dateformat(dateNow, "yy") + "')", function(err, result) {
				if (err) {
					console.log(colorScheme.critical, `[MySQL][Critical][${msg.channel.guild.id}] An unexpected error occured while trying to add a quote to the database. See debug for error.`)
					debugMsg(`[`+moduleName+`][`+debugMode+`] Error: `, err)
					msg.channel.createMessage("Vind het geen goeie quote. Probeer het later nog eens.").catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
				} else {
					if (!settings.bot.quotes[msg.channel.guild.id]) {
						settings.bot.quotes[msg.channel.guild.id] = {}
					}
					if (!settings.bot.quotes[msg.channel.guild.id][msg.mentions[0].id]) {
						settings.bot.quotes[msg.channel.guild.id][msg.mentions[0].id] = []
					}
					settings.bot.quotes[msg.channel.guild.id][msg.mentions[0].id].push({
						"id": result.insertId,
						"guild_id": msg.channel.guild.id,
						"quote": quote,
						"user_id": msg.mentions[0].id,
						"timestamp": dateNow.valueOf(),
						"year_k": "2k" + dateformat(dateNow, "yy")
					})
					msg.channel.createMessage(`Quote toegevoegd!.\nGebruik \`${botPrefix}quote show\` voor een random quote of \`${botPrefix}quote show @user\` voor een quote van de gespecifeerde gebruiker.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
				}
			})
		}
	}).catch(() => {
		msg.channel.createMessage(`Sorry maar jouw rol(l)e(n) staat niet in mijn speciale rechten boek.`).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
	})
}, {
	aliases: ["add"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: true,
	// String (optional) - A short description of the command to show in the default help command
	description: "Quote toevoegen",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	guildOnly: true,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Ik ben me best pen kwijt!\nMocht ik hem straks nog niet gevonden hebben vraag even een nieuwe aan het Tevredenheids bureau.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "<> <= Zijn verplicht",
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "Wie en welke quote moet ik opschrijven?",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: function(msg) {
		if (isGuild(msg.channel.guild)) {
			return null
		} else {
			return "Sorry maar deze functie is niet beschikbaar in privé/group chats."
		}
	},
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "<@iemand> <quote>"
}).parentCommand.registerSubcommand("weergeef", (msg, args) => {
	devMsg(`[quote/show][`+debugMode+`][${msg.channel.guild.id}] Quotes show ran! Quote obj: `, settings.bot.quotes[msg.channel.guild.id])
	devMsg(`[quote/show][`+debugMode+`][${msg.channel.guild.id}] Length of keys is ${Object.keys(settings.bot.quotes[msg.channel.guild.id]).length}`)
	if (settings.bot.quotes[msg.channel.guild.id] && Object.keys(settings.bot.quotes[msg.channel.guild.id]).length > 0) {
		let userPickID = randomObj(settings.bot.quotes[msg.channel.guild.id], "key")
		if (typeof(msg.mentions[0]) == "object") {
			if (typeof(settings.bot.quotes[msg.channel.guild.id][msg.mentions[0].id]) == "object") {
				userPickID = msg.mentions[0].id
				devMsg(`[quote/show][`+debugMode+`][${msg.channel.guild.id}] Picked mentioned user quote array: `, userPickID)
			} else {
				msg.channel.createMessage(`Er zijn geen geweldige quotes van deze gebruiker in mijn grote quote boek.`).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning] Message couldn't be sent. Error:`, error)
				})
				return
			}
		} else {
			devMsg(`[quote/show][`+debugMode+`][${msg.channel.guild.id}] Picked user quote array: `, userPickID)
		}
		let quote = settings.bot.quotes[msg.channel.guild.id][userPickID][randomInt(0, settings.bot.quotes[msg.channel.guild.id][userPickID].length-1)]
		devMsg(`[quote/show][`+debugMode+`][${msg.channel.guild.id}] Picked quote: `, quote)
		let member = bot.guilds.get(quote.guild_id).members.get(quote.user_id)
		msg.channel.createMessage(content = {embed: {
			title: "Het grote quote boek",
			description: quote_desc[randomInt(0, quote_desc.length-1)].replace(randomPage_reg, randomInt(1, 100000)),
			color: 0x9b0004,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: member.nick ? member.nick : member.username,
				icon_url: member.avatarURL
			},
			fields: [
				{name: `Quote #${quote.id} gemaakt op ${dateformat(new Date(quote.timestamp), "d-m-yyyy HH:MM:ss")}`, value: `*${quote.quote}*\n*- ${member.nick ? member.nick : member.username} ${quote.year_k}*`, inline: false}
			]
		}}).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning] Embed couldn't be sent. Error:`, error)
		})
	} else {
		msg.channel.createMessage(`Blijkbaar heeft niemand heeft ooit iets geweldigs gezegd in deze server volgens mijn grote quote boek.`).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning] Message couldn't be sent. Error:`, error)
		})
	}
}, {
	aliases: ["show", "weergeven", "random", "willekeurige"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Quote weergeven",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	guildOnly: true,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Ah poep. Ik heb me thee over het grote quote boek laten vallen.\nIk zal even een nieuwe printen. Mocht ik nog niet klaar zijn over een paar minuten ga dan even langs het Tevredenheids bureau.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "[] <= Zijn optioneel",
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "Invalid Usages",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "Permission Error",
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "[@iemand]"
}).parentCommand.registerSubcommand("verwijderen", (msg, args) => {
	roleAccess(settings.web.quotes[msg.channel.guild.id].role_access, msg.member).then(() => {
		if (typeof(msg.mentions[0]) != "object") {
			msg.channel.createMessage(`Van wie wil je een quote verwijderen?`).catch((error) => {
				console.log(colorScheme.warning, `[DiscordShard][Warning] Message couldn't be sent. Error:`, error)
			})
		} else {
			let quoteArr = settings.bot.quotes[msg.channel.guild.id][msg.mentions[0].id]
			let findQID = function(quoteArr, id2F) {
				return new Promise(function(resolve, reject) {
					async.forEachOf(quoteArr, (quoteObj, key, callback) => {
						if (quoteObj.id === id2F) {
							callback(key.toString())
						} else {
							callback()
						}
					}, function(res) {
						if (res) {
							resolve(parseInt(res))
						} else {
							reject()
						}
					})
				})
			}
			findQID(quoteArr, parseInt(args[1])).then((arrID) => {
				mysql_con_bot.query("DELETE FROM `quotes` WHERE id=" + quoteArr[arrID].id, function(err, result) {
					if (err) {
						console.log(`[${moduleName}][Critical][${msg.channel.guild.id}] Failed executing the MySQL query. See debug for error.`)
						debugMsg(`[`+moduleName+`][`+debugMode+`] Error: `, err)
						msg.channel.createMessage(`De pagina waar de quote op staat is zo sterk als staal.\nIk pak even wat gereedschap om deze quote eruit te verwijderen.\nMocht ik het na een paar minuten de quote nog niet uit het boek hebben vraag het Tevredenheids bureau even om assistentie.`).catch((error) => {
							console.log(colorScheme.warning, `[DiscordShard][Warning] Message couldn't be sent. Error:`, error)
						})
					} else {
						delete settings.bot.quotes[msg.channel.guild.id][msg.mentions[0].id][arrID]
						msg.channel.createMessage(`Quote is uit het boek gescheurd! Ik hoop dat niemand de quote gaat missen..`).catch((error) => {
							console.log(colorScheme.warning, `[DiscordShard][Warning] Message couldn't be sent. Error:`, error)
						})
					}
				})
			}).catch(() => {
				msg.channel.createMessage(`Met de opgegeven ID en gebruiker kon ik geen quote vinden om uit met grote quoten boek te scheuren.`).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning] Message couldn't be sent. Error:`, error)
				})
			})
		}
	}).catch(() => {
		msg.channel.createMessage(`Sorry maar jouw rol(l)e(n) staat niet in mijn speciale rechten boek.`).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
	})
}, {
	aliases: ["delete"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: true,
	// String (optional) - A short description of the command to show in the default help command
	description: "Quote verwijderen",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	guildOnly: true,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Mijn papier shredder is kapot.\nMocht ik het in een paar minuten niet gefixt hebben ga dan even naar Tevredenheids bureau.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "<> <= Zijn verplicht",
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "Invalid Usages",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "Permission Error",
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "<@iemand> <id zonder de #>"
})

bot.registerCommand("rip", (msg, args) => {
	let ripCode = function() {
		let mention = msg.author.mention
		if (isGuild(msg.channel.guild) && typeof(msg.mentions[0]) == "object") {
			mention = msg.mentions[0].mention
		}
		msg.channel.createMessage(`${ripText.rip[randomInt(0, ripText.rip.length-1)]} ${mention}. Hij/zij is ${ripText.death[randomInt(0, ripText.rip.length-1)]}.\nEven een moment stilte voor ${mention}.\n\n${ripText.story[randomInt(0, ripText.story.length-1)]}`).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
	}
	if (isGuild(msg.channel.guild)) {
		roleAccess(settings.web.rip[msg.channel.guild.id].role_access, msg.member).then(() => {
			ripCode()
		}).catch(() => {
			msg.channel.createMessage(`Jouw rol(l)e(n) staan niet in mijn zwarte boekje. Dus jij mag geen mensen RIP verklaren.`).catch((error) => {
				console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
			})
		})
	} else {
		ripCode()
	}
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: [],
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 30000,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: developers.list,
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "Oh. Ja ik ben nog bezig met de papieren van de vorige overleden persoon in orde te brengen.\nProbeer het over 30 seconden nog een keer.",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 1,
	// Object (optional) - Default subcommand options. This object takes the same options as a normal Command
	defaultSubcommandOptions: {},
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "R.I.P. momentje",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Hmmm. Iemand heeft het graf steen verplaats. Wat een graf zooi.\nIk ben al aan het zoeken er naar maar mocht ik hem in een paar minuten niet vinden vraag even aan het Tevredenheids bureau of ze een nieuwe willen printen.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Soms heb je z'n rip moment. Maak er iets speciaals.\n\n[] <= Zijn optioneel",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: false,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// Object (optional) - A set of functions to be executed at different times throughout the command's processing
	hooks: {
		// function (optional) - A function that is executed before any permission or cooldown checks is made. The function is passed the command message and arguments as parameters.
		preCommand: function(cMsg, args) {
			return true
		},
		// function (optional) - A function that is executed after all checks have cleared, but before the command is executed. The function is passed the command message, arguments, and if command checks were passed as parameters.
		postCheck: function(cMsg, args, checks) {
			return true
		},
		// function (optional) - A function that is executed after the command is executed, regardless of the final failed state of the command. The function is passed the command message, arguments, and if execution succeeded as parameters.
		postExecution: function(cMsg, args, success) {
			return true
		},
		// function (optional) - A function that is executed after a response has been posted, and the command has finished processing. The function is passed the command message, arguments, and the response message (if applicable) as parameters.
		postCommand: function(cMsg, args, rMsg) {
			return true
		}
	},
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "",
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else {
				if (isGuild(msg.channel.guild)) {
					let statCode = switchCheck(msg.channel.guild.id, "rip_ena", "rip", null, "default")
					if (statCode === 0) {
						return true
					} else if (statCode === 1) {
						msg.channel.createMessage(`De RIP module is niet geactiveerd.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
							console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
						})
						return false
					} else {
						msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
							console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
						})
						return false
					}
				} else {
					return true
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "[@iemand]"
})

bot.registerCommand("rank", (msg, args) => {
	if (settings.bot.ranks[msg.channel.guild.id]) {
		let user = msg.author
		if (typeof(msg.mentions[0]) == "object") {
			if (settings.bot.ranks[msg.channel.guild.id][msg.mentions[0].id]) {
				user = msg.mentions[0]
			} else {
				msg.channel.createMessage(`Of de opgegeven persoon bestaat niet of ik heb geen ranking data van hem/haar.\nZorg dat hij/zij praat dan hou ik de score bij.`)
				return
			}
		}
		let rank = settings.bot.ranks[msg.channel.guild.id][user.id]

		let sortedRanks = Object.keys(settings.bot.ranks[msg.channel.guild.id]).sort(function(a,b) {
			return settings.bot.ranks[msg.channel.guild.id][b].total_exp-settings.bot.ranks[msg.channel.guild.id][a].total_exp
		})
		let rankNumb = "Unknown"
		for (let i = 0; i < sortedRanks.length; i++) {
			if (settings.bot.ranks[msg.channel.guild.id][sortedRanks[i]].user_id === user.id) {
				rankNumb = i+1
			}
		}

		if (rankNumb === 1) {
			rankNumb = ":first_place:"
		} else if (rankNumb === 2) {
			rankNumb = ":second_place:"
		} else if (rankNumb === 3) {
			rankNumb = ":third_place:"
		} else {
			rankNumb = `#${rankNumb}`
		}

		msg.channel.createMessage(content = {embed: {
			title: `${user.username}'s Rank`,
			description: rank_desc[randomInt(0, rank_desc.length-1)],
			color: 0x9b0004,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: `Persoonlijke ranking`,
				icon_url: user.avatarURL
			},
			fields: [
				{name: "Server Rank", value: `${rankNumb}`, inline: false},
				{name: "Huidige EXP", value: `${rank.exp}/${math.eval(def_exp + " + (" + incres_exp + " * " + rank.level + ")")}`, inline: true},
				{name: "Level", value: `${rank.level}`, inline: true},
				{name: "Total EXP", value: `${rank.total_exp}`, inline: true}
			]
		}}).catch((error) => {
			msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
				console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
			})
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Embed couldn't be sent. Error:`, error)
		})
	} else {
		msg.channel.createMessage(`Het ziet er naar uit deze server geen (nog) rankings heeft.\nAls je je eigen ranking wilt zien zorg er dan voor dat je actief bent in deze server en dan hou ik de score bij.`).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
	}
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: [],
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 5000,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: developers.list,
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "Ik heb net je kaartje wegelegd waar je rank informatie op staat.\nIk zal het er even opnieuw bij pakken, geef me 5 seconden.",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 1,
	// Object (optional) - Default subcommand options. This object takes the same options as a normal Command
	defaultSubcommandOptions: {},
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Weergeeft jouw chat rank",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Waar heb ik mijn kaarten bakje met ranks nou gelaten?\nIk ben het voor je aan het zoeken. Mocht ik het in een paar minuten nog niet gevonden hebben haal dan even het Tevredenheids bureau erbij.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Hiermee word alle ranking informatie gegeven van jouw (of the opgegeven persoon)\n\n[] <= Zijn optioneel",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: true,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// Object (optional) - A set of functions to be executed at different times throughout the command's processing
	hooks: {
		// function (optional) - A function that is executed before any permission or cooldown checks is made. The function is passed the command message and arguments as parameters.
		preCommand: function(cMsg, args) {
			return true
		},
		// function (optional) - A function that is executed after all checks have cleared, but before the command is executed. The function is passed the command message, arguments, and if command checks were passed as parameters.
		postCheck: function(cMsg, args, checks) {
			return true
		},
		// function (optional) - A function that is executed after the command is executed, regardless of the final failed state of the command. The function is passed the command message, arguments, and if execution succeeded as parameters.
		postExecution: function(cMsg, args, success) {
			return true
		},
		// function (optional) - A function that is executed after a response has been posted, and the command has finished processing. The function is passed the command message, arguments, and the response message (if applicable) as parameters.
		postCommand: function(cMsg, args, rMsg) {
			return true
		}
	},
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: function(msg) {
		if (isGuild(msg.channel.guild)) {
			return null
		} else {
			return "Sorry maar deze functie is niet beschikbaar in privé/group chats."
		}
	},
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else {
				let statCode = switchCheck(msg.channel.guild.id, "levels_ena", "levels", null, "default")
				if (statCode === 0) {
					return true
				} else if (statCode === 1) {
					msg.channel.createMessage(`De Ranking module is niet geactiveerd.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else {
					msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "[@iemand]"
}).registerSubcommand("lijst", (msg, args) => {
	if (settings.bot.ranks[msg.channel.guild.id]) {
		let fieldsArr = []
		let sortedRanks = Object.keys(settings.bot.ranks[msg.channel.guild.id]).sort(function(a,b) {
			return settings.bot.ranks[msg.channel.guild.id][b].total_exp-settings.bot.ranks[msg.channel.guild.id][a].total_exp
		})
		let len = sortedRanks.length
		let maxLen = sortedRanks.length
		let winnerCounter = 1
		if (sortedRanks.length > 9) {
			len = 9
		}
		for (let i = 0; i < len; i++) {
			let rankInf = settings.bot.ranks[msg.channel.guild.id][sortedRanks[i]]
			let user = bot.users.get(rankInf.user_id)
			if (user.bot) {
				if (len < maxLen-1) {
					len++
				}
			} else {
				let iconTxt = ""
				if (winnerCounter === 1) {
					iconTxt = ":first_place:"
				} else if (winnerCounter === 2) {
					iconTxt = ":second_place:"
				} else if (winnerCounter === 3) {
					iconTxt = ":third_place:"
				}
				fieldsArr.push({
					name: `#${winnerCounter} - ${iconTxt} ${user.username}#${user.discriminator}`,
					value: `Huidige EXP: **${rankInf.exp}/${math.eval(def_exp + " + (" + incres_exp + " * " + rankInf.level + ")")}**\nLevel: **${rankInf.level}**\nTotale EXP: **${rankInf.total_exp}**`,
					inline: false
				})
				winnerCounter++
			}
		}
		fieldsArr.push({
			name: `Volledige ranking`,
			value: `Voor de volledige ranking ga naar:\nhttp://www.discord-frankie.nl/leaderboard.php?guid=${msg.channel.guild.id}`,
			inline: false
		})
		let oneUser = bot.users.get(settings.bot.ranks[msg.channel.guild.id][sortedRanks[0]].user_id)
		msg.channel.createMessage(content = {embed: {
			title: "TOP 10 Ranking",
			description: ranking_desc[randomInt(0, ranking_desc.length-1)].replace(player_web_reg, msg.author.mention),
			color: 0x9b0004,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: `Ranking`,
				icon_url: oneUser.avatarURL
			},
			fields: fieldsArr
		}}).catch((error) => {
			msg.channel.createMessage(msg.command.errorMessage + `\n\nJe kan de ranking ook zien op:\nhttp://www.discord-frankie.nl/leaderboard.php?guid=${msg.channel.guild.id}`).catch((error) => {
				console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
			})
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
	} else {
		msg.channel.createMessage(`Het ziet er naar uit deze server geen (nog) rankings heeft.\nZorg ervoor dat mensen praten in deze server en dan hou ik de score bij.`).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
	}
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["list"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 60000,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: developers.list,
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "Ik heb je net nog de rankings laten zien.\nIk zal het je over 1 minuut weer laten zien.",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 1,
	// Object (optional) - Default subcommand options. This object takes the same options as a normal Command
	defaultSubcommandOptions: {},
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Weergeeft de chat ranking",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Alle kaartjes waar de rankings opstaan zijn door elkaar gehuselt.\nGeef me een paar minuten om dit te corrigeren anders ga even langs het Tevredenheids bureau.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Laat je gebruikers XP en Levels verdienen door gewoon te chatten.",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: true,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// Object (optional) - A set of functions to be executed at different times throughout the command's processing
	hooks: {
		// function (optional) - A function that is executed before any permission or cooldown checks is made. The function is passed the command message and arguments as parameters.
		preCommand: function(cMsg, args) {
			return true
		},
		// function (optional) - A function that is executed after all checks have cleared, but before the command is executed. The function is passed the command message, arguments, and if command checks were passed as parameters.
		postCheck: function(cMsg, args, checks) {
			return true
		},
		// function (optional) - A function that is executed after the command is executed, regardless of the final failed state of the command. The function is passed the command message, arguments, and if execution succeeded as parameters.
		postExecution: function(cMsg, args, success) {
			return true
		},
		// function (optional) - A function that is executed after a response has been posted, and the command has finished processing. The function is passed the command message, arguments, and the response message (if applicable) as parameters.
		postCommand: function(cMsg, args, rMsg) {
			return true
		}
	},
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: function(msg) {
		if (isGuild(msg.channel.guild)) {
			return null
		} else {
			return "Sorry maar deze functie is niet beschikbaar in privé/group chats."
		}
	},
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else {
				let statCode = switchCheck(msg.channel.guild.id, "levels_ena", "levels", null, "default")
				if (statCode === 0) {
					return true
				} else if (statCode === 1) {
					msg.channel.createMessage(`De Ranking module is niet geactiveerd.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else {
					msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: ""
})

bot.registerCommand("waarschuwing", (msg, args) => {
	roleAccess(settings.web.warnings[msg.channel.guild.id].access_roles, msg.member).then(() => {
		if (msg.mentions[0].bot) {
			msg.channel.createMessage(`Je kan bots geen waarschuwing geven.`)
			return
		}
		roleHighCheck(msg.member.roles, msg.channel.guild.members.get(msg.mentions[0].id).roles, msg.channel.guild.roles).then(() => {
			let wsReason = "Geen reden opgegeven"
			if (args[1]) {
				wsReason = args.slice(1, args.length).join(" ")
			}
			let dateNow = new Date()
			if (!settings.bot.warnings[msg.channel.guild.id]) {
				settings.bot.warnings[msg.channel.guild.id] = {}
			}
			if (!settings.bot.warnings[msg.channel.guild.id][msg.mentions[0].id]) {
				settings.bot.warnings[msg.channel.guild.id][msg.mentions[0].id] = []
			}
			let totalWarnings = settings.bot.warnings[msg.channel.guild.id][msg.mentions[0].id].length
			let maxWarnings = ""
			let maxWarningsGUI = ""
			if (settings.web.warnings[msg.channel.guild.id].max_warn_ena === true) {
				maxWarningsGUI = "/"
				maxWarnings = settings.web.warnings[msg.channel.guild.id].max_warn
			}
			if (settings.web.warnings[msg.channel.guild.id].max_warn_ena === true && settings.bot.warnings[msg.channel.guild.id][msg.mentions[0].id].length >= settings.web.warnings[msg.channel.guild.id].max_warn) {
				msg.channel.createMessage(`Het limiet is al bereikt voor de opgegeven persoon.\nIk zal het meteen doorgeven aan het Moderator/Admin team.`).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
				})
				wsReason = settings.bot.warnings[msg.channel.guild.id][msg.mentions[0].id][settings.bot.warnings[msg.channel.guild.id][msg.mentions[0].id].length-1].reason
				let fieldsArr = [
					{name: "Limiet berijkt door", value: ` ${msg.mentions[0].username}#${msg.mentions[0].discriminator}`, inline: true},
					{name: "Totale waarschuwingen", value: `${totalWarnings}${maxWarningsGUI}${maxWarnings}`, inline: true},
					{name: "Laatste reden", value: `*${wsReason}*`, inline: false}
				]
				let channelID = (settings.web.guild_info[msg.channel.guild.id] && settings.web.guild_info[msg.channel.guild.id].ann_hdj_ch) ? settings.web.guild_info[msg.channel.guild.id].ann_hdj_ch : msg.channel.id
				bot.createMessage(channelID, content = {embed: {
					title: `Waarschuwing!`,
					description: warning_limit[randomInt(0, warning_limit.length-1)],
					color: 0x9b0004,
					timestamp: dateNow,
					footer: {
						text: bot.user.username + " " + versionInfo.version,
						icon_url: bot.user.avatarURL
					},
					author: {
						name: `Waarschuwings limiet berijkt`,
						icon_url: msg.mentions[0].avatarURL
					},
					fields: fieldsArr
				}}).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Embed couldn't be sent. Error:`, error)
				})
			} else {
				mysql_con_bot.query(`INSERT INTO \`warnings\`(guild_id, user_id, reason, timestamp, given_by_id) VALUES ('${msg.channel.guild.id}', '${msg.mentions[0].id}', '${wsReason}', ${dateNow.valueOf()}, ${msg.author.id})`, function(err, result) {
					if (err) {
						console.log(colorScheme.critical, `[MySQL][Critical][${msg.channel.guild.id}] An unexpected MySQL query error has occured. See debug for error.`)
						debugMsg(`[`+moduleName+`][`+debugMode+`] Error: `, err)
						msg.channel.createMessage(`Me bonnetjes zijn op. Ik ga het even bijvullen.\nMocht dat niet zijn gelukt over een paar minuten vraag het Tevredenheids bureau even om nieuwe bonnetjes.`).catch((error) => {
							console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
						})
					} else {
						settings.bot.warnings[msg.channel.guild.id][msg.mentions[0].id].push({
							"id": result.insertId,
							"guild_id": msg.channel.guild.id,
							"user_id": msg.mentions[0].id,
							"reason": wsReason,
							"timestamp": dateNow.valueOf(),
							"given_by_id": msg.author.id
						})
						let channelID = (settings.web.guild_info[msg.channel.guild.id] && settings.web.guild_info[msg.channel.guild.id].ann_hdj_ch) ? settings.web.guild_info[msg.channel.guild.id].ann_hdj_ch : msg.channel.id
						bot.createMessage(channelID, content = {embed: {
							title: `Waarschuwing!`,
							description: warning_give[randomInt(0, warning_give.length-1)].replace(player_web_reg, msg.mentions[0].mention),
							color: 0x9b0004,
							timestamp: dateNow,
							footer: {
								text: bot.user.username + " " + versionInfo.version,
								icon_url: bot.user.avatarURL
							},
							author: {
								name: `Waarschuwings Systeem`,
								icon_url: msg.mentions[0].avatarURL
							},
							fields: [
								{name: "Waarschuwing voor", value: ` ${msg.mentions[0].username}#${msg.mentions[0].discriminator}`, inline: true},
								{name: "Totale waarschuwingen", value: `${totalWarnings+1}${maxWarningsGUI}${maxWarnings}`, inline: true},
								{name: "Reden", value: `*${wsReason}*`, inline: false}
							]
						}}).then((nMsg) => {
							if (settings.web.warnings[msg.channel.guild.id].max_warn_ena === true && totalWarnings+1 >= maxWarnings) {
								let fieldsArr = [
									{name: "Limiet berijkt door", value: ` ${msg.mentions[0].username}#${msg.mentions[0].discriminator}`, inline: true},
									{name: "Totale waarschuwingen", value: `${totalWarnings+1}${maxWarningsGUI}${maxWarnings}`, inline: true},
									{name: "Laatste reden", value: `*${wsReason}*`, inline: false}
								]
								let channelID = (settings.web.guild_info[msg.channel.guild.id] && settings.web.guild_info[msg.channel.guild.id].ann_hdj_ch) ? settings.web.guild_info[msg.channel.guild.id].ann_hdj_ch : msg.channel.id
								bot.createMessage(channelID, content = {embed: {
									title: `Waarschuwing!`,
									description: warning_limit[randomInt(0, warning_limit.length-1)],
									color: 0x9b0004,
									timestamp: dateNow,
									footer: {
										text: bot.user.username + " " + versionInfo.version,
										icon_url: bot.user.avatarURL
									},
									author: {
										name: `Waarschuwings limiet berijkt`,
										icon_url: msg.mentions[0].avatarURL
									},
									fields: fieldsArr
								}}).catch((error) => {
									console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Embed couldn't be sent. Error:`, error)
								})
							}
						}).catch((error) => {
							msg.channel.createMessage(`Ik heb de waarschuwing opgeschreven maar ik kon het niet aankondigen in het aankondig kanaal.\nZorg dat ik daar rechten heb om berichten te sturen.`).catch((error) => {
								console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
							})
							console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Embed couldn't be sent. Error:`, error)
						})
					}
				})
			}
		}).catch((err) => {
			if (err) {
				msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
				})
				console.log(colorScheme.critical, `[${moduleName}][Critical][${msg.channel.guild.id}] An unexpected error occured while running 'roleHighCheck()' for 'waarschuwing'. See debug for error`)
				debugMsg(`[`+moduleName+`][`+debugMode+`] Error: `, err)
			} else {
				msg.channel.createMessage(`Jouw hoogste rol in deze server staat gelijk aan of is lager dan de hoogste rol van de persoon die jij een waarschuwing wilt geven.\nZorg dat jouw rol hoger is dan deze persoon als je hem/haar een waarschuwing wilt geven.`).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
				})
			}
		})
	}).catch((err) => {
		msg.channel.createMessage(`NEE, jij mag geen bonnetjes uitprinten!`).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
		console.log(colorScheme.critical, `[${moduleName}][Critical][${msg.channel.guild.id}] Role checking failed for 'waarschuwing'. See debug for error`)
		debugMsg(`[`+moduleName+`][`+debugMode+`] Error: `, err)
	})
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["ws"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: true,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 10000,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: developers.list,
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "Ik heb je net nog de waarschuwingen laten zien.\nIk zal ze over 10 seconden weer laten zien.",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 1,
	// Object (optional) - Default subcommand options. This object takes the same options as a normal Command
	defaultSubcommandOptions: {},
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: true,
	// String (optional) - A short description of the command to show in the default help command
	description: "Geeft een waarschuwing aan de opgegeven gebruiker",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Moment ik moet even iemand een waarschuwing geven.\nMocht ik na een paar minuten er nog niet klaar mee zijn loop dan even naar het Tevredenheids bureau.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "<> <= Zijn verplicht\n[] <= Zijn optioneel",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: true,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// Object (optional) - A set of functions to be executed at different times throughout the command's processing
	hooks: {
		// function (optional) - A function that is executed before any permission or cooldown checks is made. The function is passed the command message and arguments as parameters.
		preCommand: function(cMsg, args) {
			return true
		},
		// function (optional) - A function that is executed after all checks have cleared, but before the command is executed. The function is passed the command message, arguments, and if command checks were passed as parameters.
		postCheck: function(cMsg, args, checks) {
			return true
		},
		// function (optional) - A function that is executed after the command is executed, regardless of the final failed state of the command. The function is passed the command message, arguments, and if execution succeeded as parameters.
		postExecution: function(cMsg, args, success) {
			return true
		},
		// function (optional) - A function that is executed after a response has been posted, and the command has finished processing. The function is passed the command message, arguments, and the response message (if applicable) as parameters.
		postCommand: function(cMsg, args, rMsg) {
			return true
		}
	},
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "Je hebt niet opgegeven wie en/of waarom hij/zij een waarschuwings moet krijgen.",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: function(msg) {
		if (isGuild(msg.channel.guild)) {
			return null
		} else {
			return "Sorry maar deze functie is niet beschikbaar in privé/group chats."
		}
	},
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			let subComms = []
			let subLabels = Object.keys(msg.command.subcommands)
            let subAliases = []
            
            for (let i = 0; i < subLabels.length; i++) {
                subAliases = subAliases.concat(msg.command.subcommands[subLabels[i]].aliases)
            }
            subComms = subLabels.concat(subAliases)
			if (msg.command.label == "help") {
				return true
			} else if (subComms.indexOf(msg.content.split(" ")[1]) != -1) {
				return true
			} else if (typeof(msg.mentions[0]) != "object") {
				msg.channel.createMessage(msg.command.invalidUsageMessage)
				return false
			} else {
				let statCode = switchCheck(msg.channel.guild.id, "warnigns_ena", "warnings", null, "default")
				if (statCode === 0) {
					return true
				} else if (statCode === 1) {
					msg.channel.createMessage(`De Waarschuwings module is niet geactiveerd.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else {
					msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "<@iemand> [reden]"
}).registerSubcommand("lijst", (msg, args) => {
	if (settings.bot.warnings[msg.channel.guild.id]) {
		let fieldsArr = []
		let sortedKeys = Object.keys(settings.bot.warnings[msg.channel.guild.id]).sort(function(a,b) {
			return settings.bot.warnings[msg.channel.guild.id][b].length-settings.bot.warnings[msg.channel.guild.id][a].length
		})

		let maxWarnings = ""
		let maxWarningsGUI = ""
		if (settings.web.warnings[msg.channel.guild.id].max_warn_ena === true) {
			maxWarningsGUI = "/"
			maxWarnings = settings.web.warnings[msg.channel.guild.id].max_warn
		}

		let len = sortedKeys.length
		let maxLen = sortedKeys.length
		let winnerCounter = 1
		if (sortedKeys.length > 9) {
			len = 9
		}
		for (let i = 0; i < len; i++) {
			let warnInfo = settings.bot.warnings[msg.channel.guild.id][sortedKeys[i]]
			let user = bot.users.get(warnInfo[0].user_id)
			if (user.bot) {
				if (len < maxLen-1) {
					len++
				}
			} else {
				let iconTxt = ""
				if (winnerCounter === 1) {
					iconTxt = ":rotating_light:"
				} else if (winnerCounter === 2) {
					iconTxt = ":oncoming_police_car:"
				} else if (winnerCounter === 3) {
					iconTxt = ":cop:"
				}
				fieldsArr.push({
					name: `#${winnerCounter} - ${iconTxt} ${user.username}#${user.discriminator}`,
					value: `Totale waarschuwingen: ${warnInfo.length}${maxWarningsGUI}${maxWarnings}`,
					inline: false
				})
				winnerCounter++
			}
		}
		fieldsArr.push({
			name: `Individuele waarschuwingen`,
			value: `Om quotes van 1 persoon te zien in detail type dan \`${botPrefix}waarschuwing weergeven @iemand\`.`,
			inline: false
		})
		let oneUser = bot.users.get(settings.bot.warnings[msg.channel.guild.id][sortedKeys[0]][0].user_id)
		msg.channel.createMessage(content = {embed: {
			title: "TOP 10 gebruikers met waarschuwingen",
			description: warning_list[randomInt(0, warning_list.length-1)],
			color: 0x9b0004,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: `Waarschuwingen`,
				icon_url: oneUser.avatarURL
			},
			fields: fieldsArr
		}}).catch((error) => {
			msg.channel.createMessage(msg.command.errorMessage + `\n\nMocht je individuele waarschuwingen zien type dan \`${botPrefix}waarschuwing weergeven @iemand\`.`).catch((error) => {
				console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
			})
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Embed couldn't be sent. Error:`, error)
		})
	} else {
		msg.channel.createMessage(`Het ziet er naar uit dat dit de netste server is in heel Discord.\nNog niemand heeft een waarschuwing.`).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
	}
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["list"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 30000,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: developers.list,
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "De waarschuwings lijst veranderd echt niet zo snel.\nOver 1 minuut zal ik ff kijken of er een update is.",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 1,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Weergeeft een lijst met gebruikers en hun waarschuwingen",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "De printer staat vast.\nIk ga hem fixen, maar mocht dit niet lukken binnen een paar minuten ga dan even naar het Tevredenheids bureau en vraag voor een nieuwe printer.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Vanuit deze lijst met waarschuwingen kan je ook individuele waarschuwingen bekijken.",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: true,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// Object (optional) - A set of functions to be executed at different times throughout the command's processing
	hooks: {
		// function (optional) - A function that is executed before any permission or cooldown checks is made. The function is passed the command message and arguments as parameters.
		preCommand: function(cMsg, args) {
			return true
		},
		// function (optional) - A function that is executed after all checks have cleared, but before the command is executed. The function is passed the command message, arguments, and if command checks were passed as parameters.
		postCheck: function(cMsg, args, checks) {
			return true
		},
		// function (optional) - A function that is executed after the command is executed, regardless of the final failed state of the command. The function is passed the command message, arguments, and if execution succeeded as parameters.
		postExecution: function(cMsg, args, success) {
			return true
		},
		// function (optional) - A function that is executed after a response has been posted, and the command has finished processing. The function is passed the command message, arguments, and the response message (if applicable) as parameters.
		postCommand: function(cMsg, args, rMsg) {
			return true
		}
	},
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: function(msg) {
		if (isGuild(msg.channel.guild)) {
			return null
		} else {
			return "Sorry maar deze functie is niet beschikbaar in privé/group chats."
		}
	},
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else {
				let statCode = switchCheck(msg.channel.guild.id, "warnigns_ena", "warnings", null, "default")
				if (statCode === 0) {
					return true
				} else if (statCode === 1) {
					msg.channel.createMessage(`De Waarschuwings module is niet geactiveerd.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else {
					msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: ""
}).parentCommand.registerSubcommand("weergeven", (msg, args) => {
	let user = msg.author
	if (typeof(msg.mentions[0]) == "object") {
		user = msg.mentions[0]
	}
	let warningArr = settings.bot.warnings[msg.channel.guild.id][msg.mentions[0].id]
	let totalWarnings = warningArr.length
	let maxWarnings = ""
	let maxWarningsGUI = ""
	if (settings.web.warnings[msg.channel.guild.id].max_warn_ena === true) {
		maxWarningsGUI = "/"
		maxWarnings = settings.web.warnings[msg.channel.guild.id].max_warn
	}
	let fieldsArr = [
		{name: "Waarschuwing(en) van", value: `${user.username}#${user.discriminator}`, inline: true},
		{name: "Totale waarschuwingen", value: `${totalWarnings}${maxWarningsGUI}${maxWarnings}`, inline: true},
	]
	for (let i = 0; i < totalWarnings; i++) {
		let givenBy = ""
		if (msg.member.permission.has("kickMembers")) {
			let givenByUser = bot.users.get(warningArr[i].given_by_id)
			givenBy = `Gegeven door: **${givenByUser.username}#${givenByUser.discriminator}**\n`
		}
		fieldsArr.push({
			name: `Waarschuwing ${i+1}`,
			value: `ID: **${warningArr[i].id}**\nGegeven op: **${dateformat(new Date(warningArr[i].timestamp), "dd-mm-yyyy HH:MM:ss")}**\n${givenBy}Reden: ***${warningArr[i].reason}***`
		})
	}
	msg.channel.createMessage(content = {embed: {
		title: `${user.username}'s waarschuwingen`,
		description: warning_list[randomInt(0, warning_list.length-1)],
		color: 0x9b0004,
		timestamp: new Date(),
		footer: {
			text: bot.user.username + " " + versionInfo.version,
			icon_url: bot.user.avatarURL
		},
		author: {
			name: `Individuele waarschuwingen`,
			icon_url: user.avatarURL
		},
		fields: fieldsArr
	}}).catch((error) => {
		msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
		console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Embed couldn't be sent. Error:`, error)
	})
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["show"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 30000,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: developers.list,
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "Oh. Uhhh.. Mijn Windows heeft op dit moment een update.\nVraag het over 30 seconden nog een keer.",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 1,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Weergeeft de waarschuwing(en) van de opgegeven gebruiker in detail",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Al mijn bonnetjes zijn door elkaar!\nBen al bezig met het te sorteren maar mocht het over een paar minuten nog niet gesorteerd zijn neen dan contact op met het Tevredenheids bureau.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "[] <= Zijn optioneel",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: true,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// Object (optional) - A set of functions to be executed at different times throughout the command's processing
	hooks: {
		// function (optional) - A function that is executed before any permission or cooldown checks is made. The function is passed the command message and arguments as parameters.
		preCommand: function(cMsg, args) {
			return true
		},
		// function (optional) - A function that is executed after all checks have cleared, but before the command is executed. The function is passed the command message, arguments, and if command checks were passed as parameters.
		postCheck: function(cMsg, args, checks) {
			return true
		},
		// function (optional) - A function that is executed after the command is executed, regardless of the final failed state of the command. The function is passed the command message, arguments, and if execution succeeded as parameters.
		postExecution: function(cMsg, args, success) {
			return true
		},
		// function (optional) - A function that is executed after a response has been posted, and the command has finished processing. The function is passed the command message, arguments, and the response message (if applicable) as parameters.
		postCommand: function(cMsg, args, rMsg) {
			return true
		}
	},
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "Van wie wil je de bonnetjes (waarschuwingen) zien?",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: function(msg) {
		if (isGuild(msg.channel.guild)) {
			return null
		} else {
			return "Sorry maar deze functie is niet beschikbaar in privé/group chats."
		}
	},
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else {
				let statCode = switchCheck(msg.channel.guild.id, "warnigns_ena", "warnings", null, "default")
				if (statCode === 0) {
					return true
				} else if (statCode === 1) {
					msg.channel.createMessage(`De Waarschuwings module is niet geactiveerd.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else {
					msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "[@iemand]"
}).parentCommand.registerSubcommand("aanpassen", (msg, args) => {
	roleAccess(settings.web.warnings[msg.channel.guild.id].access_roles, msg.member).then(() => {
		let changeable = {
			"gegeven_door": "given_by_id",
			"reden": "reason"
		}
		let user = null
		if (msg.channel.guild.members.get(args[0].slice(2, args[0].length-1))) {
			user = msg.channel.guild.members.get(args[0].slice(2, args[0].length-1))
		} else {
			msg.channel.createMessage(`De opgegeven gebruiker blijkt niet te bestaan in deze server.\nGeef iets anders op.`)
			return
		}
		
		let id = null
		if (args[1] && !isNaN(parseInt(args[1]))) {
			id = parseInt(args[1])
		} else {
			msg.channel.createMessage(`Je mist een geldig waarschuwings ID.\nGebruik \`${botPrefix}waarschuwing weergeven @iemand\` om achter z'n ID te komen.`)
			return
		}

		let element = null
		if (args[2] && Object.keys(changeable).indexOf(args[2]) != -1) {
			element = changeable[args[2]]
		} else {
			msg.channel.createMessage(`Je bent vergeten aan te geven welk (geldig) element je wilt wijzigen.\nZie \`${botPrefix}help waarschuwing aanpassen\` voor meer informatie.`)
			return
		}

		let newData = null
		if (args[3] && !msg.channel.guild.members.get(args[3].slice(2, args[3].length-1))) {
			newData = args.slice(3, args.length).join(" ")
		} else if (args[3] && msg.channel.guild.members.get(args[3].slice(2, args[3].length-1))) {
			newData = msg.channel.guild.members.get(args[3].slice(2, args[3].length-1)).id
		} else {
			msg.channel.createMessage(`Je hebt geen nieuwe waarde opgegeven.\nZo kan ik niks aanpassen. :joy:`)
			return
		}

		if (user != null && id != null && element != null && newData != null) {
			if (!settings.bot.warnings[msg.channel.guild.id][user.id]) {
				msg.channel.createMessage(`Deze gebruiker blijkt geen waarschuwingen te hebben.\nDus ik kan hier niks aanpassen.`)
				return
			}
			let warnArr = settings.bot.warnings[msg.channel.guild.id][user.id]
			let warnKey = null
			for (let i = 0; i < warnArr.length; i++) {
				if (warnArr[i].id === id) {
					warnKey = i
				}
			}
			if (warnKey != null) {
				if (warnArr[warnKey].user_id === msg.author.id) {
					msg.channel.createMessage(`Ha ha ha. Leuk geprobeerd.\nMaar ik laat jouw niet je eigen waarschuwingen aanpassen.`)
					return
				} else if (warnArr[warnKey].given_by_id != msg.author.id && warnArr[warnKey].given_by_id != botID && !msg.member.permission.has("administrator")) {
					msg.channel.createMessage(`Alleen de persoon die de waarschuwing heeft gegeven kan de waarschuwing aanpassen of iemand met Administrator rechten.`)
					return
				} else {
					mysql_con_bot.query(`UPDATE \`warnings\` SET ${element}='${newData}' WHERE id=${id} AND guild_id='${msg.channel.guild.id}' AND user_id='${user.id}'`, function(err, result) {
						if (err) {
							msg.channel.createMessage(`Papier is op.\nMocht ik dit niet over een paar minuten hebben opgelost ga dan even naar het Tevredenheids bureau.`).catch((error) => {
								console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
							})
							console.log(colorScheme.warning, `[MySQL][Warning][${msg.channel.guild.id}] An SQL error occured while querying for 'waarschuwing aanpassen'. See debug for error.`)
							debugMsg(`[`+moduleName+`][`+debugMode+`] MySQL Error: `, err)
						} else {
							settings.bot.warnings[msg.channel.guild.id][user.id][warnKey][element] = newData
							msg.channel.createMessage(`Ik heb de aanpassing doorgevoerd!\nMocht je deze willen zien type dan \`${botPrefix}waarschuwing weergeven @${user.username}#${user.discriminator}\`.`).catch((error) => {
								console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
							})
						}
					})
				}
			} else {
				msg.channel.createMessage(`De opgegeven ID kon ik niet vinden bij deze gebruiker als bon.\nControleer de ID en gebruiker en probeer het opnieuw.`).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
				})
			}
		}
	}).catch((err) => {
		msg.channel.createMessage(`NEE, jij mag mijn pen niet gebruiken!`).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
		console.log(colorScheme.critical, `[${moduleName}][Critical][${msg.channel.guild.id}] Role checking failed for 'waarschuwing weergeven'. See debug for error`)
		debugMsg(`[`+moduleName+`][`+debugMode+`] Error: `, err)
	})
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["edit", "change", "veranderen"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: true,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 15000,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: developers.list,
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "Ik ben nog bezig met de vorige bon aan te passen.\nVraag het me over 15 seconden nogmaals.",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 1,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: true,
	// String (optional) - A short description of the command to show in the default help command
	description: "Verander de gegeven waarschuwing",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Me pen is leeg.\nMocht ik over een paar minuten geen nieuwe pen hebben ga dan even langs het Tevredenheids bureau.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Met deze commando kan je (bijna) alles aanpassen van een waarschuwing.\n\nElementen:\n- gegeven_door (nieuwe waarde: @nieuwIemand)\n-reden\n\n<> <= Zijn verplicht",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: true,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// Object (optional) - A set of functions to be executed at different times throughout the command's processing
	hooks: {
		// function (optional) - A function that is executed before any permission or cooldown checks is made. The function is passed the command message and arguments as parameters.
		preCommand: function(cMsg, args) {
			return true
		},
		// function (optional) - A function that is executed after all checks have cleared, but before the command is executed. The function is passed the command message, arguments, and if command checks were passed as parameters.
		postCheck: function(cMsg, args, checks) {
			return true
		},
		// function (optional) - A function that is executed after the command is executed, regardless of the final failed state of the command. The function is passed the command message, arguments, and if execution succeeded as parameters.
		postExecution: function(cMsg, args, success) {
			return true
		},
		// function (optional) - A function that is executed after a response has been posted, and the command has finished processing. The function is passed the command message, arguments, and the response message (if applicable) as parameters.
		postCommand: function(cMsg, args, rMsg) {
			return true
		}
	},
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "Zorg dat je alle informatie aan me door geeft. Ik kan niks met een halve opdracht.",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: function(msg) {
		if (isGuild(msg.channel.guild)) {
			return null
		} else {
			return "Sorry maar deze functie is niet beschikbaar in privé/group chats."
		}
	},
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else if (typeof(msg.mentions[0]) != "object") {
				msg.channel.createMessage(msg.command.invalidUsageMessage)
				return false
			} else {
				let statCode = switchCheck(msg.channel.guild.id, "warnigns_ena", "warnings", null, "default")
				if (statCode === 0) {
					return true
				} else if (statCode === 1) {
					msg.channel.createMessage(`De Waarschuwings module is niet geactiveerd.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else {
					msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "<@iemand> <waarschuwings id> <element> <nieuwe waarde>"
}).parentCommand.registerSubcommand("verwijderen", (msg, args) => {
	roleAccess(settings.web.warnings[msg.channel.guild.id].access_roles, msg.member).then(() => {
		roleHighCheck(msg.member.roles, msg.mentions[0].roles, msg.channel.guild.roles).then(() => {
			let user = null
			if (msg.channel.guild.members.get(args[0].slice(2, args[0].length-1))) {
				user = msg.channel.guild.members.get(args[0].slice(2, args[0].length-1))
			} else {
				msg.channel.createMessage(`De opgegeven gebruiker blijkt niet te bestaan in deze server.\nGeef iets anders op.`)
				return
			}

			let id = null
			if (args[1] && !isNaN(parseInt(args[1]))) {
				id = parseInt(args[1])
			} else {
				msg.channel.createMessage(`Je mist een geldig waarschuwings ID.\nGebruik \`${botPrefix}waarschuwing weergeven @iemand\` om achter z'n ID te komen.`)
				return
			}

			if (!settings.bot.warnings[msg.channel.guild.id][user.id]) {
				msg.channel.createMessage(`Deze gebruiker blijkt geen waarschuwingen te hebben.\nDus ik kan hier niks verwijderen.`)
				return
			}

			if (user != null && id != null) {
				let warnArr = settings.bot.warnings[msg.channel.guild.id][user.id]
				let warnKey = null
				for (let i = 0; i < warnArr.length; i++) {
					if (warnArr[i].id === id) {
						warnKey = i
					}
				}

				if (warnKey != null) {
					if (warnArr[warnKey].user_id === msg.author.id) {
						msg.channel.createMessage(`Ha ha ha. Leuk geprobeerd.\nMaar ik laat jouw niet je eigen waarschuwingen verwijderen.`)
						return
					} else {
						mysql_con_bot.query(`DELETE FROM \`warnings\` WHERE id=${id} AND guild_id='${msg.channel.guild.id}' AND user_id=${user.id}`, function(err, result) {
							if (err) {
								msg.channel.createMessage(`Me kabel van me papier shredder is niet lang genoeg.\nMocht ik dit niet over een paar minuten hebben opgelost ga dan even naar het Tevredenheids bureau.`).catch((error) => {
									console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
								})
								console.log(colorScheme.warning, `[MySQL][Warning][${msg.channel.guild.id}] An SQL error occured while querying for 'waarschuwing verwijderen'. See debug for error.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] MySQL Error: `, err)
							} else {
								settings.bot.warnings[msg.channel.guild.id][user.id].splice(warnKey, 1)
								msg.channel.createMessage(`Waarschuwing verwijderd!\nHoop dat je dit niet stiekem deed. :slight_smile:`).catch((error) => {
									console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
								})
							}
						})
					}
				} else {
					msg.channel.createMessage(`De opgegeven ID kon ik niet vinden bij deze gebruiker als bon.\nControleer de ID en gebruiker en probeer het opnieuw.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
				}
			}
		}).catch((err) => {
			if (err) {
				msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
				})
				console.log(colorScheme.critical, `[${moduleName}][Critical][${msg.channel.guild.id}] An unexpected error occured while running 'roleHighCheck()' for 'waarschuwing'. See debug for error`)
				debugMsg(`[`+moduleName+`][`+debugMode+`] Error: `, err)
			} else {
				msg.channel.createMessage(`Jouw hoogste rol in deze server staat gelijk aan of is lager dan de hoogste rol van de persoon waarvan jij een waarschuwing wilt verwijderen.\nZorg dat jouw rol hoger is dan deze persoon als je zijn/haar waarschuwing wilt verwijderen.`).catch((error) => {
					console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
				})
			}
		})
	}).catch((err) => {
		msg.channel.createMessage(`NEE, jij mag mijn papier shredder niet gebruiken!`).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
		console.log(colorScheme.critical, `[${moduleName}][Critical][${msg.channel.guild.id}] Role checking failed for 'waarschuwing verwijderen'. See debug for error`)
		debugMsg(`[`+moduleName+`][`+debugMode+`] Error: `, err)
	})
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["delete", "del", "rm"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: true,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 20000,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: developers.list,
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "Me papier shredder is nog bezig met de vorige waarschuwing te verwijderen.\nWacht even 20 seconden en dan kan ik de volgende doen.",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 1,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Verwijderen van een opgegeven waarschuwing",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Ah kak me papier shredder staat vast.\nMocht ik mijn papier shredder niet in een paar minuten hebben opgelost ga dan even naar het Tevredenheids bureau en vraag om een nieuwe.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Verwijdert de opgegeven waarschuwing.\n\n<> <= Zijn verplicht",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: true,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// Object (optional) - A set of functions to be executed at different times throughout the command's processing
	hooks: {
		// function (optional) - A function that is executed before any permission or cooldown checks is made. The function is passed the command message and arguments as parameters.
		preCommand: function(cMsg, args) {
			return true
		},
		// function (optional) - A function that is executed after all checks have cleared, but before the command is executed. The function is passed the command message, arguments, and if command checks were passed as parameters.
		postCheck: function(cMsg, args, checks) {
			return true
		},
		// function (optional) - A function that is executed after the command is executed, regardless of the final failed state of the command. The function is passed the command message, arguments, and if execution succeeded as parameters.
		postExecution: function(cMsg, args, success) {
			return true
		},
		// function (optional) - A function that is executed after a response has been posted, and the command has finished processing. The function is passed the command message, arguments, and the response message (if applicable) as parameters.
		postCommand: function(cMsg, args, rMsg) {
			return true
		}
	},
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "Je bent vergeten aan te geven wiens waarschuwing is moest verwijderen en welke.",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: function(msg) {
		if (isGuild(msg.channel.guild)) {
			return null
		} else {
			return "Sorry maar deze functie is niet beschikbaar in privé/group chats."
		}
	},
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else if (typeof(msg.mentions[0]) != "object") {
				msg.channel.createMessage(msg.command.invalidUsageMessage)
				return false
			} else {
				let statCode = switchCheck(msg.channel.guild.id, "warnigns_ena", "warnings", null, "default")
				if (statCode === 0) {
					return true
				} else if (statCode === 1) {
					msg.channel.createMessage(`De Waarschuwings module is niet geactiveerd.\nGa naar het paneel en zet het aan om deze functie te gebruiken.`).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				} else {
					msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
						console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
					})
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "<@iemand> <waarschuwings id>"
})

bot.registerCommand("credits", (msg, args) => {
	let leadDevs = ""
	let devs = ""
	let volunteers = ""
	async.forEach(credits.leaddevelopers, function(ld, callback) {
		let ldUsr = bot.users.get(ld[0])
		if (typeof(ldUsr) == "undefined") {
			leadDevs += ld[1] + " (Gecached)\n"
		} else {
			leadDevs += ldUsr.username + "#" + ldUsr.discriminator + "\n"
		}
		callback()
	}, function(err) {
		if (err) {
			return false
		} else {
			if (leadDevs == "") {
				leadDevs = "<geen>"
			}
		}
	})
	async.forEach(credits.developers, function(dev, callback) {
		let devUsr = bot.users.get(dev[0])
		if (typeof(devUsr) == "undefined") {
			devs += dev[1] + " (Gecached)\n"
		} else {
			devs += devUsr.username + "#" + devUsr.discriminator + "\n"
		}
		callback()
	}, function(err) {
		if (err) {
			return false
		} else {
			if (devs == "") {
				devs = "<geen>"
			}
		}
	})
	async.forEach(credits.volunteers, function(vol, callback) {
		let volUsr = bot.users.get(vol[0])
		if (typeof(volUsr) == "undefined") {
			volunteers += vol[1] + " (Gecached)\n"
		} else {
			volunteers += volUsr.username + "#" + volUsr.discriminator + "\n"
		}
		callback()
	}, function(err) {
		if (err) {
			return false
		} else {
			if (volunteers == "") {
				volunteers = "<geen>"
			}
		}
	})
	msg.channel.createMessage(content = {embed: {
		title: "Frankie's Credits",
		description: "Bedankt jongens en meisjes die geholpen hebben om mij steeds beter te maken! :heart:\nOok een speciale shoutout naar [Cookie/Mee6Bot](https://mee6.xyz/) waar sommige functies gebaseerd op zijn.",
		color: 0x9b0004,
		timestamp: new Date(),
		footer: {
			text: bot.user.username + " " + versionInfo.version,
			icon_url: bot.user.avatarURL
		},
		author: {
			name: "Officiële (Support) Frankie Server (Klik hier om te joinen)",
			url: "https://discord.gg/ksT5t3y",
			icon_url: bot.user.avatarURL
		},
		fields: [
			{name: "Hoofdontwikkelaar", value: leadDevs, inline: true},
			{name: "Ontwikkelaars", value: devs, inline: true},
			{name: "Vrijwillegers (/Module ideeën)", value: volunteers, inline: true}
		]
	}}).catch((error) => {
		msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
		console.log(colorScheme.warning, `[DiscordShard][Warning] Embed couldn't be sent. Error:`, error)
	})
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: [],
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 30000,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: developers.list,
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "Scroll maar terug als je de credits wilt zien luilak. :joy:\nOver 30 seconden zal ik het weer posten.",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 1,
	// Object (optional) - Default subcommand options. This object takes the same options as a normal Command
	defaultSubcommandOptions: {},
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "De credits lijst weergeven",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Ik blijk tijdelijke Alzheimer te hebben.\nMocht ik me niet beter voelen over een paar minuten ga dan naar het Tevredenheids bureau want daar naast zit onze Dokter.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Een lijst weergeven met mensen die geholpen hebben met Frankie.",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: false,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// Object (optional) - A set of functions to be executed at different times throughout the command's processing
	hooks: {
		// function (optional) - A function that is executed before any permission or cooldown checks is made. The function is passed the command message and arguments as parameters.
		preCommand: function(cMsg, args) {
			return true
		},
		// function (optional) - A function that is executed after all checks have cleared, but before the command is executed. The function is passed the command message, arguments, and if command checks were passed as parameters.
		postCheck: function(cMsg, args, checks) {
			return true
		},
		// function (optional) - A function that is executed after the command is executed, regardless of the final failed state of the command. The function is passed the command message, arguments, and if execution succeeded as parameters.
		postExecution: function(cMsg, args, success) {
			return true
		},
		// function (optional) - A function that is executed after a response has been posted, and the command has finished processing. The function is passed the command message, arguments, and the response message (if applicable) as parameters.
		postCommand: function(cMsg, args, rMsg) {
			return true
		}
	},
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "",
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			return true
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: ""
})

bot.registerCommand("versie", (msg, args) => {
	msg.channel.createMessage(content = {embed: {
		title: "Frankie's Versie Informatie",
		description: `${version_desc[randomInt(0, version_desc.length-1)]}\n\nVersie word als volgt weergeven.\nVoorbeeld: *v1.2.3*\nv = versie\n1 = hoeveelste stabliele versie\n2 = hoeveelste beta versie\n3 = hoeveelste alpha versie`,
		color: 0x9b0004,
		timestamp: new Date(),
		footer: {
			text: bot.user.username + " " + versionInfo.version,
			icon_url: bot.user.avatarURL
		},
		author: {
			name: "Officiële (Support) Frankie Server (Klik hier om te joinen)",
			url: "https://discord.gg/ksT5t3y",
			icon_url: bot.user.avatarURL
		},
		fields: [
			{name: "Versie", value: `${versionInfo.versionNumber}`, inline: true},
			{name: "Staat van Frankie", value: `${versionInfo.gitBranch}`, inline: true},
			{name: "Laatst geupdate", value: `${dateformat(versionInfo.latestUpdate.date, "dd-mm-yyyy HH:MM:ss")}`, inline: true},
			{name: "De recenste update", value: `**Update korte samenvatting**\n${versionInfo.latestUpdate.name}\n\n**Hash**\n${versionInfo.latestUpdate.hash}\n\n**Update veranderingen**\n[${versionInfo.latestUpdate.fullHash}](${versionInfo.latestUpdate.commitUrl})`, inline: false}
		]
	}}).catch((error) => {
		msg.channel.createMessage(msg.command.errorMessage).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
		})
		console.log(colorScheme.warning, `[DiscordShard][Warning] Embed couldn't be sent. Error:`, error)
	})
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["version"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 60000,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: developers.list,
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "Mijn versie verandert hoog uit 1x per 2 weken. Dus het is niet nodig om alweer me versie te weergeven.\nOver 1 minuut zal ik je updaten, oké?",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 1,
	// Object (optional) - Default subcommand options. This object takes the same options as a normal Command
	defaultSubcommandOptions: {},
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Weergeeft Frankie's versie",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Volgens mij zit er een schroefje los bij mij.\nMocht dit niet vast zitten binnen een paar minuten contacteer dan even het Tevredenheids bureau.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Weergeeft uitgebreide versie informatie over Frankie.",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: false,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// Object (optional) - A set of functions to be executed at different times throughout the command's processing
	hooks: {
		// function (optional) - A function that is executed before any permission or cooldown checks is made. The function is passed the command message and arguments as parameters.
		preCommand: function(cMsg, args) {
			return true
		},
		// function (optional) - A function that is executed after all checks have cleared, but before the command is executed. The function is passed the command message, arguments, and if command checks were passed as parameters.
		postCheck: function(cMsg, args, checks) {
			return true
		},
		// function (optional) - A function that is executed after the command is executed, regardless of the final failed state of the command. The function is passed the command message, arguments, and if execution succeeded as parameters.
		postExecution: function(cMsg, args, success) {
			return true
		},
		// function (optional) - A function that is executed after a response has been posted, and the command has finished processing. The function is passed the command message, arguments, and the response message (if applicable) as parameters.
		postCommand: function(cMsg, args, rMsg) {
			return true
		}
	},
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "",
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			return true
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: ""
})

bot.registerCommand("apiAuth", (msg, args) => {
	msg.channel.createMessage(msg.command.invalidUsageMessage).catch((error) => {
		console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
	})
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["apiAdmin", "apiAdminAuth"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: true,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: false,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 0,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: [],
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 0,
	// Object (optional) - Default subcommand options. This object takes the same options as a normal Command
	defaultSubcommandOptions: {},
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: true,
	// String (optional) - A short description of the command to show in the default help command
	description: "Verkrijg een API authenticatie sleutel (Alleen voor API administrators)",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: true,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Hmmm ik kon geen sleutel uitprinten met me 3D printer.\nProbeer het later nog een keer of vraag hulp aan het Tevredenheids bureau.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Genereert een API authenticatie sleutel om toegang te krijgen tot de API administrator eindpunten.",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: false,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: true,
	// Object (optional) - A set of functions to be executed at different times throughout the command's processing
	hooks: {
		// function (optional) - A function that is executed before any permission or cooldown checks is made. The function is passed the command message and arguments as parameters.
		preCommand: function(cMsg, args) {
			return true
		},
		// function (optional) - A function that is executed after all checks have cleared, but before the command is executed. The function is passed the command message, arguments, and if command checks were passed as parameters.
		postCheck: function(cMsg, args, checks) {
			return true
		},
		// function (optional) - A function that is executed after the command is executed, regardless of the final failed state of the command. The function is passed the command message, arguments, and if execution succeeded as parameters.
		postExecution: function(cMsg, args, success) {
			return true
		},
		// function (optional) - A function that is executed after a response has been posted, and the command has finished processing. The function is passed the command message, arguments, and the response message (if applicable) as parameters.
		postCommand: function(cMsg, args, rMsg) {
			return true
		}
	},
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "Zorg dat je een subcommando geeft.\nZie ook `" + botPrefix + "help apiAuth`.",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "Jij bent geen API Administrator volgens mijn papieren.\nDus opbokken voordat ik de beveiliging erbij haal!",
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			let subComms = []
			let subLabels = Object.keys(msg.command.subcommands)
            let subAliases = []
            
            for (let i = 0; i < subLabels.length; i++) {
                subAliases = subAliases.concat(msg.command.subcommands[subLabels[i]].aliases)
            }
            subComms = subLabels.concat(subAliases)
			if (msg.command.label == "help") {
				return true
			} else if (subComms.indexOf(msg.content.split(" ")[1]) != -1) {
				return true
			} else {
				if (apiAdmins.indexOf(msg.author.id) != -1) {
					return true
				} else {
					msg.channel.createMessage(msg.command.permissionMessage)
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "<soort sleutel> <optie> [@iemand]"
}).registerSubcommand("oneTime", (msg, args) => {
	msg.channel.createMessage(msg.command.invalidUsageMessage).catch((error) => {
		console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
	})
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["ot"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: true,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: false,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 0,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: [],
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 0,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Verkrijg een eenmalige API authenticatie sleutel (Alleen voor API administrators)",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: true,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Hmmm ik kon geen sleutel uitprinten met me 3D printer.\nProbeer het later nog een keer of vraag hulp aan het Tevredenheids bureau.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Genereert een eenmalige API authenticatie sleutel om eenmalig toegang te krijgen tot de API administrator eindpunten.\nTeminste als je er nog geen hebt.",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: false,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// Object (optional) - A set of functions to be executed at different times throughout the command's processing
	hooks: {
		// function (optional) - A function that is executed before any permission or cooldown checks is made. The function is passed the command message and arguments as parameters.
		preCommand: function(cMsg, args) {
			return true
		},
		// function (optional) - A function that is executed after all checks have cleared, but before the command is executed. The function is passed the command message, arguments, and if command checks were passed as parameters.
		postCheck: function(cMsg, args, checks) {
			return true
		},
		// function (optional) - A function that is executed after the command is executed, regardless of the final failed state of the command. The function is passed the command message, arguments, and if execution succeeded as parameters.
		postExecution: function(cMsg, args, success) {
			return true
		},
		// function (optional) - A function that is executed after a response has been posted, and the command has finished processing. The function is passed the command message, arguments, and the response message (if applicable) as parameters.
		postCommand: function(cMsg, args, rMsg) {
			return true
		}
	},
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "Zorg dat je een subcommando geeft.\nZie ook `" + botPrefix + "help apiAuth oneTime`.",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "Jij bent geen API Administrator volgens mijn papieren.\nDus opbokken voordat ik de beveiliging erbij haal!",
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			let subComms = []
			let subLabels = Object.keys(msg.command.subcommands)
            let subAliases = []
            
            for (let i = 0; i < subLabels.length; i++) {
                subAliases = subAliases.concat(msg.command.subcommands[subLabels[i]].aliases)
            }
            subComms = subLabels.concat(subAliases)
			if (msg.command.label == "help") {
				return true
			} else if (subComms.indexOf(msg.content.split(" ")[1]) != -1) {
				return true
			} else {
				if (apiAdmins.indexOf(msg.author.id) != -1) {
					return true
				} else {
					msg.channel.createMessage(msg.command.permissionMessage)
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "<optie> [@iemand]"
}).registerSubcommand("show", (msg, args) => {
	apiKeyFinder(msg.author.id, "key").then((result) => {
		let authKeyObj = apiAuthKeys[result]
		let expired = ((new Date(apiAuthKeys[result].expires) > new Date()) ? "Geldig" : "**Verlopen**")
		msg.channel.createMessage(content = {embed: {
			title: "API Administrator Authenticatie",
			description: "Dit is jouw eenmalige API Authenticatie sleutel.\nDeze sleutel word verwijderd als je een succesvolle API aanvraag doet.\nOok word deze sleutel verwijderd als de bot opnieuw opstart.",
			color: 0xcc0000,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: msg.author.username,
				icon_url: msg.author.avatarURL
			},
			fields: [
				{name: "Gebruikers ID", value: authKeyObj.userID, inline: false},
				{name: "Sleutel", value: authKeyObj.key, inline: false},
				{name: "Verloopt op", value: `${dateformat(new Date(authKeyObj.expires), 'dd/mm/yyyy HH:MM:ss')} (${expired})`, inline: false}
			]
		}}).catch((error) => {
			console.log(colorScheme.warning, `[Discord-REST_API][Warning] Embed couldn't be sent. Error:`, error)
		})
		logger(apiAKfilename, `User ${msg.author.username}#${msg.author.discriminator} (${msg.author.id}) requested to SHOW his/her key.`).catch((error) => {
			console.log(colorScheme.warning, `[Promise][Warning] Logger function failed. Error:`, error)
		})
	}).catch(() => {
		msg.channel.createMessage(content = {embed: {
			title: "API Administrator Authenticatie",
			description: "Je hebt nog geen sleutel.",
			color: 0xcc0000,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: msg.author.username,
				icon_url: msg.author.avatarURL
			},
			fields: [
				{name: "Antwoord", value: "Verkeerde optie. Beschikbare opties: CREATE", inline: false}
			]
		}}).catch((error) => {
			console.log(colorScheme.warning, `[Discord-REST_API][Warning] Message couldn't be sent. Error:`, error)
		})
	})
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["weergeven"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 0,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: [],
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 0,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Weergeeft jouw eenmalige API sleutel",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: true,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Weergeeft jouw eenmalige API sleutel.\nTenminste als je er één hebt.",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: false,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// Object (optional) - A set of functions to be executed at different times throughout the command's processing
	hooks: {
		// function (optional) - A function that is executed before any permission or cooldown checks is made. The function is passed the command message and arguments as parameters.
		preCommand: function(cMsg, args) {
			return true
		},
		// function (optional) - A function that is executed after all checks have cleared, but before the command is executed. The function is passed the command message, arguments, and if command checks were passed as parameters.
		postCheck: function(cMsg, args, checks) {
			return true
		},
		// function (optional) - A function that is executed after the command is executed, regardless of the final failed state of the command. The function is passed the command message, arguments, and if execution succeeded as parameters.
		postExecution: function(cMsg, args, success) {
			return true
		},
		// function (optional) - A function that is executed after a response has been posted, and the command has finished processing. The function is passed the command message, arguments, and the response message (if applicable) as parameters.
		postCommand: function(cMsg, args, rMsg) {
			return true
		}
	},
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "Jij bent geen API Administrator volgens mijn papieren.\nDus opbokken voordat ik de beveiliging erbij haal!",
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else {
				if (apiAdmins.indexOf(msg.author.id) != -1) {
					return true
				} else {
					msg.channel.createMessage(msg.command.permissionMessage)
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: ""
}).parentCommand.registerSubcommand("create", (msg, args) => {
	apiKeyFinder(msg.author.id, "key").then((result) => {
		msg.channel.createMessage(content = {embed: {
			title: "API Administrator Authenticatie",
			description: "Je hebt al een sleutel.",
			color: 0xcc0000,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: msg.author.username,
				icon_url: msg.author.avatarURL
			},
			fields: [
				{name: "Antwoord", value: "Verkeerde optie. Beschikbare opties: SHOW, DELETE", inline: false}
			]
		}}).catch((error) => {
			console.log(colorScheme.warning, `[Discord-REST_API][Warning] Embed couldn't be sent. Error:`, error)
		})
	}).catch(() => {
		let expDate = new Date()
		expDate.setDate(expDate.getDate() + 1)
		let authKey = md5(new Date().valueOf())
		apiAuthKeys[authKey] = {}
		apiAuthKeys[authKey].userID = msg.author.id
		apiAuthKeys[authKey].key = authKey
		apiAuthKeys[authKey].expires = expDate.valueOf()
		msg.channel.createMessage(content = {embed: {
			title: "API Administrator Authenticatie",
			description: "Dit is jouw eenmalige API Authenticatie sleutel.\nDeze sleutel word verwijderd als je een succesvolle API aanvraag doet.\nOok word deze sleutel verwijderd als de bot opnieuw opstart.",
			color: 0xcc0000,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: msg.author.username,
				icon_url: msg.author.avatarURL
			},
			fields: [
				{name: "Gebruikers ID", value: msg.author.id + " (Dit is jouw Discord Gebruikers ID)", inline: false},
				{name: "Sleutel", value: authKey, inline: false},
				{name: "Verloopt op", value: dateformat(expDate, 'dd/mm/yyyy HH:MM:ss') + ` (Geldig)`, inline: false}
			]
		}}).catch((error) => {
			console.log(colorScheme.warning, `[Discord-REST_API][Warning] Embed couldn't be sent. Error:`, error)
		})
		logger(apiAKfilename, `User ${msg.author.username}#${msg.author.discriminator} (${msg.author.id}) requested to CREATE a (new) key.`).catch((error) => {
			console.log(colorScheme.warning, `[Promise][Warning] Logger function failed. Error:`, error)
		})
	})
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["aanmaken"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 0,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: [],
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 0,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Genereert jouw eenmalige API sleutel",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: true,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Genereert jouw eenmalige API sleutel.\nTenminste als je er nog geen hebt.",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: false,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "Jij bent geen API Administrator volgens mijn papieren.\nDus opbokken voordat ik de beveiliging erbij haal!",
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else {
				if (apiAdmins.indexOf(msg.author.id) != -1) {
					return true
				} else {
					msg.channel.createMessage(msg.command.permissionMessage)
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: ""
}).parentCommand.registerSubcommand("delete", (msg, args) => {
	apiKeyFinder(msg.author.id, "key").then((result) => {
		delete apiAuthKeys[result]
		msg.channel.createMessage(content = {embed: {
			title: "API Administrator Authenticatie",
			description: "Sleutel verwijderd!",
			color: 0xcc0000,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: msg.author.username,
				icon_url: msg.author.avatarURL
			},
			fields: [
				{name: "Antwoord", value: "Sleutel is verwijderd!", inline: false}
			]
		}}).catch((error) => {
			console.log(colorScheme.warning, `[Discord-REST_API][Warning] Embed couldn't be sent. Error:`, error)
		})
		logger(apiAKfilename, `User ${msg.author.username}#${msg.author.discriminator} (${msg.author.id}) requested to DELETE his/her key.`).catch((error) => {
			console.log(colorScheme.warning, `[Promise][Warning] Logger function failed. Error:`, error)
		})
	}).catch(() => {
		msg.channel.createMessage(content = {embed: {
			title: "API Administrator Authenticatie",
			description: "Je hebt nog geen sleutel.",
			color: 0xcc0000,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: msg.author.username,
				icon_url: msg.author.avatarURL
			},
			fields: [
				{name: "Antwoord", value: "Verkeerde optie. Beschikbare opties: CREATE", inline: false}
			]
		}}).catch((error) => {
			console.log(colorScheme.warning, `[Discord-REST_API][Warning] Message couldn't be sent. Error:`, error)
		})
	})
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["verwijderen"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 0,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: [],
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 0,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Verwijderd jouw eenmalige API sleutel",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: true,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Verwijderd jouw eenmalige API sleutel.\nTenminste als je er één hebt.",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: false,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "Jij bent geen API Administrator volgens mijn papieren.\nDus opbokken voordat ik de beveiliging erbij haal!",
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else {
				if (apiAdmins.indexOf(msg.author.id) != -1) {
					return true
				} else {
					msg.channel.createMessage(msg.command.permissionMessage)
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: ""
}).parentCommand.parentCommand.registerSubcommand("SEMI", (msg, args) => {
	msg.channel.createMessage(msg.command.invalidUsageMessage).catch((error) => {
		console.log(colorScheme.warning, `[DiscordShard][Warning][${msg.channel.guild.id}] Message couldn't be sent. Error:`, error)
	})
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["semi"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: true,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: false,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 0,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: [],
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 0,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Verkrijg een SEMI API authenticatie sleutel (Alleen voor API administrators)",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: true,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Hmmm ik kon geen sleutel uitprinten met me 3D printer.\nProbeer het later nog een keer of vraag hulp aan het Tevredenheids bureau.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Genereert een SEMI API authenticatie sleutel om eenmalig toegang te krijgen tot de API administrator eindpunten.\nTeminste als je er nog geen hebt.",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: false,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "Zorg dat je een subcommando geeft.\nZie ook `" + botPrefix + "help apiAuth oneTime`.",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "Jij bent geen API Administrator volgens mijn papieren.\nDus opbokken voordat ik de beveiliging erbij haal!",
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			let subComms = []
			let subLabels = Object.keys(msg.command.subcommands)
            let subAliases = []
            
            for (let i = 0; i < subLabels.length; i++) {
                subAliases = subAliases.concat(msg.command.subcommands[subLabels[i]].aliases)
            }
            subComms = subLabels.concat(subAliases)
			if (msg.command.label == "help") {
				return true
			} else if (subComms.indexOf(msg.content.split(" ")[1]) != -1) {
				return true
			} else {
				if (apiAdmins.indexOf(msg.author.id) != -1) {
					return true
				} else {
					msg.channel.createMessage(msg.command.permissionMessage)
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "<optie> [@iemand]"
}).registerSubcommand("show", (msg, args) => {
	apiKeyFinder(msg.author.id, "semiKey").then((result) => {
		let authKeyObj = apiAuthKeys[result]
		msg.channel.createMessage(content = {embed: {
			title: "API Administrator Authenticatie",
			description: "SEMI Keys kunnen niet weergeven worden.",
			color: 0xcc0000,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: targetUser.username,
				icon_url: targetUser.avatarURL
			},
			fields: [
				{name: "Antwoord", value: "Vanwege ons beveiligings systeem kunnen we geen Semi-permanente API sleutels weergeven.\nDe reden is omdat wij niet meer de rauwe sleutel hebben.\nHet systeem versleuted de sleutel en dat is de enige sleutel die we hebben opgeslagen is de versleutelde.\n\nMaar je kan nog steeds je SEMI ker verwijderen en een nieuwe maken.", inline: false}
			]
		}}).catch((error) => {
			console.log(colorScheme.warning, `[Discord-REST_API][Warning] Embed couldn't be sent. Error:`, error)
		})
	}).catch(() => {
		msg.channel.createMessage(content = {embed: {
			title: "API Administrator Authenticatie",
			description: "Je hebt nog geen SEMI sleutel.",
			color: 0xcc0000,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: msg.author.username,
				icon_url: msg.author.avatarURL
			},
			fields: [
				{name: "Antwoord", value: "Verkeerde optie. Beschikbare opties: CREATE", inline: false}
			]
		}}).catch((error) => {
			console.log(colorScheme.warning, `[Discord-REST_API][Warning] Message couldn't be sent. Error:`, error)
		})
	})
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["weergeven"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 0,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: [],
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 0,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Weergeeft jouw SEMI API sleutel",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: true,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Weergeeft jouw SEMI API sleutel.\nTenminste als je er één hebt.",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: false,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "Jij bent geen API Administrator volgens mijn papieren.\nDus opbokken voordat ik de beveiliging erbij haal!",
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else {
				if (apiAdmins.indexOf(msg.author.id) != -1) {
					return true
				} else {
					msg.channel.createMessage(msg.command.permissionMessage)
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: ""
}).parentCommand.registerSubcommand("create", (msg, args) => {
	apiKeyFinder(msg.author.id, "key").then(() => {
		msg.channel.createMessage(content = {embed: {
			title: "API Administrator Authenticatie",
			description: "Je hebt al een SEMI sleutel.",
			color: 0xcc0000,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: msg.author.username,
				icon_url: msg.author.avatarURL
			},
			fields: [
				{name: "Antwoord", value: "Verkeerde optie. Beschikbare opties: SHOW, DELETE", inline: false}
			]
		}}).catch((error) => {
			console.log(colorScheme.warning, `[Discord-REST_API][Warning] Embed couldn't be sent. Error:`, error)
		})
	}).catch(() => {
		let targetUser = null
		if (bot.users.get(args[0])) {
			targetUser = bot.users.get(args[0])
		} else if (args[0]) {
			targetUser = undefined
		} else {
			targetUser = msg.author
		}
		let expDate = "semi-permanent"
		let authKey = md5(new Date().valueOf())
		apiAuthKeys[md5(authKey)] = {}
		apiAuthKeys[md5(authKey)].userID = targetUser.id
		apiAuthKeys[md5(authKey)].key = md5(authKey)
		apiAuthKeys[md5(authKey)].expires = expDate
		apiSemiKeysIO().catch(() => {
			// Nothing?
		})
		if (targetUser.id != msg.author.id) {
			bot.getDMChannel(targetUser.id).then((dmChnl) => {
				dmChnl.createMessage(content = {embed: {
					title: "API Admin Authentication",
					description: `GOEEEEEEEEEEEEEEEEEEEEEEEEEEEEDE MORGEN POSTCODE KANJER! JA DAG ${targetUser.username} OP EEN DISCORD VAN 40.000 EUROOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO!\n\n${msg.author.id} gaf toegang om de Admin API eindpunten te gebruiken. Dus bij deze een SEMI Admin API sleutel\nSleutel zal resseten als je een succesvolle API aanvraag doet en er zal een nieuwe SEMI sleutel worden meegegeven met die zelfde aanvraag.\nDit bericht word verwijderd in 5 minuten.\n**Deze sleutel kan je niet meer opnieuw zien dus schrijf het ergens op!** \n*(Je kan Androiddd#4682 contacteren om je SEMI sleutel te laten verwijderen)*`,
					color: 0xcc0000,
					timestamp: new Date(),
					footer: {
						text: bot.user.username + " " + versionInfo.version,
						icon_url: bot.user.avatarURL
					},
					author: {
						name: targetUser.username,
						icon_url: targetUser.avatarURL
					},
					fields: [
						{name: "Gebruikers ID", value: targetUser.id, inline: false},
						{name: "Sleutel", value: authKey, inline: false},
						{name: "Verloopt op", value: `Never, resets on a succesfull request.`, inline: false}
					]
				}}).then((newMsg) => {
					setTimeout(function() {
						newMsg.delete()
					}, 300000)
				}).catch((error) => {
					console.log(colorScheme.warning, `[Discord-REST_API][Warning] Message couldn't be sent. Error:`, error)
				})
			}).catch((error) => {
				console.log(colorScheme.warning, `[Discord-REST_API][Warning] DM Channel couldn't be found. Error:`, error)
			})
		}
		msg.channel.createMessage(content = {embed: {
			title: "API Admin Authentication",
			description: `SEMI API Admin Auth Key generated for ${targetUser.username}#${targetUser.discriminator}.\nSleutel zal resseten als je een succesvolle API aanvraag doet en er zal een nieuwe SEMI sleutel worden meegegeven met die zelfde aanvraag.\nDit bericht word verwijderd in 2 minuten.\n**Deze sleutel kan je niet meer opnieuw zien dus schrijf het ergens op!** \n*(Je kan de sleutel altijd nog verwijderen en een nieuwe maken)*`,
			color: 0xcc0000,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: targetUser.username,
				icon_url: targetUser.avatarURL
			},
			fields: [
				{name: "Key's Owner User ID", value: targetUser.id, inline: false},
				{name: "API Admin Auth Key", value: authKey, inline: false},
				{name: "Key Expires On", value: `Never, resets on a succesfull request.`, inline: false}
			]
		}}).then((newMsg) => {
			setTimeout(function() {
				newMsg.delete()
			}, 120000)
		}).catch((error) => {
			console.log(colorScheme.warning, `[Discord-REST_API][Warning] Message couldn't be sent. Error:`, error)
		})
		logger(apiAKfilename, `User ${msg.author.username}#${msg.author.discriminator} (${msg.author.id}) requested to CREATE a (new) SEMI PERMANENT key.`).catch((error) => {
			console.log(colorScheme.warning, `[Promise][Warning] Logger function failed. Error:`, error)
		})
	})
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["aanmaken"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 0,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: [],
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 0,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Genereert jouw SEMI API sleutel",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: true,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Genereert jouw SEMI API sleutel.\nTenminste als je er nog geen hebt.",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: false,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "Jij bent geen API Administrator volgens mijn papieren.\nDus opbokken voordat ik de beveiliging erbij haal!",
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else {
				if (apiAdmins.indexOf(msg.author.id) != -1) {
					return true
				} else {
					msg.channel.createMessage(msg.command.permissionMessage)
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: ""
}).parentCommand.registerSubcommand("delete", (msg, args) => {
	let targetUser = null
	if (bot.users.get(args[2])) {
		targetUser = bot.users.get(args[2])
	} else if (args[2]) {
		targetUser = undefined
	} else {
		targetUser = msg.author
	}
	apiKeyFinder(msg.author.id, "key").then((result) => {
		delete apiAuthKeys[result]
		apiSemiKeysIO().catch(() => {
			// Nothing?
		})
		if (targetUser.id != msg.author.id) {
			bot.getDMChannel(targetUser.id).then((dmChnl) => {
				dmChnl.createMessage(content = {embed: {
					title: "API Administrator Authenticatie",
					description: "SEMI Sleutel verwijderd!",
					color: 0xcc0000,
					timestamp: new Date(),
					footer: {
						text: bot.user.username + " " + versionInfo.version,
						icon_url: bot.user.avatarURL
					},
					author: {
						name: targetUser.username,
						icon_url: targetUser.avatarURL
					},
					fields: [
						{name: "Antwoord", value: "Je SEMI API sleutel is verwijderd.\nAls je een nieuwe wilt hebben contacteer dan Androiddd#4682", inline: false}
					]
				}}).catch((error) => {
					console.log(colorScheme.warning, `[Discord-REST_API][Warning] Embed couldn't be sent. Error:`, error)
				})
			}).catch((error) => {
				console.log(colorScheme.warning, `[Discord-REST_API][Warning] DM Channel couldn't be found. Error:`, error)
			})
		}
		msg.channel.createMessage(content = {embed: {
			title: "API Administrator Authenticatie",
			description: "SEMI Sleutel verwijderd!",
			color: 0xcc0000,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: targetUser.username,
				icon_url: targetUser.avatarURL
			},
			fields: [
				{name: "Antwoord", value: "Sleutel is verwijderd!", inline: false}
			]
		}}).catch((error) => {
			console.log(colorScheme.warning, `[Discord-REST_API][Warning] Message couldn't be sent. Error:`, error)
		})
		logger(apiAKfilename, `User ${msg.author.username}#${msg.author.discriminator} (${msg.author.id}) requested to DELETE his/her SEMI-PERMANENT key.`).catch((error) => {
			console.log(colorScheme.warning, `[Promise][Warning] Logger function failed. Error:`, error)
		})
	}).catch(() => {
		msg.channel.createMessage(content = {embed: {
			title: "API Administrator Authenticatie",
			description: "Je hebt nog geen SEMI sleutel.",
			color: 0xcc0000,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: msg.author.username,
				icon_url: msg.author.avatarURL
			},
			fields: [
				{name: "Antwoord", value: "Verkeerde optie. Beschikbare opties: CREATE", inline: false}
			]
		}}).catch((error) => {
			console.log(colorScheme.warning, `[Discord-REST_API][Warning] Message couldn't be sent. Error:`, error)
		})
	})
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: ["verwijderen"],
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 0,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: [],
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 0,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Verwijderd jouw SEMI API sleutel",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: true,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Verwijderd jouw SEMI API sleutel.\nTenminste als je er één hebt.",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: false,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: false,
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "Jij bent geen API Administrator volgens mijn papieren.\nDus opbokken voordat ik de beveiliging erbij haal!",
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(msg) {
			if (msg.command.label == "help") {
				return true
			} else {
				if (apiAdmins.indexOf(msg.author.id) != -1) {
					return true
				} else {
					msg.channel.createMessage(msg.command.permissionMessage)
					return false
				}
			}
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: ""
})

bot.registerCommand("debug", (msg, _args) => {
	return msg.command.invalidUsageMessage
}, {
	// Array<String> (optional) - An array of command aliases
	aliases: [],
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// Boolean (optional) - Whether the command label (and aliases) is case insensitive or not
	caseInsensitive: true,
	// Number (optional) - The cooldown between command usage in milliseconds
	cooldown: 12500,
	// Object (optional) - A set of factors that limit where cooldowns are active
	cooldownExclusions: {
		// Array<String> - An array of user IDs representing users that are not affected by cooldowns.
		userIDs: developers.list,
		// Array<String> - An array of guild IDs representing guilds that are not affected by cooldowns.
		guildIDs: [],
		// Array<String> - An array of channel IDs representing channels that are not affected by cooldowns.
		channelIDs: []
	},
	// function | String (optional) - A string or a function that returns a string to show when the command is on cooldown. The function is passed the Message object as a parameter.
	cooldownMessage: "Wacht even 12.5 seconden voordat je dit commando weer gebruikt.",
	// Number (optional) - Number of times to return a message when the command is used during it's cooldown. Once the cooldown expires this is reset. Set this to 0 to always return a message.
	cooldownReturns: 1,
	// Object (optional) - Default subcommand options. This object takes the same options as a normal Command
	defaultSubcommandOptions: {
		caseInsensitive: true,
		cooldown: 12500,
		cooldownExclusions: {
			userIDs: developers.list
		},
		cooldownReturns: 1,
		cooldownMessage: "Wacht even 12.5 seconden voordat je dit commando weer gebruikt.",
		errorMessage: "Ik zei nog zo, niet aan dit commando zitten!\nNou heb je het gesloopt *zucht*.\nEn nee het Tevredenheids bureau gaat je niet helpen met dit probleem."
	},
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Weergeeft debug informatie",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	// function | String (optional) - A string or a function that returns a string to show if the execution of the command handler somehow fails. The function is passed the Message object as a parameter.
	errorMessage: "Ik zei nog zo, niet aan dit commando zitten!\nNou heb je het gesloopt *zucht*.\nEn nee het Tevredenheids bureau gaat je niet helpen met dit probleem.",
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Weergeeft debug informatie.\nHeeft een subcommando nodig",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: false,
	// Boolean (optional) - Whether or not the command should be hidden from the default help command list.
	hidden: true,
	// function | String (optional) - A string or a function that returns a string to show when a command was improperly used. The function is passed the Message object as a parameter.
	invalidUsageMessage: "Dit commando heeft een subcommando nodig.",
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "",
	// function | String (optional) - An array of objects specifying reaction buttons/`emoji` specifies the button emoji. Custom emojis should be in format `emojiName:emojiID`/`type` specifies the type of the reaction button, either "edit" or "cancel"/`response` specifies the content to edit the message to when the reaction button is pressed. This accepts the same arguments as the `generator` parameter of this function, but with an extra userID parameter for generator functions (`function(msg, args, userID)`) describing the user that made the reaction
	reactionButtons: [],
	// Number (optional) - Time (in milliseconds) to wait before invalidating the command's reaction buttons
	reactionButtonTimeout: 60000,
	// Object (optional) - A set of factors that limit who can call the command
	requirements: {
		// function | Array<String> (optional) - An array or a function that returns an array of user IDs representing users that can call the command. The function is passed the Message object as a parameter.
		userIDs: [],
		// function | Object (optional) - An object or a function that returns an object containing permission keys the user must match to use the command. The function is passed the Message object as a parameter.
		permissions: {},
		// function | Array<String> (optional) - An array or a function that returns an array of role IDs that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleIDs: [],
		// function | Array<String> (optional) - An array or a function that returns an array of role names that would allow a user to use the command. The function is passed the Message object as a parameter.
		roleNames: [],
		// function (optional) - A function that accepts a message and returns true if the command should be run
		custom: function(_msg) {
			return true
		}
	},
	// Boolean (optional) - Whether or not to restart a command's cooldown every time it's used.
	restartCooldown: false,
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "<subcommando>"
}).registerSubcommand("settings", (msg, args) => {
	let sendMessage = function() {
		devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] fieldsArr(/fR) before sending: `, fieldsArr)
		msg.channel.createMessage(content = {embed: {
			title: `Debug settings from ${infoType}`,
			description: `Debug settings of **${guild.name}** (*${guild.id}*)`,
			color: 0x668cff,
			timestamp: new Date(),
			footer: {
				text: bot.user.username + " " + versionInfo.version,
				icon_url: bot.user.avatarURL
			},
			author: {
				name: "fr!debug settings",
				icon_url: bot.user.avatarURL
			},
			fields: fieldsArr
		}}).catch((error) => {
			console.log(colorScheme.warning, `[DiscordShard][Warning] Embed couldn't be sent. Error:`, error)
			msg.channel.createMessage(`Het is me niet gelukt het bericht te sturen. Sorry.`).catch((error) => {
				console.log(colorScheme.warning, `[DiscordShard][Warning] Message couldn't be sent. Error:`, error)
			})
		})
	}
	let infoType = "memory"
	if (args[0]) {
		if (args[0] == "memory" || args[0] == "database" || args[0] == "diff") {
			infoType = args[0]
		} else {
			return "Je eerste argument is niet een van de volgende: `memory, database, both`"
		}
	}
	let guild = msg.channel.guild
	if (args[1]) {
		if (developers.list.indexOf(msg.author.id) != -1) {
			guild = bot.guilds.get(args[1])
			if (!guild) {
				return "Je tweede argument is geen server of ik zit niet in deze server."
			}
		} else {
			return "OI! JA DAT ZAG IK! Jij probeert in ander mans servers te kijken!!! A-OH!\nLopen! want anders haal ik de Politie der bij, dat willen jullie niet hé?!"
		}
	}
	let fieldsArr = []
	let catList = [
		"dashboards",
		"guild_info",
		"levels",
		"modcom",
		"quotes",
		"reaction",
		"rip",
		"voicecom",
		"warnings",
		"welcome",
		"user_updates"
	]
	let arrayNames = [
		"banned_words",
		"reward_lvls",
		"banned_roles",
		"reward_roles",
		"role_access",
		"access_roles"
	]
	let roleNames = [
		"banned_roles",
		"reward_roles",
		"role_access",
		"access_roles"
	]
	let channelNames = [
		"ann_ch",
		"ann_hdj_ch",
		"gen_ch"
	]
	if (infoType == "memory") {
		for (let i = 0; i < catList.length; i++) {
			let valueStr = ""
			async.forEachOf(settings.web[catList[i]][guild.id], (value, variable, callback) => {
				if (variable != "id" && variable != "guid") {
					if (roleNames.indexOf(variable) != -1) {
						devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] If statement roleNames.indexOf(${variable}) != -1 seems to be ${(arrayNames.indexOf(variable) != -1) ? true : false}, value: `, value)
						
						let mA = []
						for (let i = 0; i < value.length; i++) {
							mA[i] = `<@&${value[i]}>`
						}
						valueStr += `**${variable}**: ${mA.join(",")}\n`

						devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Adding following to 'valueStr': ${variable + ": " + mA.join(",") + "\n"}`)
					} else if (channelNames.indexOf(variable) != -1) {
						devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] If statement channelNames.indexOf(${variable}) != -1 seems to be ${(arrayNames.indexOf(variable) != -1) ? true : false}, value: `, value)
						
						valueStr += `**${variable}**: <#${value}>\n`

						devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Adding following to 'valueStr': ${variable + ": " + value + "\n"}`)
					} else if (arrayNames.indexOf(variable) != -1) {
						devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] If statement arrayNames.indexOf(${variable}) != -1 seems to be ${(arrayNames.indexOf(variable) != -1) ? true : false}, value: `, value)
						
						valueStr += `**${variable}**: ${value.join(",")}\n`

						devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Adding following to 'valueStr': ${variable + ": " + value.join(",") + "\n"}`)
					} else {
						valueStr += `**${variable}**: ${(typeof(value) == "boolean") ? value.toString() : value}\n`

						devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Adding following to 'valueStr': ${variable + ": " + (typeof(value) == "boolean") ? value.toString() : value + "\n"}`)
					}
					callback()
				}
			}, function (err) {
				if (err) {
					console.log(colorScheme.critical, `[${moduleName}][Critical][${guild.id}] An unexpected error occured while running '${msg.command.label} ${args.split(" ")}'. See debug for error.`)
					debugMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Error message: `, err)
				} else {
					devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Pushing 'valueStr' to 'fieldsArr', valueStr: `, valueStr)
					if (isEmpty(valueStr)) {
						valueStr = "<empty>"
					}
					fieldsArr.push({
						name: catList[i],
						value: valueStr,
						inline: true
					})
					devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] fieldsArr now: `, fieldsArr)
				}
			})
		}
		sendMessage()
	} else if (infoType == "database") {
		devMsg(`[`+moduleName+`][`+debugMode+`][${msg.channel.guild.id}] Started Gathering information... 0ms`)
		console.time("dbGather")
		async.forEachOf(catList, (_value, i, callback) => {
			let valueStr = ""
			mysql_con_web.query(`SELECT * FROM ${(catList[i] == "user_updates") ? "roles" : catList[i]} WHERE guid='${guild.id}'`, function(err, rows, _fields) {
				if (err) {
					console.log(colorScheme.critical, `[MySQL][Critical][${guild.id}] An unexpected error occured while doing a query for '${msg.command.label} ${args.split(" ")}'. See debug for error.`)
					debugMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Error message: `, err)
					return
				}
				async.forEachOf(rows[0], (value, variable, callback) => {
					if (variable != "id" && variable != "guid") {
						if (roleNames.indexOf(variable) != -1) {
							devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] If statement roleNames.indexOf(${variable}) != -1 seems to be ${(arrayNames.indexOf(variable) != -1) ? true : false}, value: `, value)
							
							let valueArr = value.split(",")
							let mA = []
							for (let i = 0; i < valueArr.length; i++) {
								mA[i] = `<@&${valueArr[i]}>`
							}
							valueStr += `**${variable}**: ${mA.join(",")}\n`
		
							devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Adding following to 'valueStr': ${variable + ": " + mA.join(",") + "\n"}`)
						} else if (channelNames.indexOf(variable) != -1) {
							devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] If statement channelNames.indexOf(${variable}) != -1 seems to be ${(arrayNames.indexOf(variable) != -1) ? true : false}, value: `, value)
							
							valueStr += `**${variable}**: <#${value}>\n`
		
							devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Adding following to 'valueStr': ${variable + ": " + value + "\n"}`)
						} else if (arrayNames.indexOf(variable) != -1) {
							devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] If statement arrayNames.indexOf(${variable}) != -1 seems to be ${(arrayNames.indexOf(variable) != -1) ? true : false}, value: `, value)
							
							valueStr += `**${variable}**: ${value}\n`
		
							devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Adding following to 'valueStr': ${variable + ": " + value + "\n"}`)
						} else {
							valueStr += `**${variable}**: ${(value == "on" || value == "yes") ? true : (value == "off" || value == "no") ? false : value}\n`
							
							devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Adding following to 'valueStr': ${variable + ": " + (value == "on" || value == "yes") ? true : (value == "off" || value == "no") ? false : value + "\n"}`)
						}
					}
					callback()
				}, function (err) {
					if (err) {
						console.log(colorScheme.critical, `[${moduleName}][Critical][${guild.id}] An unexpected error occured while running '${msg.command.label} ${args.split(" ")}'. See debug for error.`)
						debugMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Error message: `, err)
						return callback()
					} else {
						devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Pushing 'valueStr' to 'fieldsArr', valueStr: `, valueStr)
						fieldsArr.push({
							name: catList[i],
							value: valueStr,
							inline: true
						})
						devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] fieldsArr now: `, fieldsArr)
						return callback()
					}
				})
			})
		}, function (err) {
			devMsg(`[`+moduleName+`][`+debugMode+`][${msg.channel.guild.id}] Done with gathering data, toke me: `)
			console.timeEnd("dbGather")
			if (err) {
				console.log(colorScheme.critical, `[${moduleName}][Critical][${guild.id}] An unexpected error occured while running '${msg.command.label} ${args.split(" ")}'. See debug for error.`)
				debugMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Error message: `, err)
			} else {
				sendMessage()
			}
		})
	} else if (infoType == "diff") {
		devMsg(`[`+moduleName+`][`+debugMode+`][${msg.channel.guild.id}] Started Gathering information... 0ms`)
		console.time("dbGather")
		async.forEachOf(catList, (_value, i, callback) => {
			let valueStr = ""
			mysql_con_web.query(`SELECT * FROM ${(catList[i] == "user_updates") ? "roles" : catList[i]} WHERE guid='${guild.id}'`, function(err, rows, _fields) {
				if (err) {
					console.log(colorScheme.critical, `[MySQL][Critical][${guild.id}] An unexpected error occured while doing a query for '${msg.command.label} ${args.split(" ")}'. See debug for error.`)
					debugMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Error message: `, err)
					return
				}
				async.forEachOf(rows[0], (value, variable, callback) => {
					if (variable != "id" && variable != "guid") {
						if (roleNames.indexOf(variable) != -1) {
							devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] If statement roleNames.indexOf(${variable}) != -1 seems to be ${(arrayNames.indexOf(variable) != -1) ? true : false}, value: `, value)
							
							let valueArr = value.split(",")
							let mA = []
							for (let i = 0; i < valueArr.length; i++) {
								mA[i] = `<@&${valueArr[i]}>`
							}
							valueStr += `**${variable}**: ${mA.join(",")} ${(settings.web[catList[i]][guild.id] && typeof(settings.web[catList[i]][guild.id][variable]) == "object") ? (value == settings.web[catList[i]][guild.id][variable].join(",")) ? "*(same)*" : `***(diff: ${settings.web[catList[i]][guild.id][variable].join(",")}***` : "*(non-existent in memory)*"}\n`
		
							devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Adding following to 'valueStr': ${variable + ": " + mA.join(",") + "\n"})`)
						} else if (channelNames.indexOf(variable) != -1) {
							devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] If statement channelNames.indexOf(${variable}) != -1 seems to be ${(arrayNames.indexOf(variable) != -1) ? true : false}, value: `, value)
							devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Checking if ${value} and ${settings.web[catList[i]][guild.id][variable]} are the same and it seems to be ${(value == settings.web[catList[i]][guild.id][variable]) ? true : false}`)
							
							valueStr += `**${variable}**: <#${value}> ${(settings.web[catList[i]][guild.id] && settings.web[catList[i]][guild.id][variable]) ? (value == settings.web[catList[i]][guild.id][variable]) ? "*(same)*" : `***(diff: ${settings.web[catList[i]][guild.id][variable]})***` : "*(non-existent in memory)*"}\n`
		
							devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Adding following to 'valueStr': ${variable + ": " + value + "\n"}`)
						} else if (arrayNames.indexOf(variable) != -1) {
							devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] If statement arrayNames.indexOf(${variable}) != -1 seems to be ${(arrayNames.indexOf(variable) != -1) ? true : false}, value: `, value)
							
							valueStr += `**${variable}**: ${value} ${(settings.web[catList[i]][guild.id] && settings.web[catList[i]][guild.id][variable]) ? (value == settings.web[catList[i]][guild.id][variable].join(",")) ? "*(same)*" : `***(diff: ${settings.web[catList[i]][guild.id][variable].join(",")}***` : "*(non-existent in memory)*"}\n`
		
							devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Adding following to 'valueStr': ${variable + ": " + value + "\n"}`)
						} else {
							let valueRpl = (value == "on" || value == "yes") ? true : (value == "off" || value == "no") ? false : value
							valueStr += `**${variable}**: ${valueRpl} ${((settings.web[catList[i]][guild.id] && settings.web[catList[i]][guild.id][variable]) || (settings.web[catList[i]][guild.id] && typeof(settings.web[catList[i]][guild.id][variable]) == "boolean") ? true : false) ? (valueRpl == settings.web[catList[i]][guild.id][variable]) ? "*(same)*" : `***(diff: ${settings.web[catList[i]][guild.id][variable]})***` : "*(non-existent in memory)*"}\n`
						
							devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Adding following to 'valueStr': ${variable + ": " + (value == "on" || value == "yes") ? true : (value == "off" || value == "no") ? false : value + " " + ((value == "on" || value == "yes") ? true : (value == "off" || value == "no") ? false : value == settings.web[catList[i]][guild.id][variable]) ? "*(same)*" : `*(diff: ${(arrayNames.indexOf(variable) != -1) ? settings.web[catList[i]][guild.id][variable].split(",") : settings.web[catList[i]][guild.id][variable]})` + "\n"}`)
						}
					}
					callback()
				}, function (err) {
					if (err) {
						console.log(colorScheme.critical, `[${moduleName}][Critical][${guild.id}] An unexpected error occured while running '${msg.command.label} ${args.split(" ")}'. See debug for error.`)
						debugMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Error message: `, err)
						return callback()
					} else {
						devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Pushing 'valueStr' to 'fieldsArr', valueStr: `, valueStr)
						fieldsArr.push({
							name: catList[i],
							value: valueStr,
							inline: true
						})
						devMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] fieldsArr now: `, fieldsArr)
						return callback()
					}
				})
			})
		}, function (err) {
			devMsg(`[`+moduleName+`][`+debugMode+`][${msg.channel.guild.id}] Done with gathering data, toke me: `)
			console.timeEnd("dbGather")
			if (err) {
				console.log(colorScheme.critical, `[${moduleName}][Critical][${guild.id}] An unexpected error occured while running '${msg.command.label} ${args.split(" ")}'. See debug for error.`)
				debugMsg(`[`+moduleName+`][`+debugMode+`][${guild.id}] Error message: `, err)
			} else {
				sendMessage()
			}
		})
	} else {
		return "Je eerste argument is niet een van de volgende: `memory, database, both`"
	}
	//
}, {
	// Boolean (optional) - If arguments are required or not
	argsRequired: false,
	// Boolean (optional) - Whether to delete the user command message or not
	deleteCommand: false,
	// String (optional) - A short description of the command to show in the default help command
	description: "Weergeeft de (web) instellingen van de server",
	// Boolean (optional) - Whether to prevent the command from being used in guilds or not
	dmOnly: false,
	// String (optional) - A detailed description of the command to show in the default help command
	fullDescription: "Weergeeft de (web) instellingen van de server zoals ze opgeslagen zijn in memory.\nDiff: is van `database` -> `memory`\n\n[] <= Zijn optioneel\n{} <= Is standaard",
	// Boolean (optional) - Whether to prevent the command from being used in Direct Messages or not
	guildOnly: false,
	// function | String (optional) - A string or a function that returns a string to show when the user doesn't have permissions to use the command. The function is passed the Message object as a parameter.
	permissionMessage: "",
	// String (optional) - Details on how to call the command to show in the default help command
	usage: "[{memory}/database/diff] [guild id]"
})

// API Handler
const http = require('http')
debugMsg(`[`+moduleName+`][`+debugMode+`] Module 'http' loaded as 'http'.`)
const httpAuth = require('http-auth')
debugMsg(`[`+moduleName+`][`+debugMode+`] Module 'http-auth' loaded as 'httpAuth'.`)
const url = require('url')
debugMsg(`[`+moduleName+`][`+debugMode+`] Module 'url' loaded as 'url'.`)
const basic = httpAuth.basic({
	realm: "Frankie's API Handler gemaakt door Androiddd#4682",
	file: "./credentials/.htpasswd"
})

const authModeuleName = "API-Auth"

basic.on('success', (result) => {
    console.log(colorScheme.cyan, `[${authModeuleName}][Info] User authenticated: ${result.user}`)
})
 
basic.on('fail', (result, req) => {
	if (typeof(result.user) != "undefined") {
		console.log(colorScheme.yellow, `[${authModeuleName}][Info] User authentication failed: ${result.user}`)
	} else {
		console.log(colorScheme.red, `[${authModeuleName}][Warning] Unauthorized authentication attempt from IP: ${req.client.remoteAddress}:${req.client.remotePort}`)
	}
})

basic.on('error', (error) => {
    console.log(colorScheme.red, `[${authModeuleName}][Error] Authentication error: ${error.code + " - " + error.message}`)
})

const httpInf = require("./credentials/httpInf.js")
const apiModuleName = "API-Handler"

function apiKeyRegen(authKey) {
	return new Promise(function(resolve, reject) {
		if (apiAuthKeys[md5(authKey)]) {
			if (apiAuthKeys[md5(authKey)].expires == "semi-permanent") {
				let newKey = md5(new Date().valueOf())
				apiAuthKeys[md5(newKey)] = {}
				apiAuthKeys[md5(newKey)].userID = apiAuthKeys[md5(authKey)].userID
				apiAuthKeys[md5(newKey)].key = md5(newKey)
				apiAuthKeys[md5(newKey)].expires = apiAuthKeys[md5(authKey)].expires
				delete apiAuthKeys[md5(authKey)]
				apiSemiKeysIO().catch(() => {
					// Nothing?
				})
				resolve(newKey)
			} else if (apiAuthKeys[md5(authKey)].expires == "permanent") {
				resolve("same")
			} else {
				reject("forgery")
			}
		} else {
			resolve(null)
		}
	})
}

function apiAdminAuth(authKey) {
	return new Promise(function(resolve, reject) {
		if (apiAuthKeys[authKey]) {
			if (new Date(apiAuthKeys[authKey].expires) > new Date()) {
				resolve(null)
			} else {
				reject("expired")
			}
		} else if (apiAuthKeys[md5(authKey)]) {
			if (apiAuthKeys[md5(authKey)].expires == "semi-permanent" || apiAuthKeys[md5(authKey)].expires == "permanent") {
				resolve("semi")
			} else {
				reject("forgery")
			}
		} else {
			reject("invalid")
		}
	})
}

// Loading ip lists
fs.readFile(`./vars/ipLists.js`, (err, data) => {
	if (err) {
		if (err.message.indexOf("no such file or directory") != -1) {
			debugMsg(`[`+moduleName+`][`+debugMode+`] ./vars/ipLists.js couldn't be found. Creating a new one...`)
			fs.writeFile(`./vars/ipLists.js`, JSON.stringify(ipLists), function(err) {
				if (err) {
					console.log(colorScheme.error, `[${moduleName}][Error] Failed to create ipLists.js file!`)
					console.log(colorScheme.error, `[${moduleName}][Error-Message] ${err}`)
					process.exit(48)
				} else {
					console.log(colorScheme.warning, `[${moduleName}][Warning] New ipLists.js file has been created successfully!`)
				}
			})
		} else {
			console.log(colorScheme.error, `[${moduleName}][Error] Failed to load ipLists.js contents from file!`)
			console.log(colorScheme.error, `[${moduleName}][Error-Message] ${err}`)
			process.exit(49)
		}
	} else {
		try {
			ipLists = JSON.parse(data)
		} catch (error) {
			console.log(colorScheme.critical, `[${moduleName}][Critical] ipList.js contents are not a JSON! Failed to initialize ip lists`)
			console.log(colorScheme.error, `[${moduleName}][Error] Error messages: ${error}`)
			process.exit(50)
		}
	}
})

// Actual API server
const server = http.createServer(basic, (req, res) => {
	// IP Check Blacklist
	if (ipLists.blacklist.indexOf(req.client.remoteAddress) != -1) {
		devMsg(`[`+apiModuleName+`][`+debugMode+`] IP on blacklist! Calc: (ipLists.blacklist.indexOf(${req.client.remoteAddress}) != -1) which seems to be ${(ipLists.blacklist.indexOf(req.client.remoteAddress) != -1) ? true : false}. Blacklist: `, ipLists.blacklist)
		debugMsg(`[`+apiModuleName+`][`+debugMode+`] Dropping request from ${req.client.remoteAddress} because it's blacklisted`)
		res.end()
	}
	// IP Check Whitelist
	if (ipLists.whitelist[0] && ipLists.whitelist.indexOf(req.client.remoteAddress) == -1) {
		devMsg(`[`+apiModuleName+`][`+debugMode+`] IP on whitelist! Calc: (ipLists.whitelist[0] AND ipLists.whitelist.indexOf(${req.client.remoteAddress}) == -1) which seems to be ${(ipLists.whitelist[0]) ? true : false} AND ${(ipLists.whitelist.indexOf(req.client.remoteAddress) == -1) ? true : false}. Whitelist: `, ipLists.whitelist)
		debugMsg(`[`+apiModuleName+`][`+debugMode+`] Dropping request from ${req.client.remoteAddress} because whitelist is set and IP isn't on the list`)
		res.end()
	}
	// API Endpoints
	let reqURL = url.parse(req.url, true)
	if (req.method === "GET") {
		kDebugMsg(`[`+apiModuleName+`][`+debugMode+`] Receiving GET request from ${req.client.remoteAddress}:${req.client.remotePort}, url: `, reqURL)
		if (reqURL.pathname === "/guild") {
			debugMsg(`[${apiModuleName}][`+debugMode+`] Executing /guild code to IP ${req.client.remoteAddress}:${req.client.remotePort}.`)
			let infoObj = reqURL.query
			if (infoObj.guildID) {
				let guild = bot.guilds.get(infoObj.guildID)
				devMsg(`[API/guild][`+debugMode+`][${req.client.remoteAddress}:${req.client.remotePort}] Got guild: `, guild)
				if (guild && guild.id === infoObj.guildID) {
					let returnVal = {
						guilds: {
							[guild.id]: {
								id: guild.id,
								name: guild.name,
								iconURL: guild.iconURL,
								icon: guild.icon,
								createdAt: guild.createdAt,
								memberCount: guild.memberCount,
								mfaLevel: guild.mfaLevel,
								region: guild.region,
								verificationLevel: guild.verificationLevel,
								botPermission: guild.members.get(bot.user.id).permission.allow,
								roles: {}
							}
						}
					}
					let member = null
					if (infoObj.userID) {
						member = guild.members.get(infoObj.userID)
						devMsg(`[API/guild][`+debugMode+`][${req.client.remoteAddress}:${req.client.remotePort}] Got member: `, member)
						if (member && member.id != infoObj.userID) {
							res.statusCode = 400
							res.setHeader('Content-Type', 'application/json')
							res.end(`{"code": 400, "response": "failed", "return": "Invalid user ID OR user couldn't be found in the server"}`)
							return
						} else {
							returnVal.members = {
								[member.id]: {
									avatar: member.avatar,
									avatarURL: member.avatarURL,
									bot: member.bot,
									createdAt: member.createdAt,
									defaultAvatar: member.defaultAvatar,
									defaultAvatarURL: member.defaultAvatarURL,
									discriminator: member.discriminator,
									id: member.id,
									joinedAt: member.joinedAt,
									nick: member.nick,
									permission: member.permission.allow,
									roles: member.roles,
									staticAvatarURL: member.staticAvatarURL,
									username: member.username
								}
							}
						}
					}
					devMsg(`[API/guild][`+debugMode+`] Starting role sorting...`)
					async.forEachOf(guild.roles, (roleArr, key, callback) => {
						let roleID = roleArr[0]
						let role = roleArr[1]
						devMsg(`[API/guild][`+debugMode+`] Got role ${roleID}, obj: `, role)
						devMsg(`[API/guild][`+debugMode+`] Found permission obj: `, role.permissions)
						returnVal.guilds[infoObj.guildID].roles[roleID] = {
							color: role.color,
							createdAt: role.createdAt,
							hoist: role.hoist,
							id: role.id,
							managed: role.managed,
							mentionable: role.mentionable,
							name: role.name,
							permissions: role.permissions.allow,
							position: role.position
						}
						callback()
					}, function(err) {
						if (err) {
							res.statusCode = 500
							res.setHeader('Content-Type', 'application/json')
							res.end(`{"code": 500, "response": "error", "return": ${JSON.stringify(err)}}`)
							return
						}
					})
					res.statusCode = 200
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 200, "response": "success", "return": ${JSON.stringify(returnVal)}}`)
				} else {
					res.statusCode = 400
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 400, "response": "failed", "return": "Invalid guild ID OR bot isn't in the server"}`)
					return
				}
			} else {
				res.statusCode = 400
				res.setHeader('Content-Type', 'application/json')
				res.end(`{"code": 400, "response": "failed", "return": "Missing guild id"}`)
				return
			}
		} else if (reqURL.pathname === "/memberList") {
			debugMsg(`[${apiModuleName}][`+debugMode+`] Executing /memberList code to IP ${req.client.remoteAddress}:${req.client.remotePort}.`)
			let infoObj = reqURL.query
			if (infoObj.guildID) {
				let guild = bot.guilds.get(infoObj.guildID)
				devMsg(`[API/memberList][`+debugMode+`][${req.client.remoteAddress}:${req.client.remotePort}] Got guild: `, guild)
				if (guild && guild.id === infoObj.guildID) {
					let returnVal = {
						members: {}
					}
					let i = 0
					async.forEachOf(guild.members, (memberArr, _key, callback) => {
						let memberID = memberArr[0]
						let member = memberArr[1]
						let rHC = roleHighest(member.roles, guild.id)
						devMsg(`[API/memberList][`+debugMode+`][${req.client.remoteAddress}:${req.client.remotePort}] rHC`, rHC)
						returnVal.members[memberID] = {
							avatar: member.avatar,
							avatarURL: member.avatarURL,
							bot: member.bot,
							createdAt: member.createdAt,
							defaultAvatar: member.defaultAvatar,
							defaultAvatarURL: member.defaultAvatarURL,
							discriminator: member.discriminator,
							id: member.id,
							joinedAt: member.joinedAt,
							nick: member.nick,
							permission: member.permission.allow,
							roles: member.roles,
							staticAvatarURL: member.staticAvatarURL,
							username: member.username,
							roleHighID: rHC
						}
						callback()
					}, function(err) {
						if (err) {
							res.statusCode = 500
							res.setHeader('Content-Type', 'application/json')
							res.end(`{"code": 500, "response": "error", "return": ${JSON.stringify(err)}}`)
							return
						}
					})
					res.statusCode = 200
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 200, "response": "success", "return": ${JSON.stringify(returnVal)}}`)
				} else {
					res.statusCode = 400
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 400, "response": "failed", "return": "Invalid guild ID OR bot isn't in the server"}`)
					return
				}
			} else {
				res.statusCode = 400
				res.setHeader('Content-Type', 'application/json')
				res.end(`{"code": 400, "response": "failed", "return": "Missing guild id"}`)
				return
			}
		} else if (reqURL.pathname === "/channelList") {
			debugMsg(`[${apiModuleName}][`+debugMode+`] Executing /channelList code to IP ${req.client.remoteAddress}:${req.client.remotePort}.`)
			let infoObj = reqURL.query
			if (infoObj.guildID) {
				let guild = bot.guilds.get(infoObj.guildID)
				devMsg(`[API/channelList][`+debugMode+`][${req.client.remoteAddress}:${req.client.remotePort}] Got guild: `, guild)
				if (guild && guild.id === infoObj.guildID) {
					let returnVal = {
						channels: {}
					}
					async.forEachOf(guild.channels, (channelArry, _key, callback) => {
						let channelID = channelArry[0]
						let channel = channelArry[1]
						returnVal.channels[channelID] = {
							id: channel.id,
							name: channel.name,
							nsfw: channel.nsfw,
							parentID: channel.parentID,
							position: channel.position,
							type: channel.type
						}
						callback()
					}, function(err) {
						if (err) {
							res.statusCode = 500
							res.setHeader('Content-Type', 'application/json')
							res.end(`{"code": 500, "response": "error", "return": ${JSON.stringify(err)}}`)
							return
						}
					})
					res.statusCode = 200
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 200, "response": "success", "return": ${JSON.stringify(returnVal)}}`)
				} else {
					res.statusCode = 400
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 400, "response": "failed", "return": "Invalid guild ID OR bot isn't in the server"}`)
					return
				}
			} else {
				res.statusCode = 400
				res.setHeader('Content-Type', 'application/json')
				res.end(`{"code": 400, "response": "failed", "return": "Missing guild id"}`)
				return
			}
		} else if (reqURL.pathname === "/leadDevInfo") {
			debugMsg(`[${apiModuleName}][`+debugMode+`] Executing /leadDevInfo code to IP ${req.client.remoteAddress}:${req.client.remotePort}.`)

			let leadDev = bot.users.get(credits.leaddevelopers[0][0])
			devMsg(`[API/leadDevInfo][`+debugMode+`][${req.client.remoteAddress}:${req.client.remotePort}] Got leadDev: `, leadDev)
			returnVal = {
				"avatar": leadDev.avatar,
				"avatarURL": leadDev.avatarURL,
				"createdAt":  leadDev.createdAt,
				"defaultAvatar": leadDev.defaultAvatar,
				"defaultAvatarURL": leadDev.defaultAvatarURL,
				"discriminator": leadDev.discriminator,
				"id": leadDev.id,
				"staticAvatarURL": leadDev.staticAvatarURL,
				"username": leadDev.username
			}
			res.statusCode = 200
			res.setHeader('Content-Type', 'application/json')
			res.end(`{"code": 200, "response": "success", "return": ${JSON.stringify(returnVal)}}`)
		} else if (reqURL.pathname === "/testSemiKey") {
			debugMsg(`[${apiModuleName}][`+debugMode+`] Executing /testSemiKey code to IP ${req.client.remoteAddress}:${req.client.remotePort}.`)
			let infoObj = reqURL.query
			apiAdminAuth(reqURL.query.authKey).then((keyType) => {
				let authKey = reqURL.query.authKey
				if (keyType == "semi") {
					authKey = md5(reqURL.query.authKey)
				}
				let authUser = bot.users.get(apiAuthKeys[authKey].userID) ? bot.users.get(apiAuthKeys[authKey].userID).username + "#" + bot.users.get(apiAuthKeys[authKey].userID).discriminator + ` (${bot.users.get(apiAuthKeys[authKey].userID).id})` : apiAuthKeys[authKey].userID
				console.log(colorScheme.critical, `[${apiModuleName}-Admin][Critical][${req.client.remoteAddress}:${req.client.remotePort}] User ${authUser} requested to testSemiKey!`)
				devMsg(`[API/testSemiKey][`+debugMode+`][${req.client.remoteAddress}:${req.client.remotePort}] Key expire is: ${apiAuthKeys[authKey].expires}`)
				if (apiAuthKeys[authKey].expires != "permanent") {
					console.dir(infoObj)
				}
				logger(adminReqFileName, `[${apiModuleName}-Admin][Critical][${req.client.remoteAddress}:${req.client.remotePort}] User ${authUser} requested to testSemiKey!`).catch((error) => {
					console.log(colorScheme.warning, `[Promise][Warning] Logger function failed. Error:`, error)
				})
				// Actual code
				apiKeyRegen(reqURL.query.authKey).then((newKey) => {
					if (newKey === null) {
						delete apiAuthKeys[reqURL.query.authKey]
					}
					res.statusCode = 200
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 200, "response": "success", "return": "Test success!", "newKey": "${newKey}"}`)
				}).catch((error) => {
					console.log(colorScheme.warning, `[API/testSemiKey][Promise][Warning][${req.client.remoteAddress}:${req.client.remotePort}] API Key Regens promise failed. See debug for error.`)
					debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, error)
					res.statusCode = 200
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 200, "response": "success", "return": "Test failed!", "newKey": "error"}`)
				})
			}).catch((errMsg) => {
				if (errMsg == "expired") {
					res.statusCode = 401
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 401, "response": "Failed", "return": "Key is expired"}`)
				} else if (errMsg == "invalid") {
					res.statusCode = 498
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 498, "response": "Invalid Token", "return": "Key is invalid"}`)
				} else if (errMsg == "forgery") {
					res.statusCode = 418
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 418, "response": "I'm a teapot", "return": "Key is fake"}`)
				} else {
					res.statusCode = 403
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 403, "response": "Failed", "return": "Key got rejected", "errorMessage": "${errMsg}"}`)
				}
			})
		} else if (reqURL.pathname === "/leaveServer") {
			debugMsg(`[${apiModuleName}][`+debugMode+`] Executing /leaveServer code to IP ${req.client.remoteAddress}:${req.client.remotePort}.`)
			let infoObj = reqURL.query
			apiAdminAuth(reqURL.query.authKey).then((keyType) => {
				let authKey = reqURL.query.authKey
				if (keyType == "semi") {
					authKey = md5(reqURL.query.authKey)
				}
				let authUser = bot.users.get(apiAuthKeys[authKey].userID) ? bot.users.get(apiAuthKeys[authKey].userID).username + "#" + bot.users.get(apiAuthKeys[authKey].userID).discriminator + ` (${bot.users.get(apiAuthKeys[authKey].userID).id})` : apiAuthKeys[authKey].userID
				console.log(colorScheme.critical, `[${apiModuleName}-Admin][Critical][${req.client.remoteAddress}:${req.client.remotePort}] User ${authUser} requested to leaveServer!`)
				devMsg(`[API/leaveServer][`+debugMode+`][${req.client.remoteAddress}:${req.client.remotePort}] Key expire is: ${apiAuthKeys[authKey].expires}`)
				if (apiAuthKeys[authKey].expires != "permanent") {
					console.dir(infoObj)
				}
				logger(adminReqFileName, `[${apiModuleName}-Admin][Critical][${req.client.remoteAddress}:${req.client.remotePort}] User ${authUser} requested to leaveServer!`).catch((error) => {
					console.log(colorScheme.warning, `[Promise][Warning] Logger function failed. Error:`, error)
				})
				// Actual code
				if (infoObj.guildID) {
					let guild = bot.guilds.get(infoObj.guildID)
					devMsg(`[API/leaveServer][`+debugMode+`][${req.client.remoteAddress}:${req.client.remotePort}] Got guild: `, guild)
					if (guild && guild.id === infoObj.guildID) {
						bot.leaveGuild(infoObj.guildID).then(() => {
							apiKeyRegen(reqURL.query.authKey).then((newKey) => {
								if (newKey === null) {
									delete apiAuthKeys[reqURL.query.authKey]
								}
								res.statusCode = 200
								res.setHeader('Content-Type', 'application/json')
								res.end(`{"code": 200, "response": "success", "return": null, "newKey": "${newKey}"}`)
							}).catch((error) => {
								console.log(colorScheme.warning, `[API/leaveServer][Promise][Warning][${req.client.remoteAddress}:${req.client.remotePort}] API Key Regens promise failed. See debug for error.`)
								debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, error)
								res.statusCode = 200
								res.setHeader('Content-Type', 'application/json')
								res.end(`{"code": 200, "response": "success", "return": null, "newKey": "error"}`)
							})
						}).catch((err) => {
							console.log(colorScheme.critical, `[API/leaveServer][Promise][Critical][${req.client.remoteAddress}:${req.client.remotePort}] Bot failed to leaver the guild. See debug for error.`)
							debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, err)
							res.statusCode = 500
							res.setHeader('Content-Type', 'application/json')
							res.end(`{"code": 500, "response": "failed", "return": "Bot failed to leave the guild. See console for more information."}`)
						})
					}
				} else {
					res.statusCode = 400
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 400, "response": "failed", "return": "Missing guild id"}`)
					return
				}
			}).catch((errMsg) => {
				if (errMsg == "expired") {
					res.statusCode = 401
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 401, "response": "Failed", "return": "Key is expired"}`)
				} else if (errMsg == "invalid") {
					res.statusCode = 498
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 498, "response": "Invalid Token", "return": "Key is invalid"}`)
				} else if (errMsg == "forgery") {
					res.statusCode = 418
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 418, "response": "I'm a teapot", "return": "Key is fake"}`)
				} else {
					res.statusCode = 403
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 403, "response": "Failed", "return": "Key got rejected", "errorMessage": "${errMsg}"}`)
				}
			})
		} else {
			res.statusCode = 404
			res.setHeader('Content-Type', 'application/json')
			res.end(`{"code": 404, "response": "failed", "return": "Endpoint not found"}`)
			return
		}
	} else if (req.method === "POST") {
		if (reqURL.pathname === "/updateSettings") {
			debugMsg(`[${apiModuleName}][`+debugMode+`] Executing /updateSettings code to IP ${req.client.remoteAddress}:${req.client.remotePort}.`)
			apiAdminAuth(reqURL.query.authKey).then((keyType) => {
				let authKey = reqURL.query.authKey
				if (keyType == "semi") {
					authKey = md5(reqURL.query.authKey)
				}
				let authUser = bot.users.get(apiAuthKeys[authKey].userID) ? bot.users.get(apiAuthKeys[authKey].userID).username + "#" + bot.users.get(apiAuthKeys[authKey].userID).discriminator + ` (${bot.users.get(apiAuthKeys[authKey].userID).id})` : apiAuthKeys[authKey].userID
				console.log(colorScheme.critical, `[${apiModuleName}-Admin][Critical][${req.client.remoteAddress}:${req.client.remotePort}] User ${authUser} requested to updateSettings!`)
				devMsg(`[API/updateSettings][`+debugMode+`][${req.client.remoteAddress}:${req.client.remotePort}] Key expire is: ${apiAuthKeys[authKey].expires}`)
				if (apiAuthKeys[authKey].expires != "permanent") {
					console.dir(infoObj)
				}
				logger(adminReqFileName, `[${apiModuleName}-Admin][Critical][${req.client.remoteAddress}:${req.client.remotePort}] User ${authUser} requested to testSemiKey!`).catch((error) => {
					console.log(colorScheme.warning, `[Promise][Warning] Logger function failed. Error:`, error)
				})
				let infoObj = reqURL.query
				let body = ""
				req.on("data", function (chunk) {
					body += chunk
				})

				req.on("end", function() {
					let bodyJSON = null
					try {
						bodyJSON = JSON.parse(body)
						if (!bodyJSON.setting || !bodyJSON.values) {
							throw new Error("Missing bodyJSON variables")
						} else {
							devMsg(`[API/updateSettings][`+debugMode+`][${req.client.remoteAddress}:${req.client.remotePort}] bodyJSON: `, bodyJSON)
						}
					} catch (error) {
						debugMsg(`[`+apiModuleName+`][`+debugMode+`][${req.client.remoteAddress}:${req.client.remotePort}] Body didn't seem to be JSON. Error message: `, error)
						res.statusCode = 400
						res.setHeader('Content-Type', 'application/json')
						res.end(`{"code": 400, "response": "failed", "return": "Body isn't a JSON"}`)
						return
					}
					if (infoObj.guildID) {
						let guild = bot.guilds.get(infoObj.guildID)
						if (guild) {
							if (guild.id == infoObj.guildID) {
								devMsg(`[API/updateSettings][`+debugMode+`][${req.client.remoteAddress}:${req.client.remotePort}] Found guild (${guild.id}), obj: `, guild)
								if (settings.web[bodyJSON.setting]) {
									devMsg(`[API/updateSettings][`+debugMode+`][${req.client.remoteAddress}:${req.client.remotePort}] settings.web[${bodyJSON.setting}]: `, settings.web[bodyJSON.setting])
									if (!settings.web[bodyJSON.setting][guild.id]) {
										settings.web[bodyJSON.setting][guild.id] = {}
									}
									devMsg(`[API/updateSettings][`+debugMode+`][${req.client.remoteAddress}:${req.client.remotePort}] settings.web[${bodyJSON.setting}][${guild.id}] before update: `, settings.web[bodyJSON.setting][guild.id])
									for (let i = 0; i < bodyJSON.values.length; i++) {
										settings.web[bodyJSON.setting][guild.id][bodyJSON.values[i].variable] = bodyJSON.values[i].value
									}
									settings.web[bodyJSON.setting][guild.id].guild_id = guild.id
									devMsg(`[API/updateSettings][`+debugMode+`][${req.client.remoteAddress}:${req.client.remotePort}] settings.web[${bodyJSON.setting}][${guild.id}] after update: `, settings.web[bodyJSON.setting][guild.id])
									apiKeyRegen(reqURL.query.authKey).then((newKey) => {
										if (newKey === null) {
											delete apiAuthKeys[reqURL.query.authKey]
										}
										res.statusCode = 200
										res.setHeader('Content-Type', 'application/json')
										res.end(`{"code": 200, "response": "success", "return": null, "newKey": "${newKey}"}`)
									}).catch((error) => {
										console.log(colorScheme.warning, `[API/updateSettings][Promise][Warning][${req.client.remoteAddress}:${req.client.remotePort}] API Key Regens promise failed. See debug for error.`)
										debugMsg(`[`+moduleName+`][`+debugMode+`] Error message: `, error)
										res.statusCode = 200
										res.setHeader('Content-Type', 'application/json')
										res.end(`{"code": 200, "response": "success", "return": null, "newKey": "error"}`)
									})
								} else {
									res.statusCode = 400
									res.setHeader('Content-Type', 'application/json')
									res.end(`{"code": 400, "response": "failed", "return": "Invalid setting given"}`)
								}
							} else {
								res.statusCode = 404
								res.setHeader('Content-Type', 'application/json')
								res.end(`{"code": 404, "response": "failed", "return": "Invalid guild ID or bot isn't in guild"}`)
							}
						} else {
							res.statusCode = 404
							res.setHeader('Content-Type', 'application/json')
							res.end(`{"code": 404, "response": "failed", "return": "Invalid guild ID or bot isn't in guild"}`)
						}
					} else {
						res.statusCode = 400
						res.setHeader('Content-Type', 'application/json')
						res.end(`{"code": 400, "response": "failed", "return": "Missing guild id"}`)
						return
					}
				})
			}).catch((errMsg) => {
				if (errMsg == "expired") {
					res.statusCode = 401
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 401, "response": "Failed", "return": "Key is expired"}`)
				} else if (errMsg == "invalid") {
					res.statusCode = 498
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 498, "response": "Invalid Token", "return": "Key is invalid"}`)
				} else if (errMsg == "forgery") {
					res.statusCode = 418
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 418, "response": "I'm a teapot", "return": "Key is fake"}`)
				} else {
					res.statusCode = 403
					res.setHeader('Content-Type', 'application/json')
					res.end(`{"code": 403, "response": "Failed", "return": "Key got rejected", "errorMessage": "${errMsg}"}`)
				}
			})
		} else {
			debugMsg(`[${apiModuleName}][`+debugMode+`] Sending 'This isn\'t a valid API point.' to IP ${req.client.remoteAddress}:${req.client.remotePort}.`)
			res.statusCode = 404
			res.setHeader('Content-Type', 'application/json')
			res.end(`{"code": 404, "response": "failed", "return": "Endpoint not found"}`)
			return
		}
	} else {
		res.statusCode = 405
		res.setHeader('Content-Type', 'application/json')
		res.end(`{"code": 405, "response": "failed", "return": "Method not supported"}`)
		return
	}
})

bot.on("warn", (warn, shardID) => {
	console.log(colorScheme.warning, `[DiscordShard][Warning][${shardID}] The shard shat itself and is warning us about it.`)
	console.log(colorScheme.warning, `[DiscordShard][Warning][${shardID}] Warning message:`)
	console.dir(warn)
	logger("discordShard", `[Warning][${shardID}] ${warn}\nFull warning:\n${(typeof(warn) == "object") ? JSON.stringify(warn, null, 4) : warn}`)
})

bot.on("error", (err, shardID) => {
	console.log(colorScheme.error, `[DiscordShard][Error][${shardID}] The shard shat itself and doesn't know what to do now.`)
	console.log(colorScheme.error, `[DiscordShard][Error][${shardID}] Error message:`)
	console.dir(err)
	logger("discordShard", `[Error][${shardID}] ${err}\nFull error:\n${(typeof(err) == "object") ? JSON.stringify(err, null, 4) : err}`)
})

apiSemiKeysIO("load").then((_statusCode) => {
	console.log(colorScheme.execSuccess, `[${apiModuleName}-Admin][Info] API Semi Keys loaded!`)
	// Start API server
	server.listen(httpInf.port, httpInf.hostname, () => {
		console.log(colorScheme.connectionIS, `[${apiModuleName}][Info] API Handler is running on ${httpInf.hostname}:${httpInf.port} with Basis Authentication.`)
	})
	// Start Discord bot
	bot.connect().catch((err) => {
		console.log(colorScheme.error, `[${moduleName}][Error] Failed to connect to the Discord API Servers. Error message: `)
		console.dir(err)
		process.exit(51)
	})
}).catch((errCode) => {
	console.error(colorScheme.error, `[${apiModuleName}-Admin] Failed to load API Semi Keys. Exiting.`, errCode)
	process.exit(52)
})