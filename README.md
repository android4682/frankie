# DE Discord Bot voor alle Retro's!

Iedereen kent Frank(ie) wel maar het zou toch geweldig zijn als hij ook kan helpen in jouw Discord server?

Bij deze is dat absoluut mogelijk!

Op deze website/pagina kan je alle source code vinden van Frankie.
Niet alles staat hierop (zoals database tables, credentials etc.) maar wel het meeste.

## Hoe kan ik Frankie toevoegen aan mijn server?

Kwam je hier per ongeluk? Geen nood. Ga naar de [website van Frankie](http://www.discord-frankie.nl/) en voeg hem via daar toe.

## Features van de bot

- Levels
  - Laat je gebruikers XP en Levels verdienen door gewoon te chatten.
- Mod Commands
  - Verschillende pre-made commando's! Zoals: mute/ontslag (of kick)/ban enz.
- Voice Commando's
  - Gebruik verschillende voice commando's.
- Reactie geven
  - Elke actie heeft een reactie. Net als bij Frankie.
- Gebruiker Updates
  - Wil je weten wanneer iemands rollen, bijnaam etc. word geupdate en wat het ervoor was? Dan is deze functie voor jouw!
- Quotes
  - Er word vast wel eens genialen dingen gezegd in jouw server. Met deze functie kan je quotes toevoegen en weergeven.
- Waarschuwingen
  - Als je van die mensen hebt die de regels breken kan je met deze functie daar tegen in gaan!
- Welkom
  - Weergeeft een welkoms bericht voor de mensen die jouw server joinen. Maar ook als iemand de server verlaat.
- Rest in Peace
  - Soms heb je z'n rip moment. Maak er iets speciaals.

## FAQ

Als je de FAQ zoekt deze kan je [hier](https://gitlab.com/android4682/frankie/wikis/faq) vinden.